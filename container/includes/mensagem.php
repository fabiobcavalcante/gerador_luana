 <?php if ($_SESSION['sMsg'])	{ 
		if($_GET['bErro']==0){
			$sClasse = "alert alert-success alert-dismissible";
			$sIcone="icon fa fa-check";
		}else{
			$sClasse = "alert alert-danger alert-dismissible";
			$sIcone="icon fa fa-ban";
		}
	 ?>
		<p>
		<div id="mensagem" title="Mensagem" class="<?php echo $sClasse?>">
		 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><i class="<?php echo $sIcone?>"></i> <?=$_SESSION['sMsg']?></p>
		</div>
		</p>

		
<?php 
unset($_SESSION['sMsg']);
} ?>

<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
           <h4 class="alert alert-danger alert-dismissible">CONFIRMAÇÃO</h4>
        </div>
        <div class="modal-body">
             <h4><i class="icon fa fa-ban"></i> Deseja mesmo excluir este REGISTRO ?</h4>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >N&atilde;o</button>
             <a  href="" class="btn btn-success btn-ok" id="ok" >Sim</a>
        </div>
    </div>
    </div>
</div>

