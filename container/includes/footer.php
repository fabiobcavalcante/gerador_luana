  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Versão</b> 1.0
    </div>
    <strong>Copyright &copy; 2019 <a href="mailto:bc_fabio@hotmail.com">Fábio Cavalcante</a>.</strong> Todos os direitos reservados.
  </footer>