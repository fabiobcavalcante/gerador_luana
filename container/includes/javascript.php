
<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script> 
<!-- Buttons DataTables -->
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script> 
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script> 
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script> 
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js "></script> 

<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Select2 -->
<script src="bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="plugins/input-mask/jquery.inputmask.js"></script>
<script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

<script src="dist/js/producao.js"></script>
<!-- page script -->
<script>
  $(function () {
	//Initialize Select2 Elements
    $('.select2').select2();
	
    $('#lista').DataTable({
		dom: 'Bfrtip',
        buttons: ['excel', 'pdf', 'print'],
		'responsive': true,
		'paging'      : true,
		'lengthChange': false,
		'info'        : true,
		'autoWidth'   : true,
		"pagingType"  : "full",
		"language": {
            "decimal": ",",
            "thousands": ".",
            "lengthMenu": "Mostrar _MENU_ registros por p&aacute;gina",
            "zeroRecords": "Nada encontrado",
            "info": "Mostrando p&aacute;gina _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhum Registro Encontrado",
            "infoFiltered": "(filtrados de _MAX_ registros)",
			"search":         "Pesquisar:",
			"paginate": {
				"first":      "Primeiro",
				"last":       "&Uacute;ltimo",
				"next":       "Pr&oacute;ximo",
				"previous":   "Anterior"
			}
		}/*,
		'footerCallback': function() {
			var api = this.api();
			// Total over all pages
             total = api
				.column(1)
                .data()
                .reduce(function(a, b) {
                return soma(a, b);
                        }, 0);

                    // Total over this page
                    pageTotal = api
                        .column(1, {
                            page: 'current'
                        })
                        .data()
                        .reduce(function(a, b) {
                            return soma(a, b);
                        }, 0);

                    // Update footer
                    $(api.column(1).footer()).html(
                        'R$ ' + pageTotal + '<br> (R$ ' + total + ' total)'
                    );
        }*/
    })
  })
  /*function fmtMoney(n, c, d, t) {
            var m = (c = Math.abs(c) + 1 ? c : 2, d = d || ",", t = t || ".",
                    /(\d+)(?:(\.\d+)|)/.exec(n + "")),
                x = m[1].length > 3 ? m[1].length % 3 : 0;
            return (x ? m[1].substr(0, x) + t : "") + m[1].substr(x).replace(/(\d{3})(?=\d)/g,
                "$1" + t) + (c ? d + (+m[2] || 0).toFixed(c).substr(2) : "");
  };

  function soma(valor1, valor2) {
	var valor1 = parseFloat(valor1.toString().replace(/\./g, '').replace(',', '.'));
    var valor2 = parseFloat(valor2.toString().replace(/\./g, '').replace(',', '.'));
    var valor3 = ('' + ((valor1 || 0) + (valor2 || 0)));
    return fmtMoney(valor3);
  }*/
  
</script>