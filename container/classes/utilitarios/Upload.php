<?php
class Upload {
	var $arquivo_permissao;
	var $sDir;
	var $max_filesize;
	var $idioma;
	var $error = 0;
	var $file_type;
	var $file_name;
	var $file_size;
	var $arquivo;
	var $file_path;
	var $warning = 0;

	function Upload($permissao="" , $max_file = 12000000, $sDir = "arquivos", $idioma="portugues"){
		if (empty ($permissao)) $permissao = array ("text/plain","application/msword","application/vnd.ms-excel","application/vnd.ms-powerpoint","application/zip","application/pdf");
		$this->arquivo_permissao = $permissao;
		
		$this->max_filesize = $max_file;
		$this->sDir = $sDir;
		$this->idioma = $idioma;
	}
	
	function pegaExtensao($file) {
  	     $sExtensao = strrchr($file, '.');
  	     return $sExtensao;
  	}
	
	function geraNomeAleatorio(){
		$sConso = 'bcdfghjklmnpqrstvwxyzbcdfghjklmnpqrstvwxyz';
		$sVogal = 'aeiou';
		$sNum = '123456789';
		$sSenha = ' ';
	
		$y = strlen($sConso)-1; //conta o n� de caracteres da vari�vel $sConso
		$z = strlen($sVogal)-1; //conta o n� de caracteres da vari�vel $sVogal
		$r = strlen($sNum)-1; //conta o n� de caracteres da vari�vel $sNum
		
		for($x=0;$x<=1;$x++){
			$rand = rand(0,$y); //Fun�ao rand() - gera um valor rand�mico
			$rand1 = rand(0,$z);
			$rand2 = rand(0,$r);
			$str = substr($sConso,$rand,1); // substr() - retorna parte de uma string
			$str1 = substr($sVogal,$rand1,1);
			$str2 = substr($sNum,$rand2,1);
			$sSenha = $str.$str1.$str2;
		}
		
		return $sSenha;
	}

	function putFile($file,$nCodarquivoreferencia,$nCodarquivo){
		
		$this->file_type = strtok($_FILES[$file]['type'], ";");
		
		$this->file_size = $_FILES[$file]['size'];
		$this->temp = $_FILES[$file]['tmp_name'];  // upload para o diretorio temp
		
		//switch ($nCodarquivoreferencia){
		//	case "topo":
				//$sDiretorio = $this->sDir;
				// gerando nome aleat�rio pra n�o repeti��o de nome de arquivos
				$sNomeArquivo = $_FILES[$file]['name'];
				//$nCodarquivoreferencia."_" . $nCodarquivo . "_" . $this->geraNomeAleatorio() . $this->pegaExtensao($_FILES[$file]['name']);
				$this->file_name = $nCodarquivoreferencia."_" . $nCodarquivo . "_" . $this->geraNomeAleatorio() . $this->pegaExtensao($_FILES[$file]['name']) ;
				
			//	break;
		//}
		

		$this->file_path = $this->sDir;//$sDiretorio . "/";
		
		if (!in_array($this->file_type, $this->arquivo_permissao)) $this->Error(1);
		//echo "Atual" . $this->file_size;
		//echo "<br>Maximo" . $this->max_filesize;
		//die();
        //if ( ($this->file_size <= 0) || ($this->file_size > $this->max_filesize) ) $this->Error(2);
				
        if ($this->error == ""){
			$filename = basename ($this->file_name);
			
			if(!is_dir($this->sDir))
				die("Diret�rio inexistente");
			
            if (!empty ($this->sDir) )
				$this->arquivo = $this->sDir . "/" . $filename;
			else
				$this->arquivo = $filename;
	
			if (file_exists($this->arquivo)){
				$this->Error(5);
            }
			
			if(!is_uploaded_file($this->temp)) $this->Error(3);
			
			
			if ($nCodarquivoreferencia == 'topo'){
					$nLargura = "1060";
					$nAltura = "430";
					
					if($this->verificaTamanhodaImagem($_FILES[$file],$nLargura,$nAltura)){
						if(!@move_uploaded_file($this->temp,$this->arquivo)){
							$this->Error(4);
						}else{
							return $filename;
						}
					}
			}
		}
		else {
			return false;
        }
	}
	
	function verificaTamanhodaImagem($imagem,$nLargura,$nAltura){
		$this->nLargura = $nLargura;
		$this->nAltura =  $nAltura;
		
		switch($imagem['type']){
			case "image/jpeg":
			case "image/jpg":
				$img = imagecreatefromjpeg($imagem['tmp_name']);
			break;
			case "image/gif":
				$img = imagecreatefromgif($imagem['tmp_name']);
			break;
			case "image/png":
				$img = imagecreatefrompng($imagem['tmp_name']);
			break;
			default:
				$this->Error(4);
		}

		$x   = imagesx($img);
		if($this->nLargura  != $x){
			$this->Error(7);
		}
	 	
		$y   = imagesy($img);
		if($this->nAltura  != $y){
			$this->Error(6);
		}

		if($this->error){
			echo "<script>alert('".$this->error."');</script>";
			return false;
		}else{
			return true;
		}
		
		
	}


	function Error($op){
		if($this->idioma="portugues"){
			switch ($op){
				case 0: return; break;
				case 1: $this->error = "Erro 1: Este tipo de arquivo n�o tem permiss�o para upload : $this->file_type."; break;
				case 2: $this->error = "Erro 2: Erro no tamanho do arquivo: ".$this->file_size ." Kb. Tamanho Maximo � ".$this->max_filesize." !"; break;
				case 3: $this->error = "Erro 3: Ocorreu um erro na transf�rencia do arquivo: $this->file_name."; break;
				case 4: $this->error = "Erro 4: Ocorreu um erro na transf�rencia de $this->temp para $this->file_name."; break;
				case 5: $this->error = "Erro 5: J� existe um arquivo com este nome, renomeie o arquivo $this->file_name e tente novamente! ";break;
				case 6: $this->error = "Erro 6: A altura do arquivo deve ser $this->nAltura px";break;
				case 7: $this->error = "Erro 7: A largura do arquivo deve ser $this->nLargura px";break;
			}
			
		return $this->error;
		}
	}
}
?>