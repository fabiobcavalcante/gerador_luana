<?php


require_once "class.AOP_CodeParser.php";


/*
 * Constants correction found at: http://br.php.net/manual/en/ref.tokenizer.php
 *
 * T_ML_COMMENT does not exist in PHP 5.
 * The following three lines define it in order to
 * preserve backwards compatibility.
 *
 * The next two lines define the PHP 5 only T_DOC_COMMENT,
 * which we will mask as T_ML_COMMENT for PHP 4.
 */
if (!defined('T_ML_COMMENT')) {
   define('T_ML_COMMENT', T_COMMENT);
} else {
   define('T_DOC_COMMENT', T_ML_COMMENT);
}


class AOP_CodeCruncher
{
	function AOP_CodeCruncher()
	{
	}


	function process($str)
	{
		$result = "";
		$codeParser = new AOP_CodeParser($str);
		
		while(($token = $codeParser->nextToken()) !== null) {
        	// Internal characters (ie, (, {, }, )) do not have a token_name
			if (is_array($token)) {
        		$result .= AOP_CodeCruncher::analizeToken(
					$token, 
					$codeParser->getIndex(),
					$codeParser->getInit()
				);
			} elseif (is_string($token)) {
    	      	$result .= $token;
			}
		}

		return trim($result);
	}


	function analizeToken($token, $i, $init)
	{
		$result = "";

		switch (token_name((int) $token[0])) {
			case "T_WHITESPACE":
			case "T_ENCAPSED_AND_WHITESPACE":
				// New line between commands fixer
	            $result .= " ";
	            break;

			// [FIXME]: Implement a fix for this situation
			case "T_CONSTANT_ENCAPSED_STRING":
				// New line in string definition fixer
				$result .= $token[1];
				break;

			case "T_OPEN_TAG":
				if ($i == $init && is_array($token[1])) {
					// Last weird behavior of PHP Tokenizer... it puts
					// the first PHP command as part of the T_OPEN_TAG
					$result .= AOP_CodeCruncher::analizeToken($token[1], $i, $init);
				} else {
					$result .= trim($token[1]);
				}
				break;

			case "T_COMMENT":
			case "T_ML_COMMENT":
			case "T_DOC_COMMENT":
				// Do nothing
				break;

	        default:
	            $result .= $token[1];
	            break;
	    }

	    return $result;
	}
}

?>