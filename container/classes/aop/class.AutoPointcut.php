<?php

require_once "class.Pointcut.php";


class AutoPointcut extends Pointcut
{
	var $auto;
	
	
	function AutoPointcut($action, $class, $function, $auto, $nclass, $nfunction)
	{
		Pointcut::Pointcut($action, $class, $function, $nclass, $nfunction);
		
		// Defining Auto(s)
		$this->auto = (is_array($auto) ? $auto : split(",[ ]*", $auto));
	}
	
	
	function getAuto() { return $this->auto; }
	
	function hasAuto($v) { return in_array($v, $this->auto, true); }
}


?>