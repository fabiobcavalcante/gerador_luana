<?php
class FrontController{
	
	public function __construct(){
		$action = (isset($_GET['action'])) ? $_GET['action'] : "";
		$vAction = explode(".",$action);
		
		if(count($vAction) != 2){
			include_once('view/main/index.php');
		} else {
			$sClasse = $vAction[0];
			$sMetodo = $vAction[1];
			$oFactoryControle = new FactoryControle();
			$oControle = $oFactoryControle->getObject($sClasse);
			$oControle->$sMetodo();
		}
	}

}