<?php
/*
Classe Responsável por todas as operações no Banco de Dados
*/
class BancoDeDados implements IBancoDeDados{

	private $sSGBD;
	private $sBanco;
	private $sServidor;
	private $sUsuario;
	private $sSenha;
	private $oPDO;
	private $oStmt;
	
	
	public function __construct($sSGBD,$sBanco,$sServidor,$sUsuario,$sSenha){
		$this->sSGBD = $sSGBD;
		$this->sBanco = $sBanco;
		$this->sServidor = $sServidor;
		$this->sUsuario = $sUsuario;
		$this->sSenha = $sSenha;
	}
	
	//Faz a conexão com o Banco de Dados
	public function iniciaConexaoBanco(){
		if(!$this->PDO){
			//MySql
			//$this->oPDO = new PDO($this->sSGBD.":dbname=".$this->sBanco.";charset=utf8;host=".$this->sServidor,$this->sUsuario,$this->sSenha);
			
			//SqlServer
			$this->oPDO = new PDO ("$this->sSGBD:Server=$this->sServidor;Database=$this->sBanco","$this->sUsuario","$this->sSenha");
			if($this->oPDO){
				return true;
			}
			return false;
		}
		return true;
	}
	
	//Desfaz a conexão com o Banco de Dados
	public function terminaConexaoBanco(){
		if (!$this->oPDO){
			return 0;
		}
		else{
			unset($this->oStmt);
			unset($this->oPDO);
			return 1;
		}
	}
	
	//Insere um registro no tabela mandada como parametro
	public function insereRegistroNoBanco($sTabela,$sCampos,$sValores){
		if($this->iniciaConexaoBanco()){
			//die("INSERT INTO $sTabela($sCampos) VALUES ($sValores)");
			$this->oStmt = $this->oPDO->query("INSERT INTO $sTabela($sCampos) VALUES ($sValores)");
			$bResultado = ($this->oStmt) ? true : false;
			$nId = $this->oPDO->lastInsertId();
			$this->terminaConexaoBanco();
			
			//LogCaminho::escreverCaminho("INSERT INTO $sTabela($sCampos) VALUES ($sValores)");
			
			if($nId)
				return $nId;
			else 
				return $bResultado;
		}
	}
	
	//Recupera registro da tabela mandada com parametro *Variável $sComplemento
	//é opcional e se refere as clausulas adicionais como WHERE, ORDER BY, LIMIT
	//e etc.*
	public function recuperaRegistrosDoBanco($sCampos,$sTabelas,$sComplemento){
		if($this->iniciaConexaoBanco()){
			//die("SELECT $sCampos FROM $sTabelas $sComplemento");
			$sSql = "SELECT $sCampos FROM $sTabelas $sComplemento";
			if ($this->oStmt = $this->oPDO->query($sSql)){
				while ($oReg = $this->oStmt->fetchObject()){
					$vObjeto[] = $oReg;
				}
				$this->terminaConexaoBanco();
				
				//LogCaminho::escreverCaminho("SELECT $sCampos FROM $sTabelas $sComplemento");
				
				return $vObjeto;
			}
		}
	}
	
	//Atera o campo de um registro da tabela mandada como parametro
	public function alteraRegistrosDoBanco($sTabela,$sCampos,$sComplemento){
		if($this->iniciaConexaoBanco()){
			//die("UPDATE $sTabela SET $sCampos $sComplemento");
			$this->oStmt = $this->oPDO->query("UPDATE $sTabela SET $sCampos $sComplemento");
			$bResultado = ($this->oStmt) ? true : false;
			$this->terminaConexaoBanco();
			//LogCaminho::escreverCaminho("UPDATE $sTabela SET $sCampos $sComplemento");
			return $bResultado;
		}
	}
	
	//Exclui logicamente um registro da tabela
	public function excluiRegistrosDoBanco($sTabela,$sComplemento){
		if($this->iniciaConexaoBanco()){
			//die("UPDATE $sTabela SET ATIVO = 0 $sComplemento");
			$this->oStmt = $this->oPDO->query("UPDATE $sTabela SET ATIVO = 0 $sComplemento");
			$bResultado = ($this->oStmt) ? true : false;
			$this->terminaConexaoBanco();
			//LogCaminho::escreverCaminho("UPDATE $sTabela SET ATIVO = 0 $sComplemento");
			return $bResultado;
		}
	}
	
	//Exclui fisicamente um registro da tabela
	public function excluiFisicamenteRegistrosDoBanco($sTabela,$sComplemento){
		if($this->iniciaConexaoBanco()){
			$this->oStmt = $this->oPDO->query("DELETE FROM $sTabela $sComplemento");
			$bResultado = ($this->oStmt) ? true : false;
			$this->terminaConexaoBanco();
			//LogCaminho::escreverCaminho("DELETE FROM $sTabela $sComplemento");
			return $bResultado;
		}
	}
}
?>