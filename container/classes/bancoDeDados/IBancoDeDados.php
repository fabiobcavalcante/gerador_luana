<?php
/*
Interface do Componente bancoDeDados
*/
interface IBancoDeDados{

	public function iniciaConexaoBanco();
	
	//Desfaz a conex�o com o Banco de Dados
	public function terminaConexaoBanco();
	
	//Insere um registro no tabela mandada como parametro
	public function insereRegistroNoBanco($sTabela,$sCampos,$sValores);
	
	//Recupera registro da tabela mandada com parametro *Vari�vel $sComplemento
	//� opcional e se refere as clausulas adicionais como WHERE, ORDER BY, LIMIT
	//e etc.*
	public function recuperaRegistrosDoBanco($sCampos,$sTabelas,$sComplemento);
	
	//Atera o campo de um registro da tabela mandada como parametro
	public function alteraRegistrosDoBanco($sTabela,$sCampos,$sComplemento);
	
	//Exclui logicamente um registro da tabela
	public function excluiRegistrosDoBanco($sTabela,$sComplemento);
	
	//Exclui fisicamente um registro da tabela
	public function excluiFisicamenteRegistrosDoBanco($sTabela,$sComplemento);
}
?>