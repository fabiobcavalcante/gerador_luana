<?php
interface IPersistencia{

	public function inserir();
 	
 	public function alterar();
 	
 	public function presente();
 	
 	public function consultar($sTabelas, $sCampos, $sComplemento);
 	
 	public function excluir();
 	
	public function excluirFisicamente();
	
	
}
?>