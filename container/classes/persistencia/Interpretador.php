<?php
class Interpretador{
	
	/**
	 * Variavel Descritor
	 * @var Descritor
	 */
	private $oDescritor;
	/**
	 * Variavel Objeto
	 * @var Object
	 */
	private $oObjeto;
	
	/**
	 * M�todo Construtor retornando um objeto Interpretador com o atributo $oDescritor setado
	 * @return Interpretador
	 * @param Object $oObjeto
	 */
	public function __construct($oObjeto){
		$this->recuperarDescricaoClasse($oObjeto);
		$this->oObjeto = $oObjeto;
	}
	
	/**
	 * M�todo para recuperar descri��o de persistencia
	 * @return void
	 * @param Object $oObjeto
	 */
	private function recuperarDescricaoClasse($oObjeto){
		$voCampoTabela = array();
		$voAtributoClasse = array();
		$this->oDescritor = new Descritor();
		$oReflectionObject = new ReflectionObject($oObjeto);
		
		$this->oDescritor->setClasse($oReflectionObject->getName());
		$sComentarioClasse = $oReflectionObject->getDocComment();
		
		$vComentarioClasse = explode("\n",$sComentarioClasse);
		
		foreach($vComentarioClasse as $sLinhaComentario){
			$sLinhaComentario = trim($sLinhaComentario);
			if(substr($sLinhaComentario,0,1) == "*"){
				$sLinhaComentario = substr($sLinhaComentario,1,strlen($sLinhaComentario));
				$sLinhaComentario = trim($sLinhaComentario);
				
				$vLinhaComentario = explode(" ",$sLinhaComentario);
				
				foreach($vLinhaComentario as $sParteComentario){
					$sParteComentario = trim($sParteComentario);
					
					if($bAchouTabela){
						$this->oDescritor->setTabela($sParteComentario);
						$bAchouTabela = false;
					}
					if($sParteComentario == "@tabela")
						$bAchouTabela = true;
						
					if($bAchouSGBD){
						$this->oDescritor->setSGBD($sParteComentario);
						$bAchouSGBD = false;
					}
					if($sParteComentario == "@SGBD")
						$bAchouSGBD = true;
				}
			}
		}
		
		
		$voReflectionProperty = $oReflectionObject->getProperties();
		foreach($voReflectionProperty as $oReflectionProperty){
			$oAtributoClasse = new AtributoClasse();
			$oCampoTabela = new CampoTabela();
			
			$oAtributoClasse->setNome($oReflectionProperty->getName());
			
			$sComentarioAtributo = $oReflectionProperty->getDocComment();
			$vComentarioAtributo = explode("\n",$sComentarioAtributo);
			foreach($vComentarioAtributo as $sLinhaComentario){
				$sLinhaComentario = trim($sLinhaComentario);
				if(substr($sLinhaComentario,0,1) == "*"){
					$sLinhaComentario = substr($sLinhaComentario,1,strlen($sLinhaComentario));
					$sLinhaComentario = trim($sLinhaComentario);
					$vLinhaComentario = explode(" ",$sLinhaComentario);
					foreach($vLinhaComentario as $sParteComentario){
						$sParteComentario = trim($sParteComentario);
						if($bAchouVar){
							$oCampoTabela->setTipo($sParteComentario);
							$bAchouVar = false;
						}
						if($sParteComentario == "@var")
							$bAchouVar = true;
							
						if($bAchouCampo){
							$oCampoTabela->setNome($sParteComentario);
							$bAchouCampo = false;
						}
						if($sParteComentario == "@campo")
							$bAchouCampo = true;
							
						if($bAchouPrimario){
							$oCampoTabela->setCampoPrimario(($sParteComentario == "true") ? true : false);
							$bAchouPrimario = false;
						}
						if($sParteComentario == "@primario")
							$bAchouPrimario = true;
							
						if($bAchouAuto){
							$oCampoTabela->setAuto(($sParteComentario == "true") ? true : false);
							$bAchouAuto = false;
						}
						if($sParteComentario == "@auto-increment")
							$bAchouAuto = true;
							
						if($bAchouNulo){
							$oCampoTabela->setNulo(($sParteComentario == "true") ? true : false);
							$bAchouNulo = false;
						}
						if($sParteComentario == "@nulo")
							$bAchouNulo = true;
					}
				}
			}
			if($oCampoTabela->getNome() && $oCampoTabela->getTipo()){
				array_push($voAtributoClasse,$oAtributoClasse);
				array_push($voCampoTabela,$oCampoTabela);
			}	
		}
		$this->oDescritor->setAtributoClasse($voAtributoClasse);
		$this->oDescritor->setCampoTabela($voCampoTabela);
		
	}
	/**
	 * M�todo para recuperar o SGBD onde o objeto ser� persistido
	 *
	 * @return String
	 */
	public function recuperarSGBD(){
		if($this->oDescritor)
			return $this->oDescritor->getSGBD();
		return false;
	}
	/**
	 * M�todo para recuperar a tabela no banco do objeto a ser persistido
	 *
	 * @return String
	 */
	public function recuperarTabela(){
		if($this->oDescritor)
			return $this->oDescritor->getTabela();
		return false;
	}
	/**
	 * M�todo para recuperar a string de campos no banco de dados correspondente ao objeto a ser persistido
	 *
	 * @return String
	 */
	public function recuperarCamposInserir(){
		if($this->oDescritor){
			foreach($this->oDescritor->getCampoTabela() as $oCampoTabela){
				if(!$oCampoTabela->getAuto()){
					$sCamposInserir .= $sVirgula.$oCampoTabela->getNome();
					$sVirgula = ",";
				}
			}
			return $sCamposInserir;
		}
		return "";
	}
	/**
	 * M�todo para recuperar a string de valores de campos no banco de dados correspondentes ao objeto a ser persistido
	 *
	 * @return String
	 */
	public function recuperarCamposInserirValores(){
		if($this->oDescritor){
			foreach($this->oDescritor->getCampoTabela() as $nIndice => $oCampoTabela){
				if(!$oCampoTabela->getAuto()){
					$voAtributoClasse = $this->oDescritor->getAtributoClasse(); 
					$oAtributoClasse = $voAtributoClasse[$nIndice];
					$sNomeMetodo = "get".substr($oAtributoClasse->getNome(),1,strlen($oAtributoClasse->getNome())-1);
					$sValor = $this->oObjeto->$sNomeMetodo();
					
					
					$sAspas = "";
					if($oCampoTabela->getTipo() == "String"){
						if(!$sValor){
							$sAspas = "";
							$sValor = "null";
						}else{
							$sAspas = "'";
						}
					}elseif($oCampoTabela->getTipo() == "data"){
						if(!$sValor){
							$sAspas = "";
							$sValor = "null";
						}else{
							$sAspas = "'";
						}

					}elseif($oCampoTabela->getTipo() == "number" && $sValor === ""){
						$sValor = "null";
					
					}elseif($oCampoTabela->getTipo() == "boolean" && $sValor == ""){
						$sValor = "null";
					}
					
					$sCamposInserirValores .= $sVirgula.$sAspas.$sValor.$sAspas;
					
					$sVirgula = ",";
				}
			}
			return $sCamposInserirValores;
		}
		return "";
	}
	/**
	 * M�todo para recuperar a string de valores de campos no banco de dados correspondentes ao objeto a ser persistido
	 *
	 * @return String
	 */
	public function recuperarCamposAlterar(){
		if($this->oDescritor){
			foreach($this->oDescritor->getCampoTabela() as $nIndice => $oCampoTabela){
				$voAtributoClasse = $this->oDescritor->getAtributoClasse(); 
				$oAtributoClasse = $voAtributoClasse[$nIndice];
				$sNomeMetodo = "get".substr($oAtributoClasse->getNome(),1,strlen($oAtributoClasse->getNome())-1);
				$sValor = $this->oObjeto->$sNomeMetodo();
				
				
				$sAspas = "";
					if($oCampoTabela->getTipo() == "String"){
						if(!$sValor){
							$sAspas = "";
							$sValor = "null";
						}else{
							$sAspas = "'";
						}
					}elseif($oCampoTabela->getTipo() == "data"){
						if(!$sValor){
							$sAspas = "";
							$sValor = "null";
						}else{
							$sAspas = "'";
						}

					}elseif($oCampoTabela->getTipo() == "number" && $sValor === ""){
						$sValor = "null";
					
					}elseif($oCampoTabela->getTipo() == "boolean" && $sValor == ""){
						$sValor = "null";
					}
					
				if(!$oCampoTabela->getCampoPrimario() && $sValor != NULL && $sValor != ""){
					$sCamposAlterar .= $sVirgula.$oCampoTabela->getNome()."=".$sAspas.$sValor.$sAspas;
					$sVirgula = ",";
				}
			}
			return $sCamposAlterar;
		}
		return "";
	}
	
	/**
	 * M�todo para recuperar a string de valores de campos no banco de dados correspondentes ao objeto a ser persistido
	 *
	 * @return String
	 */
	public function recuperarWhereCamposPrimario(){
		if($this->oDescritor){
			$sWhereCamposPrimario = "WHERE ";
			foreach($this->oDescritor->getCampoTabela() as $nIndice => $oCampoTabela){
				$voAtributoClasse = $this->oDescritor->getAtributoClasse(); 
				$oAtributoClasse = $voAtributoClasse[$nIndice];
				$sNomeMetodo = "get".substr($oAtributoClasse->getNome(),1,strlen($oAtributoClasse->getNome())-1);
				$sValor = $this->oObjeto->$sNomeMetodo();
				$sAspas = "";
				if($oCampoTabela->getTipo() == "String")
					$sAspas = "'";
				
				if($oCampoTabela->getCampoPrimario()){
					$sWhereCamposPrimario .= $sAnd.$oCampoTabela->getNome()."=".$sAspas.$sValor.$sAspas;
					$sAnd = " AND ";
				}
			}
			return $sWhereCamposPrimario;
		}
		return "";
	}
	/**
	 * Recupera vetor de objetos de acordo com uma consulta no banco
	 *
	 * 
	 */
	public function recuperarVetorObjetos($rsBanco){
		if($this->oDescritor && $rsBanco){
			$voObjeto = array();
			foreach($rsBanco as $oReg){
				$sNomeClasse = $this->oDescritor->getClasse();
				$oObj = new $sNomeClasse();
				foreach($this->oDescritor->getCampoTabela() as $nIndice => $oCampoTabela){
					$voAtributoClasse = $this->oDescritor->getAtributoClasse(); 
					$oAtributoClasse = $voAtributoClasse[$nIndice];
					$sNomeMetodo = "set".substr($oAtributoClasse->getNome(),1,strlen($oAtributoClasse->getNome())-1);
					$sNomeCampo = $oCampoTabela->getNome();
					$oObj->$sNomeMetodo($oReg->$sNomeCampo);
					
				}
				array_push($voObjeto,$oObj);
			}
			return $voObjeto;
		}
		return false;
	}
	
	public function getDescritor(){
		return $this->oDescritor;
	}
	
	public function getObjeto(){
		return $this->oObjeto;
	}
	
}

?>