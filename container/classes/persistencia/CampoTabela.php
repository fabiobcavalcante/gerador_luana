<?php
/**
 * Representa��o de um campo de tabela de banco de dados
 * @author Rodrigo Oliveira de Medeiros
 * @package persistencia
 * @since 12/11/2008
 * @version 12/11/2008
 */
class CampoTabela{
	
	/**
	 * Nome do campo
	 * @var String
	 */
	private $sNome;
	/**
	 * Tipo do Campo
	 * @var String
	 */
	private $sTipo;
	/**
	 * Informa se � campo prim�rio
	 * @var boolean
	 */
	private $bCampoPrimario;
	/**
	 * Informa se � campo auto-increment
	 * @var boolean
	 */
	private $bAuto;
	/**
	 * Informa se � campo nulo
	 * @var boolean
	 */
	private $bNulo;
		
	/**
	 * M�todo Construtor
	 * @return CampoTabela
	 */
	public function __construct(){
		
		
	}
	
	/**
	 * M�todo para recuperar o atributo $sNome
	 * @return String 
	 */
	public function getNome(){
		return $this->sNome;
		
	}
	/**
	 * M�todo para setar o atributo $sNome
	 * @return void 
	 * @param String $sNome
	 */
	public function setNome($sNome){
		$this->sNome = $sNome;
	}
	/**
	 * M�todo para recuperar o atributo $sTipo
	 * @return String 
	 */
	public function getTipo(){
		return $this->sTipo;
		
	}
	/**
	 * M�todo para setar o atributo $sTipo
	 * @return void 
	 * @param String $sTipo
	 */
	public function setTipo($sTipo){
		$this->sTipo = $sTipo;
	}
	/**
	 * M�todo para recuperar o atributo $bCampoPrimario
	 * @return boolean 
	 */
	public function getCampoPrimario(){
		return $this->bCampoPrimario;
		
	}
	/**
	 * M�todo para setar o atributo $bCampoPrimario
	 * @return void 
	 * @param String $bCampoPrimario
	 */
	public function setCampoPrimario($bCampoPrimario){
		$this->bCampoPrimario = $bCampoPrimario;
	}
	/**
	 * M�todo para recuperar o atributo $bAuto
	 * @return boolean 
	 */
	public function getAuto(){
		return $this->bAuto;
		
	}
	/**
	 * M�todo para setar o atributo $bAuto
	 * @return void 
	 * @param boolean $bAuto
	 */
	public function setAuto($bAuto){
		$this->bAuto = $bAuto;
	}
	

	/**
	 * M�todo para recuperar o atributo $bNulo
	 * @return boolean 
	 */
	public function getNulo(){
		return $this->bNulo;
		
	}
	/**
	 * M�todo para setar o atributo $bAuto
	 * @return void 
	 * @param boolean $bAuto
	 */
	public function setNulo($bNulo){
		$this->bNulo = $bNulo;
	}
	
	
}
?>