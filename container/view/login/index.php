<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Sistema de Gratifica&ccedil;&atilde;o de Desempenho de Gest&atilde;o</title>
  <link href="css2/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="css2/bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css2/style2.css" rel="stylesheet" type="text/css">
    

</head>

<!-- -->
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
		  
          <a class="navbar-brand" href="#"><img src="/gdg2/imagens/logo.png" height="60px" style="margin-top: -20px;" ></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <form class="navbar-form navbar-right" action="?action=Login.processaFormulario&sOP=Logon" method="post">
            <div class="form-group">
			 <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					<input type="text" placeholder="Matricula" class="form-control" name="fLogin"> 
			</div>
            </div>
            
			<div class="form-group">
			<div class="input-group">
				  <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
				  <input type="password" placeholder="Senha" class="form-control" name="fSenha">
            </div>
			</div>            
			
			<button type="submit" class="btn btn-success btn-md ">Entrar</button>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

	
<div class="container">

        <div class="col-md-12" style="margin-top:120px; ">

                <div class="row carousel-holder">

                    <div class="col-md-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img class="slide-image" src="/gdg2/imagens/desempenho.jpg" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="/gdg2/imagens/desempenho.jpg" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="/gdg2/imagens/desempenho.jpg" alt="">
                                </div>
                            </div>
                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>
                    </div>

                </div>

                </div>

		</div>


<!-- <p align="center" style="margin-top:90px;">.</p> -->
   

   
	<div class="container" style="color:#33333; padding:20px;">
        <!-- Title -->

        <!-- /.row -->
      <!-- Example row of columns -->
      <div class="row" >   <h4 style="margin-left:15px;">BASE LEGAL</h3>
        <div class="col-md-4">
          <h3>Lei n� 6.563,</h2>
          <p align="justify">DE 1&ordm; DE AGOSTO DE 2003. Disp&otilde;e sobre a reestrutura&ccedil;&atilde;o organizacional da Secretaria  Executiva de Estado de Administra&ccedil;&atilde;o - SEAD, e d&aacute; outras  provid&ecirc;ncias.</p>
          <p><a class="btn btn-success btn-sm" href="#" role="button">Ver Detalhes &raquo;</a></p>
        </div>
        <div class="col-md-4">
          <h3>Decreto n� 563,</h2>
          <p align="justify">De 5-11-2007, publicado no DOE n�. 31042 de 07/11/2007. Regulamenta os arts. 12-B e 12-C da Lei n� 6.563, de 1� de agosto de 2003, que disp�em
sobre a Gratifica��o de Desempenho de Gest�o.</p>
          <p><a class="btn btn-success btn-sm" href="#" role="button">Ver Detalhes &raquo;</a></p>
       </div>
        <div class="col-md-4">
          <h3>Decreto n� 358,</h2>
          <p align="justify">DE 28 DE FEVEREIRO DE 2012 Disp�e sobre a altera��o do Decreto n� 563, de 5 de novembro de 2007.</p>
          <p><a class="btn  btn-success btn-sm" href="#" role="button">Ver Detalhes &raquo;</a></p>
        </div>
      </div>

   

    </div> <!-- /container -->
     
	 
	     <footer class="footer" >
      		
					<div class="container">
				
	  
						<address>
						  <strong>Desenvolvido por, DITI-SEPLAN</strong><br>
						  Rua Boaventura da Silva, 401/403<br>
						  Bel&eacute;m, PA 66.053-050 <br>
						 <!-- <abbr title="Telefone">T:</abbr> (91)  3204-7484-->
					
						</address>
		
					</div>
			
	  
		</footer>
	
	
	  
	   
	<script src="js/jquery/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
    <div align="center"><?php require_once("controle/includes/mensagem.php")?></div>
</body>
</html>
