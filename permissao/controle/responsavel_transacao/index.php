<?
 /*@var $oTransacaoModulo TransacaoModulo*/
 $oTransacaoModulo = $_REQUEST['oTransacaoModulo'];
 $voResponsavelTransacao = ($oTransacaoModulo && $oTransacaoModulo->getResponsaveis()) ? $oTransacaoModulo->getResponsaveis() :  "";
 ?>
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <title>T�tulo</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 <style type="text/css">
 <!--
 body,td,th {
 	font-family: Verdana, Arial, Helvetica, sans-serif;
 	font-size: 10px;
 	color: #000000;
 }
 .style1 {color:#006633;}
 
 
 -->
 </style>

 <script language="javascript" src="js/jquery/jquery.js"></script>
 <script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
 <script language="javascript" src="js/producao.js"></script>
 
 <!-- InstanceBeginEditable name="head" -->
  <style type="text/css" title="currentStyle">
	@import "css/jquery-datatables.css";
	@import "css/controle.css";
  </style>

 <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		oTable = $('#lista').dataTable({
			"bJQueryUI": true,
			"aoColumnDefs": [
            { 
			"bSortable": false, "aTargets": [ 0 ],
			"bSearchable": false, "aTargets": [ 0 ]
			}
        ]
		});
	} );
 </script>
 <!-- InstanceEndEditable -->
 </head>
 
 <body>
 <? include("controle/includes/mensagem.php")?>
     <table class="TabelaPai" width="750" border="0" align="center" cellpadding="1" cellspacing="1">
   <? include("controle/includes/topo.php")?>
   <tr bgcolor="#ECE9D8">
     <td colspan="2" class="CelulaLinhaAbaixo"><? include("controle/includes/menu.php")?></td>
   </tr>
   <tr>
     <td colspan="2" class="Corpo"><table width="100%" border="0" cellpadding="2" cellspacing="2">
 		<tr>
                    <td class="CelulaMigalhaLink"><!-- InstanceBeginEditable name="Migalha" --><strong>Voc� est� aqui:</strong> <a href="index.php">Home</a> > <a href="index.php?action=TransacaoModulo.preparaLista&nCodModulo=<?php echo$oTransacaoModulo->getCodModulo()?>">Gerenciar Transa��es do M�dulo <?php echo$oTransacaoModulo->getModulo()->getDescricao()?></a> > <strong>Gerenciar Respons�veis pela transa��o <?php echo$oTransacaoModulo->getDescricao()?></strong><!-- InstanceEndEditable --></td>
 		</tr>
 		<tr>
                    <td><!-- InstanceBeginEditable name="Titulo Pagina" --><h2 class="TituloPagina" style="font-size: 16px">Gerenciar Respons�veis pela transa��o <?php echo$oTransacaoModulo->getModulo()->getDescricao()?> - <?php echo$oTransacaoModulo->getDescricao()?></h2><!-- InstanceEndEditable --></td>
 		</tr>
 		<tr>
 			<td><!-- InstanceBeginEditable name="Conteudo" -->
 			<form method="post" action="" name="formResponsavelTransacao" id="formResponsavelTransacao">
 			<table width="100%" cellpadding="2" cellspacing="2" border="0">
 				<tr>
 					<td align="left"><select class="Acoes" name="acoes" id="acoesResponsavelTransacao" onChange="JavaScript: submeteForm('ResponsavelTransacao')">
 						<option value="" selected>A��es...</option>
                                                <option value="?action=ResponsavelTransacao.preparaFormulario&sOP=Cadastrar&nCodTransacaoModulo=<?php echo$oTransacaoModulo->getCodTransacaoModulo()?>" lang="0">Cadastrar novo Respons�vel</option>
 						<option value="?action=ResponsavelTransacao.preparaFormulario&sOP=Alterar" lang="1">Alterar Respons�vel Transa��o selecionado</option>
 						<option value="?action=ResponsavelTransacao.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Respons�vel Transa��o selecionado</option>
                                                <option value="?action=ResponsavelTransacao.processaFormulario&sOP=Excluir&nCodTransacaoModulo=<?php echo$oTransacaoModulo->getCodTransacaoModulo()?>" lang="2">Excluir Respons�vel(is) selecionado(s)</option>
 					</select></td>
                                         <td align="right"><a href="javascript:window.print();"><img id="btn_acao" border="0" src="imagens/btn_impressao.gif"></a></td>
 				</tr>
 			</table><br/>
 			<table id="lista" align="left" width="100%" class="TabelaAdministracao" cellpadding="0" cellspacing="0">
 			<? if(is_array($voResponsavelTransacao)){?>
 				<thead>
 				<tr>
 					<th class="Titulo" width="1%"><img onclick="javascript: marcarTodosCheckBoxFormulario('ResponsavelTransacao')" src="controle/imagens/checkbox.gif" width="16" height="16"></th>
 					<th class='Titulo'>C�digo</th>
					<th class='Titulo'>Respons�vel</th>
					<th class='Titulo'>Opera��o</th>

 				</tr>
 				</thead>
 				<tbody>
 				<? foreach($voResponsavelTransacao as $oResponsavelTransacao){?>
 				<tr>
 					<td><input onClick="JavaScript: atualizaAcoes('ResponsavelTransacao')" type="checkbox" value="<?php echo$oResponsavelTransacao->getCodResponsavelTransacao()?>" name="fCodResponsavelTransacao[]"/></td>
 					<td><?php echo $oResponsavelTransacao->getCodResponsavelTransacao()?></td>
					<td><?php echo $oResponsavelTransacao->getResponsavel()?></td>
					<td><?php echo $oResponsavelTransacao->getOperacao()?></td>

 				</tr>
 				<? }?>
 				</tbody>
 				<tfoot>
 				<tr>
  					<td colspan="10"></td>
  				</tr>
  				</tfoot>
 			<? } else {?>
 				<tfoot>
 				<tr>
 					<td class="Conteudo">N�o h� Respons�veis cadastrados para essa Transa��o!</td>
 				</tr>
 				</tfoot>
 			<? }//if(count($voResponsavelTransacao)){?>
 			</table>
 			</form>
 			<script language="javascript">
 				atualizaAcoes('ResponsavelTransacao');
 			</script>
 			<!-- InstanceEndEditable --></td>
 		</tr>
 	</table></td>
   </tr>
 </table>
 </body>
 <!-- InstanceEnd --></html>