<?
 $voModulo = $_REQUEST['voModulo'];
 ?>
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <title>T�tulo</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 <style type="text/css">
 <!--
 body,td,th {
 	font-family: Verdana, Arial, Helvetica, sans-serif;
 	font-size: 10px;
 	color: #000000;
 }
 .style1 {color:#006633;}
 
 
 -->
 </style>

 <script language="javascript" src="js/jquery/jquery.js"></script>
 <script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
 <script language="javascript" src="js/producao.js"></script>
 
 <!-- InstanceBeginEditable name="head" -->
  <style type="text/css" title="currentStyle">
	@import "css/jquery-datatables.css";
	@import "css/controle.css";
  </style>

 <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		oTable = $('#lista').dataTable({
			"bJQueryUI": true,
			"aoColumnDefs": [
            { 
			"bSortable": false, "aTargets": [ 0 ],
			"bSearchable": false, "aTargets": [ 0 ]
			}
        ]
		});
	} );
 </script>
 <!-- InstanceEndEditable -->
 </head>
 
 <body>
 <? include("controle/includes/mensagem.php")?>
 <table class="TabelaPai" width="750" border="0" align="center" cellpadding="1" cellspacing="1">
   <? include("controle/includes/topo.php")?>
   <tr bgcolor="#ECE9D8">
     <td colspan="2" class="CelulaLinhaAbaixo"><? include("controle/includes/menu.php")?></td>
   </tr>
   <tr>
     <td colspan="2" class="Corpo"><table width="100%" border="0" cellpadding="2" cellspacing="2">
 		<tr>
 			<td class="CelulaMigalhaLink"><!-- InstanceBeginEditable name="Migalha" --><strong>Voc� est� aqui:</strong> <a href="index.php">Home</a> > <strong>Gerenciar M�dulos</strong><!-- InstanceEndEditable --></td>
 		</tr>
 		<tr>
 			<td><!-- InstanceBeginEditable name="Titulo Pagina" --><h2 class="TituloPagina">Gerenciar M�dulos</h2><!-- InstanceEndEditable --></td>
 		</tr>
 		<tr>
 			<td><!-- InstanceBeginEditable name="Conteudo" -->
 			
 			<form method="post" action="" name="formModulo" id="formModulo">
 			<table width="100%" cellpadding="2" cellspacing="2" border="0">
 				<tr>
 					<td align="left"><select class="Acoes" name="acoes" id="acoesModulo" onChange="JavaScript: submeteForm('Modulo')">
 						<option value="" selected>A��es...</option>
 						<option value="?action=Modulo.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo M�dulo</option>
 						<option value="?action=Modulo.preparaFormulario&sOP=Alterar" lang="1">Alterar M�dulo selecionado</option>
 						<option value="?action=Modulo.preparaFormulario&sOP=Detalhar" lang="1">Detalhar M�dulo selecionado</option>
                        <option value="?action=TransacaoModulo.preparaLista" lang="1">Gerenciar Transa��es do M�dulo selecionado</option>
 						<option value="?action=Modulo.processaFormulario&sOP=Excluir" lang="2">Excluir M�dulo(s) selecionado(s)</option>
 					</select></td>
 				</tr>
 			</table><br/>
 			<table id="lista" align="left" width="100%" class="TabelaAdministracao" cellpadding="0" cellspacing="0">
 			<? if(is_array($voModulo)){?>
 				<thead>
 				<tr>
                                    <th class="Titulo" width="1%"><img onclick="javascript: marcarTodosCheckBoxFormulario('Modulo')" src="controle/imagens/checkbox.gif" width="16" height="16" alt="Marcar Todos"></th>
 					<th width="5%" class='Titulo'>C�d</th>
					<th class='Titulo'>Descri��o</th>
 				</tr>
 				</thead>
 				<tbody>
 				<? foreach($voModulo as $oModulo){?>
 				<tr>
 					<td class="Conteudo"><input onClick="JavaScript: atualizaAcoes('Modulo')" type="checkbox" value="<?php echo$oModulo->getCodModulo()?>" name="fCodModulo[]"/></td>
 					<td class='Conteudo'><?php echo $oModulo->getCodModulo()?></td>
					<td class='Conteudo'>
                                            
                                            <?
                                            $voTransacao = $oModulo->getTransacaoModulo();
                                            if(is_array($voTransacao)){
                                                
                                            ?>
                                            <strong><a id="a_transacao_<?php echo$oModulo->getCodModulo()?>" style="text-decoration: none;" href="javascript: void(0);" onclick="MostraEscondeArvore('ul_transacao_<?php echo$oModulo->getCodModulo()?>');">+</a></strong> <?php echo$oModulo->getDescricao()?>
                                            <ul id="ul_transacao_<?php echo$oModulo->getCodModulo()?>" style="display: none; list-style-type: none;">
                                            <? foreach($voTransacao as $oTransacao){?>
                                                    <li>
                                                        
                                                        <?
                                                        $voResponsavel = $oTransacao->getResponsaveis();
                                                        if(is_array($voResponsavel)){
                                                        ?>
                                                        <strong><a id="a_responsavel_<?php echo$oTransacao->getCodTransacaoModulo()?>" style="text-decoration: none;" href="javascript: void(0);" onclick="MostraEscondeArvore('ul_responsavel_<?php echo$oTransacao->getCodTransacaoModulo()?>');">+</a></strong> <?php echo$oTransacao->getDescricao()?>
                                                            <ul id="ul_responsavel_<?php echo$oTransacao->getCodTransacaoModulo()?>" style="display: none;">
                                                                <? foreach($voResponsavel as $oResponsavel){?>
                                                                <li><?php echo$oTransacao->getCodTransacaoModulo() . " - " . $oResponsavel->getResponsavel()?> - <?php echo$oResponsavel->getOperacao()?></li>
                                                                <? }?>
                                                            </ul>
                                                        <?
                                                        } else {
                                                            echo $oTransacao->getCodTransacaoModulo() . " - " . $oTransacao->getDescricao();
                                                        }
                                                        ?>
                                                    </li>
                                            <? }?>


                                            </ul>
                                            <?
                                            } else {
                                                echo $oModulo->getCodModulo() . " - " . $oModulo->getDescricao();

                                            }
                                            ?>
                                        </td>
 				</tr>
 				<? }?>
 				</tbody>
 				<tfoot>
 				<tr>
  					<td colspan="10"></td>
  				</tr>
  				</tfoot>
 			<? } else {?>
 				<tfoot>
 				<tr>
 					<td class="Conteudo">N�o h� M�dulos cadastrados!</td>
 				</tr>
 				</tfoot>
 			<? }//if(count($voModulo)){?>
 			</table>
 			</form>
 			<script language="javascript">
 				atualizaAcoes('Modulo');
 			</script>
 			<!-- InstanceEndEditable --></td>
 		</tr>
 	</table></td>
   </tr>
 </table>
 </body>
 <!-- InstanceEnd --></html>