<?
 $oModulo = $_REQUEST['oModulo'];
 
 $voTransacaoModulo = $oModulo->getTransacaoModulo();
 ?>
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <title>T�tulo</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 <style type="text/css">
 <!--
 body,td,th {
 	font-family: Verdana, Arial, Helvetica, sans-serif;
 	font-size: 10px;
 	color: #000000;
 }
 .style1 {color:#006633;}
 
 
 -->
 </style>

 <script language="javascript" src="js/jquery/jquery.js"></script>
 <script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
 <script language="javascript" src="js/producao.js"></script>
 
 <!-- InstanceBeginEditable name="head" -->
  <style type="text/css" title="currentStyle">
	@import "css/jquery-datatables.css";
	@import "css/controle.css";
  </style>

 <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		oTable = $('#lista').dataTable({
			"bJQueryUI": true,
			"aoColumnDefs": [
            { 
			"bSortable": false, "aTargets": [ 0 ],
			"bSearchable": false, "aTargets": [ 0 ]
			}
        ]
		});
	} );
 </script>
 <!-- InstanceEndEditable -->
 </head>
 
 <body>
 <? include("controle/includes/mensagem.php")?>
     <table class="TabelaPai" width="750" border="0" align="center" cellpadding="1" cellspacing="1">
   <? include("controle/includes/topo.php")?>
   <tr bgcolor="#ECE9D8">
     <td colspan="2" class="CelulaLinhaAbaixo"><? include("controle/includes/menu.php")?></td>
   </tr>
   <tr>
     <td colspan="2" class="Corpo"><table width="100%" border="0" cellpadding="2" cellspacing="2">
 		<tr>
 			<td class="CelulaMigalhaLink"><!-- InstanceBeginEditable name="Migalha" --><strong>Voc� est� aqui:</strong> <a href="index.php">Home</a> > <a href="index.php?action=Modulo.preparaLista">Gerenciar M�dulos</a> > <strong>Gerenciar transa��es do m�dulo <?php echo$oModulo->getDescricao()?></strong><!-- InstanceEndEditable --></td>
 		</tr>
 		<tr>
 			<td><!-- InstanceBeginEditable name="Titulo Pagina" --><h2 class="TituloPagina">Gerenciar transa��es do m�dulo <?php echo$oModulo->getDescricao()?></h2><!-- InstanceEndEditable --></td>
 		</tr>
 		<tr>
 			<td><!-- InstanceBeginEditable name="Conteudo" -->
 			<form method="post" action="" name="formTransacaoModulo" id="formTransacaoModulo">
 			<table width="100%" cellpadding="2" cellspacing="2" border="0">
 				<tr>
 					<td align="left"><select class="Acoes" name="acoes" id="acoesTransacaoModulo" onChange="JavaScript: submeteForm('TransacaoModulo')">
 						<option value="" selected>A��es...</option>
 						<option value="?action=TransacaoModulo.preparaFormulario&sOP=Cadastrar&nCodModulo=<?php echo$oModulo->getCodModulo()?>" lang="0">Cadastrar nova Transa��o</option>
 						<option value="?action=TransacaoModulo.preparaFormulario&sOP=Alterar&nCodModulo=<?php echo$oModulo->getCodModulo()?>" lang="1">Alterar Transa��o selecionada</option>
 						<option value="?action=TransacaoModulo.preparaFormulario&sOP=Detalhar&nCodModulo=<?php echo$oModulo->getCodModulo()?>" lang="1">Detalhar Transa��o selecionada</option>
                        <option value="?action=ResponsavelTransacao.preparaLista" lang="1">Gerenciar Respons�veis pela Transa��o selecionada</option>
 						<option value="?action=TransacaoModulo.processaFormulario&sOP=Excluir&nCodModulo=<?php echo$oModulo->getCodModulo()?>" lang="2">Excluir Transa��o(�es) selecionada(s)</option>
 					</select></td>
 				</tr>
 			</table><br/>
 			<table id="lista" align="left" width="100%" class="TabelaAdministracao" cellpadding="0" cellspacing="0">
 			<? if(is_array($voTransacaoModulo)){?>
 				<thead>
 				<tr>
 					<th class="Titulo" width="1%"><img onclick="javascript: marcarTodosCheckBoxFormulario('TransacaoModulo')" src="controle/imagens/checkbox.gif" width="16" height="16"></th>
 					<th width="7%" class='Titulo'>C�digo</th>
					<th class='Titulo'>M�dulo</th>
					<th class='Titulo'>Descri��o</th>
 				</tr>
 				</thead>
 				<tbody>
 				<? foreach($voTransacaoModulo as $oTransacaoModulo){?>
 				<tr>
 					<td class="Conteudo"><input onClick="JavaScript: atualizaAcoes('TransacaoModulo')" type="checkbox" value="<?php echo$oTransacaoModulo->getCodTransacaoModulo()?>" name="fCodTransacaoModulo[]"/></td>
 					<td class='Conteudo'><?php echo $oTransacaoModulo->getCodTransacaoModulo()?></td>
					<td class='Conteudo'><?php echo $oTransacaoModulo->getModulo()->getDescricao()?></td>
					<td class='Conteudo'><?php echo $oTransacaoModulo->getDescricao()?></td>
 				</tr>
 				<? }?>
 				</tbody>
 				<tfoot>
 				<tr>
  					<td colspan="10"></td>
  				</tr>
  				</tfoot>
 			<? } else {?>
 				<tfoot>
 				<tr>
 					<td class="Conteudo">N�o h� Transa��o(�es) cadastrados!</td>
 				</tr>
 				</tfoot>
 			<? }//if(count($voTransacaoModulo)){?>
 			</table>
 			</form>
 			<script language="javascript">
 				atualizaAcoes('TransacaoModulo');
 			</script>
 			<!-- InstanceEndEditable --></td>
 		</tr>
 	</table></td>
   </tr>
 </table>
 </body>
 <!-- InstanceEnd --></html>