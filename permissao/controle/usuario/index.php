<?
 $voUsuario = $_REQUEST['voUsuario'];
 ?>
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <title>Administra&ccedil;&atilde;o de Conte�do - Gerenciar Usu�rio</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 <!-- InstanceBeginEditable name="head" -->
 
 <!-- InstanceEndEditable -->
 <style type="text/css">
 <!--
 body,td,th {
 	font-family: Verdana, Arial, Helvetica, sans-serif;
 	font-size: 10px;
 	color: #000000;
 }
 .style1 {color:#006633;}
 
 
 -->
 </style>
 <link href="css/controle.css" rel="stylesheet" type="text/css">
 <script language="javascript" src="js/jquery/jquery.js"></script>
  <script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
  <script language="javascript" src="js/producao.js"></script>
  <script type="text/javascript" language="javascript">
  	$(document).ready(function() {
 		$('#lista').dataTable();
 	} );
  </script>
 </head>
 
 <body>
 <? include("controle/includes/mensagem.php")?>
 <table class="TabelaPai" width="750" border="0" align="center" cellpadding="1" cellspacing="1">
   <? include("controle/includes/topo.php")?>
   <tr bgcolor="#ECE9D8">
     <td colspan="2" class="CelulaLinhaAbaixo"><? include("controle/includes/menu.php")?></td>
   </tr>
   <tr>
     <td colspan="2" class="Corpo"><table width="100%" border="0" cellpadding="2" cellspacing="2">
 		<tr>
 			<td class="CelulaMigalhaLink"><!-- InstanceBeginEditable name="Migalha" --><strong>Voc� est� aqui:</strong> <a href="index.php">Home</a> > <strong>Gerenciar Usu�rios</strong><!-- InstanceEndEditable --></td>
 		</tr>
 		<tr>
 			<td><!-- InstanceBeginEditable name="Titulo Pagina" --><h2 class="TituloPagina">Gerenciar Usu�rios</h2><!-- InstanceEndEditable --></td>
 		</tr>
 		<tr>
 			<td><!-- InstanceBeginEditable name="Conteudo" -->
 			<form method="post" action="" name="formUsuario" id="formUsuario">
 			<table width="100%" cellpadding="2" cellspacing="2" border="0">
 				<tr>
 					<td align="left"><select class="Acoes" name="acoes" id="acoesUsuario" onChange="JavaScript: submeteForm('Usuario')">
 						<option value="" selected>A��es...</option>
 						<option value="?action=Usuario.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Usu�rio</option>
 						<option value="?action=Usuario.preparaFormulario&sOP=Alterar" lang="1">Alterar Usu�rio selecionado</option>
 						<option value="?action=Usuario.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Usu�rio selecionado</option>
 						<option value="?action=Usuario.processaFormulario&sOP=Excluir" lang="2">Excluir Usu�rio(s) selecionado(s)</option>
 					</select></td>
 				</tr>
 			</table><br/>
 			<table id="lista" align="left" width="100%" class="TabelaAdministracao" cellpadding="0" cellspacing="0">
 			<? if(is_array($voUsuario)){?>
 				<thead>
 				<tr>
 					<th class="Titulo" width="1%"><img onclick="javascript: marcarTodosCheckBoxFormulario('Usuario')" src="controle/imagens/checkbox.gif" width="16" height="16"></th>
 					<th width="7%" class='Titulo'>C�digo</th>
					<th class='Titulo'>Grupo</th>
					<th class='Titulo'>Nome</th>
					<th class='Titulo'>Login</th>
				</tr>
 				</thead>
 				<tbody>
 				<? foreach($voUsuario as $oUsuario){?>
 				<tr onMouseOver="JavaScript: this.className = 'Regua'" onMouseOut="JavaScript: this.className = ''">
 					<td class="Conteudo"><input onClick="JavaScript: atualizaAcoes('Usuario')" type="checkbox" value="<?php echo$oUsuario->getCodUsuario()?>" name="fCodUsuario[]"/></td>
 					<td class='Conteudo'><?php echo $oUsuario->getCodUsuario()?></td>
					<td class='Conteudo'><?php echo $oUsuario->getGrupoUsuario()->getDescricao()?></td>
					<td class='Conteudo'><?php echo $oUsuario->getNome()?></td>
					<td class='Conteudo'><?php echo $oUsuario->getLogin()?></td>
				</tr>
 				<? }?>
 				</tbody>
 				<tfoot>
 				<tr>
  					<td colspan="10"></td>
  				</tr>
  				</tfoot>
 			<? } else {?>
 				<tfoot>
 				<tr>
 					<td class="Conteudo">N�o h� Usu�rios cadastrados!</td>
 				</tr>
 				</tfoot>
 			<? }//if(count($voUsuario)){?>
 			</table>
 			</form>
 			<script language="javascript">
 				atualizaAcoes('Usuario');
 			</script>
 			<!-- InstanceEndEditable --></td>
 		</tr>
 	</table></td>
   </tr>
 </table>
 </body>
 <!-- InstanceEnd --></html>