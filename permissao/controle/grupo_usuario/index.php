<?
 $voGrupoUsuario = $_REQUEST['voGrupoUsuario'];
 ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 <html><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <title>T�tulo</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 <style type="text/css">
 <!--
 body,td,th {
 	font-family: Verdana, Arial, Helvetica, sans-serif;
 	font-size: 10px;
 	color: #000000;
 }
 .style1 {color:#006633;}
 
 
 -->
 </style>

 <script language="javascript" src="js/jquery/jquery.js"></script>
 <script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
 <script language="javascript" src="js/producao.js"></script>
 
 <!-- InstanceBeginEditable name="head" -->
  <style type="text/css" title="currentStyle">
	@import "css/jquery-datatables.css";
	@import "css/controle.css";
  </style>

 <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		oTable = $('#lista').dataTable({
			"bJQueryUI": true,
			"aoColumnDefs": [
            { 
			"bSortable": false, "aTargets": [ 0 ],
			"bSearchable": false, "aTargets": [ 0 ]
			}
        ]
		});
	} );
 </script>

 </head>
 
 <body>
 <? include("controle/includes/mensagem.php")?>
 <table class="TabelaPai" width="750" border="0" align="center" cellpadding="1" cellspacing="1">
   <? include("controle/includes/topo.php")?>
   <tr bgcolor="#ECE9D8">
     <td colspan="2" class="CelulaLinhaAbaixo"><? include("controle/includes/menu.php")?></td>
   </tr>
   <tr>
     <td colspan="2" class="Corpo"><table width="100%" border="0" cellpadding="2" cellspacing="2">
 		<tr>
 			<td class="CelulaMigalhaLink"><!-- InstanceBeginEditable name="Migalha" --><strong>Voc� est� aqui:</strong> <a href="index.php">Home</a> > <strong>Gerenciar Grupos de Usu�rio</strong><!-- InstanceEndEditable --></td>
 		</tr>
 		<tr>
 			<td><!-- InstanceBeginEditable name="Titulo Pagina" --><h2 class="TituloPagina">Gerenciar Grupos de Usu�rio</h2><!-- InstanceEndEditable --></td>
 		</tr>
 		<tr>
 			<td><!-- InstanceBeginEditable name="Conteudo" -->
 			<form method="post" action="" name="formGrupoUsuario" id="formGrupoUsuario">
 			<table width="100%" cellpadding="2" cellspacing="2" border="0">
 				<tr>
 					<td align="left"><select class="Acoes" name="acoes" id="acoesGrupoUsuario" onChange="JavaScript: submeteForm('GrupoUsuario')">
 						<option value="" selected>A��es...</option>
 						<option value="?action=GrupoUsuario.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo Grupo de Usu�rio</option>
 						<option value="?action=GrupoUsuario.preparaFormulario&sOP=Alterar" lang="1">Alterar Grupo de Usu�rio selecionado</option>
 						<option value="?action=GrupoUsuario.preparaFormulario&sOP=Detalhar" lang="1">Detalhar Grupo de Usu�rio selecionado</option>
 						<option value="?action=GrupoUsuario.processaFormulario&sOP=Excluir" lang="2">Excluir Grupo(s) de Usu�rio selecionado(s)</option>
                        <option value="?action=Permissao.preparaLista" lang="1">Gerenciar permiss�es Grupo de Usu�rio selecionado</option>
 					</select></td>
 				</tr>
 			</table><br/>
 			<table id="lista" align="left" width="100%" class="TabelaAdministracao" cellpadding="0" cellspacing="0">
 			<? if(is_array($voGrupoUsuario)){?>
 				<thead>
 				<tr>
 					<th width="1%"><img onclick="javascript: marcarTodosCheckBoxFormulario('GrupoUsuario')" src="controle/imagens/checkbox.gif" width="16" height="16"></th>
 					<th width="8%" class='Titulo'>C�digo</th>
					<th>Descri��o</th>
				</tr>
 				</thead>
 				<tbody>
 				<? foreach($voGrupoUsuario as $oGrupoUsuario){?>
 				<tr>
 					<td class="Conteudo"><input onClick="JavaScript: atualizaAcoes('GrupoUsuario')" type="checkbox" value="<?php echo$oGrupoUsuario->getCodGrupoUsuario()?>" name="fCodGrupoUsuario[]"/></td>
 					<td class='Conteudo'><?php echo $oGrupoUsuario->getCodGrupoUsuario()?></td>
					<td class='Conteudo'><?php echo $oGrupoUsuario->getDescricao()?></td>
				</tr>
 				<? }?>
 				</tbody>
 				<tfoot>
 				<tr>
  					<td colspan="10"></td>
  				</tr>
  				</tfoot>
 			<? } else {?>
 				<tfoot>
 				<tr>
 					<td class="Conteudo">N�o h� Grupos de Usu�rio cadastrados!</td>
 				</tr>
 				</tfoot>
 			<? }//if(count($voGrupoUsuario)){?>
 			</table>
 			</form>
 			<script language="javascript">
 				atualizaAcoes('GrupoUsuario');
 			</script>
 			<!-- InstanceEndEditable --></td>
 		</tr>
 	</table></td>
   </tr>
 </table>
 </body>
 <!-- InstanceEnd --></html>