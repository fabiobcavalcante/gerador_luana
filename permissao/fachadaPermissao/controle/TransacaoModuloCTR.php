<?php
 class TransacaoModuloCTR implements IControle{
 
 	public function TransacaoModuloCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaPermissaoBD();
        
        $nCodModulo = ($_POST['fCodModulo'][0]) ? $_POST['fCodModulo'][0] : $_GET['nCodModulo'];
        $oModulo = $oFachada->recuperarUmModulo($nCodModulo);
        
        $_REQUEST['oModulo'] = $oModulo;
		
 		include_once("controle/transacao_modulo/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaPermissaoBD();
 
 		$oTransacaoModulo = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nCodTransacaoModulo = ($_POST['fCodTransacaoModulo'][0]) ? $_POST['fCodTransacaoModulo'][0] : $_GET['nCodTransacaoModulo'];
 	
 			if($nCodTransacaoModulo){
 				$vCodTransacaoModulo = explode("||",$nCodTransacaoModulo);
 				$oTransacaoModulo = $oFachada->recuperarUmTransacaoModulo($vCodTransacaoModulo[0]);
 			}
 		} else
            $_REQUEST['oModulo'] = $oFachada->recuperarUmModulo($_GET['nCodModulo']);

 		$_REQUEST['oTransacaoModulo'] = ($_SESSION['oTransacaoModulo']) ? $_SESSION['oTransacaoModulo'] : $oTransacaoModulo;
		$_SESSION['oTransacaoModulo'] = array();
 
 				
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("controle/transacao_modulo/detalhe.php");
 		else
 			include_once("controle/transacao_modulo/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaPermissaoBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oTransacaoModulo = $oFachada->inicializarTransacaoModulo($_POST['fCodTransacaoModulo'],$_POST['fCodModulo'],$_POST['fDescricao']);
 			$_SESSION['oTransacaoModulo'] = $oTransacaoModulo;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_number_field("C�digo", $oTransacaoModulo->getCodModulo(), "number", "y");
			$oValidate->add_text_field("Descricao", $oTransacaoModulo->getDescricao(), "text", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=TransacaoModulo.preparaFormulario&sOP=".$sOP."&nCodTransacaoModulo=".$_POST['fId']."&nCodModulo=".$_POST['fCodModulo'];
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirTransacaoModulo($oTransacaoModulo)){
					$_SESSION['oTransacaoModulo'] = array();
 					$_SESSION['sMsg'] = "Transa��o de M�dulo inserida com sucesso!";
 					$sHeader = "?bErro=0&action=TransacaoModulo.preparaLista&nCodModulo=".$_POST['fCodModulo'];
 					
 				} else {
 					$_SESSION['sMsg'] = "N�o foi poss�vel inserir a Transa��o do M�dulo!";
 					$sHeader = "?bErro=1&action=TransacaoModulo.preparaFormulario&sOP=".$sOP."&nCodModulo=".$_POST['fCodModulo'];
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarTransacaoModulo($oTransacaoModulo)){
					$_SESSION['oTransacaoModulo'] = array();
 					$_SESSION['sMsg'] = "Transa��o do M�dulo alterada com sucesso!";
 					$sHeader = "?bErro=0&action=TransacaoModulo.preparaLista&nCodModulo=".$_POST['fCodModulo'];
 					
 				} else {
 					$_SESSION['sMsg'] = "N�o foi poss�vel alterar a Transa��o do M�dulo!";
 					$sHeader = "?bErro=1&action=TransacaoModulo.preparaFormulario&sOP=".$sOP."&nCodTransacaoModulo=".$_POST['fCodTransacaoModulo']."&nCodModulo=".$_POST['fCodModulo'];
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				foreach($_POST['fCodTransacaoModulo'] as $nCodTransacaoModulo){
 					$vCodTransacaoModulo = explode("||",$nCodTransacaoModulo);
 					$bResultado &= $oFachada->excluirTransacaoModulo($vCodTransacaoModulo[0]);
 				}
 		
 				if($bResultado){
 					$_SESSION['sMsg'] = "Transa��o(�es) do M�dulo exclu�da(s) com sucesso!";
 					$sHeader = "?bErro=0&action=TransacaoModulo.preparaLista&nCodModulo=".$_REQUEST['nCodModulo'];
 				} else {
 					$_SESSION['sMsg'] = "N�o foi poss�vel excluir a(s) Transa��o(�es) do M�dulo!";
 					$sHeader = "?bErro=1&action=TransacaoModulo.preparaLista&nCodModulo=".$_REQUEST['nCodModulo'];
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>