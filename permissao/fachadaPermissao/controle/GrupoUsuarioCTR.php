<?php
 class GrupoUsuarioCTR implements IControle{
 
 	public function GrupoUsuarioCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaPermissaoBD();
 
 		$voGrupoUsuario = $oFachada->recuperarTodosGrupoUsuario();
 
 		$_REQUEST['voGrupoUsuario'] = $voGrupoUsuario;
 		
 		
 		include_once("controle/grupo_usuario/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaPermissaoBD();
 
 		$oGrupoUsuario = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nCodGrupoUsuario = ($_POST['fCodGrupoUsuario'][0]) ? $_POST['fCodGrupoUsuario'][0] : $_GET['nCodGrupoUsuario'];
 	
 			if($nCodGrupoUsuario){
 				$vCodGrupoUsuario = explode("||",$nCodGrupoUsuario);
 				$oGrupoUsuario = $oFachada->recuperarUmGrupoUsuario($vCodGrupoUsuario[0]);
 			}
 		}
 		
 		$_REQUEST['oGrupoUsuario'] = ($_SESSION['oGrupoUsuario']) ? $_SESSION['oGrupoUsuario'] : $oGrupoUsuario;
 		$_SESSION['oGrupoUsuario'] = array();
 
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("controle/grupo_usuario/detalhe.php");
 		else
 			include_once("controle/grupo_usuario/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaPermissaoBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oGrupoUsuario = $oFachada->inicializarGrupoUsuario($_POST['fCodGrupoUsuario'],$_POST['fDescricao'],$_POST['fAtivo']);
 			$_SESSION['oGrupoUsuario'] = $oGrupoUsuario;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_text_field("Nome", $oGrupoUsuario->getDescricao(), "text", "y");
			$oValidate->add_number_field("Ativo", $oGrupoUsuario->getAtivo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=GrupoUsuario.preparaFormulario&sOP=".$sOP."&nCodGrupoUsuario=".$_POST['fCodGrupoUsuario']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirGrupoUsuario($oGrupoUsuario)){
 					$_SESSION['oGrupoUsuario'] = array();
 					$_SESSION['sMsg'] = "Grupo de Usu�rio inserido com sucesso!";
 					$sHeader = "?bErro=0&action=GrupoUsuario.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N�o foi poss�vel inserir o Grupo de Usu�rio!";
 					$sHeader = "?bErro=1&action=GrupoUsuario.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarGrupoUsuario($oGrupoUsuario)){
 					$_SESSION['oGrupoUsuario'] = array();
 					$_SESSION['sMsg'] = "Grupo de Usu�rio alterado com sucesso!";
 					$sHeader = "?bErro=0&action=GrupoUsuario.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N�o foi poss�vel alterar o Grupo de Usu�rio!";
 					$sHeader = "?bErro=1&action=GrupoUsuario.preparaFormulario&sOP=".$sOP."&nCodGrupoUsuario=".$_POST['fCodGrupoUsuario']."";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				foreach($_POST['fCodGrupoUsuario'] as $nCodGrupoUsuario){
 					$vCodGrupoUsuario = explode("||",$nCodGrupoUsuario);
 					$bResultado &= $oFachada->excluirGrupoUsuario($vCodGrupoUsuario[0]);
 				}
 		
 				if($bResultado){
 					$_SESSION['sMsg'] = "Grupo(s) de Usu�rio exclu�do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=GrupoUsuario.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N�o foi poss�vel excluir o(s) Grupo(s) de Usu�rio!";
 					$sHeader = "?bErro=1&action=GrupoUsuario.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>