<?php
 class ModuloCTR implements IControle{
 
 	public function ModuloCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaPermissaoBD();
 
 		$voModulo = $oFachada->recuperarTodosModulo();
 
 		$_REQUEST['voModulo'] = $voModulo;
 		
 		
 		include_once("controle/modulo/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaPermissaoBD();
 
 		$oModulo = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nCodModulo = ($_POST['fCodModulo'][0]) ? $_POST['fCodModulo'][0] : $_GET['nCodModulo'];
 	
 			if($nCodModulo){
 				$vCodModulo = explode("||",$nCodModulo);
 				$oModulo = $oFachada->recuperarUmModulo($vCodModulo[0]);
 			}
 		}
 		
 		$_REQUEST['oModulo'] = ($_SESSION['oModulo']) ? $_SESSION['oModulo'] : $oModulo;
 		$_SESSION['oModulo'] = array();
		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("controle/modulo/detalhe.php");
 		else
 			include_once("controle/modulo/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaPermissaoBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oModulo = $oFachada->inicializarModulo($_POST['fCodModulo'],$_POST['fDescricao'],$_POST['fAtivo']);
 			$_SESSION['oModulo'] = $oModulo;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_text_field("Descricao", $oModulo->getDescricao(), "text", "y");
			$oValidate->add_number_field("Ativo", $oModulo->getAtivo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=Modulo.preparaFormulario&sOP=".$sOP."&nCodModulo=".$_POST['fCodModulo']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirModulo($oModulo)){
 					$_SESSION['oModulo'] = array();
 					$_SESSION['sMsg'] = "M�dulo inserido com sucesso!";
 					$sHeader = "?bErro=0&action=Modulo.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N�o foi poss�vel inserir o M�dulo!";
 					$sHeader = "?bErro=1&action=Modulo.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarModulo($oModulo)){
 					$_SESSION['oModulo'] = array();
 					$_SESSION['sMsg'] = "M�dulo alterado com sucesso!";
 					$sHeader = "?bErro=0&action=Modulo.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N�o foi poss�vel alterar o M�dulo!";
 					$sHeader = "?bErro=1&action=Modulo.preparaFormulario&sOP=".$sOP."&nCodModulo=".$_POST['fCodModulo']."";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				foreach($_POST['fCodModulo'] as $nCodModulo){
 					$vCodModulo = explode("||",$nCodModulo);
 					$bResultado &= $oFachada->excluirModulo($vCodModulo[0]);
 				}
 		
 				if($bResultado){
 					$_SESSION['sMsg'] = "M�dulo(s) exclu�do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=Modulo.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N�o foi poss�vel excluir o(s) M�dulo!";
 					$sHeader = "?bErro=1&action=Modulo.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>