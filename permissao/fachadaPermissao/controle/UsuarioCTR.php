<?php
 class UsuarioCTR implements IControle{
 
 	public function UsuarioCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaPermissaoBD();
 
 		$voUsuario = $oFachada->recuperarTodosUsuario();

 		$_REQUEST['voGrupoUsuario'] = $oFachada->recuperarTodosGrupoUsuario();
 		$_REQUEST['voUsuario'] = $voUsuario;
 				
 		include_once("controle/usuario/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaPermissaoBD();
 		$oFachadaPleito = new FachadaPleitoBD();
 
 		$oUsuario = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar" || $_REQUEST['sOP']=="AlterarSenha" ){
 			$nCodUsuario = ($_POST['fCodUsuario'][0]) ? $_POST['fCodUsuario'][0] : $_GET['nCodUsuario'];
 	
 			if($nCodUsuario){
 				$vIdUsuario = explode("||",$nCodUsuario);
 				$oUsuario = $oFachada->recuperarUmUsuario($vIdUsuario[0]);
 			}
 		}
 		
 		$_REQUEST['oUsuario'] = ($_SESSION['oUsuario']) ? $_SESSION['oUsuario'] : $oUsuario;
 		$_SESSION['oUsuario'] = array();
 
		$_REQUEST['voGrupoUsuario'] = $oFachada->recuperarTodosGrupoUsuario();

		if($_REQUEST['sOP'] =="AlterarSenha")
			include_once("controle/usuario/alterar_senha.php");
		else
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("controle/usuario/detalhe.php");
 		else{
			include_once("controle/usuario/insere_altera.php");
		}
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaPermissaoBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir" && $sOP != "AlterarSenha"){

			$sSenha = ($sOP == 'Cadastrar') ? md5($_POST['fSenha']) : $_POST['fSenha'];
			
 			$oUsuario = $oFachada->inicializarUsuario($_POST['fCodUsuario'],$_POST['fCodGrupoUsuario'],$_POST['fLogin'],$sSenha,$_POST['fAtivo']);
 			$_SESSION['oUsuario'] = $oUsuario;
			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_number_field("Grupo de Usu�rio", $oUsuario->getCodGrupoUsuario(), "number", "y");
			$oValidate->add_text_field("Login", $oUsuario->getLogin(), "text", "y");
			$oValidate->add_text_field("Senha", $oUsuario->getSenha(), "text", "y");
			$oValidate->add_number_field("Ativo", $oUsuario->getAtivo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=Usuario.preparaFormulario&sOP=".$sOP."&nIdUsuario=".$_POST['fCodUsuario']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
				if(!$oFachada->recuperarUmUsuarioPorLogin($oUsuario->getLogin())){
					if($oFachada->inserirUsuario($oUsuario)){
					 		$_SESSION['oUsuario'] = array();
							$_SESSION['sMsg'] = "Usu�rio inserido com sucesso!";
							$sHeader = "?bErro=0&action=Usuario.preparaLista";

					} else {
							$_SESSION['sMsg'] = "N�o foi poss�vel inserir o Usu�rio!";
							$sHeader = "?bErro=1&action=Usuario.preparaFormulario&sOP=".$sOP;
					}
				} else {
					$_SESSION['sMsg'] = "Este login j� existe no banco de dados! Por favor escolha outro!";
					$sHeader = "?bErro=1&action=Usuario.preparaFormulario&sOP=".$sOP;
				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarUsuario($oUsuario)){
			 		$_SESSION['oUsuario'] = array();
 					$_SESSION['sMsg'] = "Usu�rio alterado com sucesso!";
 					$sHeader = "?bErro=0&action=Usuario.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N�o foi poss�vel alterar o Usu�rio!";
 					$sHeader = "?bErro=1&action=Usuario.preparaFormulario&sOP=".$sOP."&nIdUsuario=".$_POST['fId']."";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				foreach($_POST['fCodUsuario'] as $nCodUsuario){
 					$vIdUsuario = explode("||",$nCodUsuario);
 					$bResultado &= $oFachada->excluirUsuario($vIdUsuario[0]);
 				}
 		
 				if($bResultado){
 					$_SESSION['sMsg'] = "Usu�rio(s) exclu�do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=Usuario.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N�o foi poss�vel excluir o(s) Usu�rio!";
 					$sHeader = "?bErro=1&action=Usuario.preparaLista";
 				}
 			break;
			case "AlterarSenha":
				$oUsuario = $oFachada->recuperarUmUsuario($_SESSION['oUsuarioLogado']->getCodUsuario());
				if(md5($_REQUEST['fSenha']) == $oUsuario->getSenha()){
					if($_REQUEST['fSenha1'] == $_REQUEST['fSenha2']){
						$oUsuario->setSenha(md5($_REQUEST['fSenha1']));
						if($oFachada->alterarUsuario($oUsuario)){
					 		$_SESSION['oUsuario'] = array();
							$_SESSION['sMsg'] = "Senha alterada com sucesso!";
							$_SESSION['oUsuarioLogado']->setSenha(md5($_REQUEST['fSenha1']));
							$sHeader = "?bErro=0";
						} else {
							$_SESSION['sMsg'] = "N�o foi poss�vel alterar a Senha!";
							$sHeader = "?bErro=1&action=Usuario.preparaFormulario&sOP=".$sOP."&nCodUsuario=".$_POST['fCodUsuario'];
						}
					}else{
						$_SESSION['sMsg'] = "Os campos 'Nova Senha' e 'Repita Nova Senha' est�o diferentes!";
						$sHeader = "?bErro=1&action=Usuario.preparaFormulario&sOP=".$sOP."&nCodUsuario=".$_POST['fCodUsuario'];
					}
				}else{
					$_SESSION['sMsg'] = "A 'Senha Atual' est� incorreta!";
					$sHeader = "?bErro=1&action=Usuario.preparaFormulario&sOP=".$sOP."&nCodUsuario=".$_POST['fCodUsuario'];				
				}
			break;

 		}
 
 		header("Location: ".$sHeader);		
 	}
 } 
 ?>