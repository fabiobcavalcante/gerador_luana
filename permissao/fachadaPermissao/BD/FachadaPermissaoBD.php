<?php

class FachadaPermissaoBD {
	/**
 	 *
 	 * M�todo para inicializar um Objeto GrupoUsuario
 	 *
 	 * @return GrupoUsuario  
 	 */
 	public function inicializarGrupoUsuario($nCodGrupoUsuario,$sDescricao,$nAtivo){
 		$oGrupoUsuario = new GrupoUsuario();
 		
		$oGrupoUsuario->setCodGrupoUsuario($nCodGrupoUsuario);
		$oGrupoUsuario->setDescricao($sDescricao);
		$oGrupoUsuario->setAtivo($nAtivo);
 		return $oGrupoUsuario;
 	}
 	
 	/**
 	 *
 	 * M�todo para inserir na base de dados um Objeto GrupoUsuario
 	 *
 	 * @return boolean
 	 */
 	public function inserirGrupoUsuario($oGrupoUsuario){
 		$oPersistencia = new Persistencia($oGrupoUsuario);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para alterar na base de dados um Objeto GrupoUsuario
 	 *
 	 * @return boolean
 	 */
 	public function alterarGrupoUsuario($oGrupoUsuario){
 		$oPersistencia = new Persistencia($oGrupoUsuario);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto GrupoUsuario
 	 *
 	 * @return boolean
 	 */
 	public function excluirGrupoUsuario($nCodGrupoUsuario){
 		$oGrupoUsuario = new GrupoUsuario();
  		
		$oGrupoUsuario->setCodGrupoUsuario($nCodGrupoUsuario);
  		$oPersistencia = new Persistencia($oGrupoUsuario);
  		if($oPersistencia->excluir())
  				return true;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para verificar se existe um Objeto GrupoUsuario na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteGrupoUsuario($nCodGrupoUsuario){
 		$oGrupoUsuario = new GrupoUsuario();
  		
		$oGrupoUsuario->setCodGrupoUsuario($nCodGrupoUsuario);
  		$oPersistencia = new Persistencia($oGrupoUsuario);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para recuperar um objeto GrupoUsuario da base de dados
 	 *
 	 * @return GrupoUsuario
 	 */
 	public function recuperarUmGrupoUsuario($nCodGrupoUsuario){
 		$oGrupoUsuario = new GrupoUsuario();
  		$oPersistencia = new Persistencia($oGrupoUsuario);
  		$sTabelas = "acesso_grupo_usuario";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_grupo_usuario = $nCodGrupoUsuario";
  		$voGrupoUsuario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voGrupoUsuario)
  			return $voGrupoUsuario[0];
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos GrupoUsuario da base de dados
 	 *
 	 * @return GrupoUsuario[]
 	 */
 	public function recuperarTodosGrupoUsuario(){
 		$oGrupoUsuario = new GrupoUsuario();
  		$oPersistencia = new Persistencia($oGrupoUsuario);
  		$sTabelas = "acesso_grupo_usuario";
  		$sCampos = "*";
  		$sComplemento = "WHERE cod_grupo_usuario <> 1 AND ativo=1";
  		$voGrupoUsuario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
		
  		if($voGrupoUsuario)
  			return $voGrupoUsuario;
  		return false;
 	}
 	
  	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos GrupoUsuario da base de dados
 	 *
 	 * @return GrupoUsuario[]
 	 */
 	public function recuperarTodosGrupoUsuarioPleitoSepof(){
 		$oGrupoUsuario = new GrupoUsuario();
  		$oPersistencia = new Persistencia($oGrupoUsuario);
  		$sTabelas = "acesso_grupo_usuario";
  		$sCampos = "*";
  		$sComplemento = "WHERE cod_grupo_usuario <> 1 AND ativo=1 AND cod_grupo_usuario IN ('6','7','8','11')";
  		$voGrupoUsuario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
		
  		if($voGrupoUsuario)
  			return $voGrupoUsuario;
  		return false;
 	}	
 
	/**
 	 *
 	 * M�todo para inicializar um Objeto Modulo
 	 *
 	 * @return Modulo  
 	 */
 	public function inicializarModulo($nCodModulo,$sDescricao,$nAtivo){
 		$oModulo = new Modulo();
 		
		$oModulo->setCodModulo($nCodModulo);
		$oModulo->setDescricao($sDescricao);
		$oModulo->setAtivo($nAtivo);
 		return $oModulo;
 	}
 	
 	/**
 	 *
 	 * M�todo para inserir na base de dados um Objeto Modulo
 	 *
 	 * @return boolean
 	 */
 	public function inserirModulo($oModulo){
 		$oPersistencia = new Persistencia($oModulo);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para alterar na base de dados um Objeto Modulo
 	 *
 	 * @return boolean
 	 */
 	public function alterarModulo($oModulo){
 		$oPersistencia = new Persistencia($oModulo);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto Modulo
 	 *
 	 * @return boolean
 	 */
 	public function excluirModulo($nCodModulo){
 		$oModulo = new Modulo();
  		
		$oModulo->setCodModulo($nCodModulo);
  		$oPersistencia = new Persistencia($oModulo);
  		if($oPersistencia->excluir())
  				return true;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para verificar se existe um Objeto Modulo na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteModulo($nCodModulo){
 		$oModulo = new Modulo();
  		
		$oModulo->setCodModulo($nCodModulo);
  		$oPersistencia = new Persistencia($oModulo);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para recuperar um objeto Modulo da base de dados
 	 *
 	 * @return Modulo
 	 */
 	public function recuperarUmModulo($nCodModulo){
 		$oModulo = new Modulo();
  		$oPersistencia = new Persistencia($oModulo);
  		$sTabelas = "acesso_modulo";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_modulo= $nCodModulo";
  		$voModulo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voModulo)
  			return $voModulo[0];
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos Modulo da base de dados
 	 *
 	 * @return Modulo[]
 	 */
 	public function recuperarTodosModulo(){
 		$oModulo = new Modulo();
  		$oPersistencia = new Persistencia($oModulo);
  		$sTabelas = "acesso_modulo";
  		$sCampos = "*";
  		$sComplemento = "WHERE ativo=1";
  		$voModulo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voModulo)
  			return $voModulo;
  		return false;
 	}
 	
 	
 
	/**
 	 *
 	 * M�todo para inicializar um Objeto Permissao
 	 *
 	 * @return Permissao  
 	 */
 	public function inicializarPermissao($nCodTransacaoModulo,$nCodGrupoUsuario){
 		$oPermissao = new Permissao();
 		
		$oPermissao->setCodTransacaoModulo($nCodTransacaoModulo);
		$oPermissao->setCodGrupoUsuario($nCodGrupoUsuario);
 		return $oPermissao;
 	}
 	
 	/**
 	 *
 	 * M�todo para inserir na base de dados um Objeto Permissao
 	 *
 	 * @return boolean
 	 */
 	public function inserirPermissao($oPermissao){
 		$oPersistencia = new Persistencia($oPermissao);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para alterar na base de dados um Objeto Permissao
 	 *
 	 * @return boolean
 	 */
 	public function alterarPermissao($oPermissao){
 		$oPersistencia = new Persistencia($oPermissao);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto Permissao
 	 *
 	 * @return boolean
 	 */
 	public function excluirPermissao($nCodTransacaoModulo,$nCodGrupoUsuario){
 		$oPermissao = new Permissao();
  		
		$oPermissao->setCodTransacaoModulo($nCodTransacaoModulo);
		$oPermissao->setCodGrupoUsuario($nCodGrupoUsuario);
  		$oPersistencia = new Persistencia($oPermissao);
  		if($oPersistencia->excluir())
  				return true;
  		return false;
 	}
	
	/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto Permissao
 	 *
 	 * @return boolean
 	 */
 	public function excluirPermissaoPorGrupoUsuario($nCodGrupoUsuario){
 		$oPermissao = new Permissao();
		$sComplemento = " WHERE cod_grupo_usuario = $nCodGrupoUsuario";
  		$oPersistencia = new Persistencia($oPermissao);
  		if($oPersistencia->excluirFisicamenteSemChavePrimaria($sComplemento))
  			return true;
  		else
  			return false;

 	}
 	/**
 	 *
 	 * M�todo para verificar se existe um Objeto Permissao na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presentePermissao($nCodTransacaoModulo,$nCodGrupoUsuario){
 		$oPermissao = new Permissao();
  		
		$oPermissao->setCodTransacaoModulo($nCodTransacaoModulo);
		$oPermissao->setCodGrupoUsuario($nCodGrupoUsuario);
  		$oPersistencia = new Persistencia($oPermissao);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para recuperar um objeto Permissao da base de dados
 	 *
 	 * @return Permissao
 	 */
 	public function recuperarUmPermissao($nIdTransacaoModulo,$nIdGrupoUsuario){
 		$oPermissao = new Permissao();
  		$oPersistencia = new Persistencia($oPermissao);
  		$sTabelas = "acesso_permissao";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_transacao_modulo = $nIdTransacaoModulo AND cod_grupo_usuario = $nIdGrupoUsuario";
  		$voPermissao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voPermissao)
  			return $voPermissao[0];
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos Permissao da base de dados
 	 *
 	 * @return Permissao[]
 	 */
 	public function recuperarTodosPermissao(){
 		$oPermissao = new Permissao();
  		$oPersistencia = new Persistencia($oPermissao);
  		$sTabelas = "acesso_permissao";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voPermissao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voPermissao)
  			return $voPermissao;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos Permissao da base de dados
 	 *
 	 * @return Permissao[]
 	 */
 	public function recuperarTodosPermissaoPorGrupoUsuario($nCodGrupoUsuario){
 		$oPermissao = new Permissao();
  		$oPersistencia = new Persistencia($oPermissao);
  		$sTabelas = "acesso_permissao";
  		$sCampos = "*";
  		$sComplemento = "WHERE cod_grupo_usuario =". $nCodGrupoUsuario;
  		$voPermissao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voPermissao)
  			return $voPermissao;
  		return false;
 	}
 
	/**
 	 *
 	 * M�todo para inicializar um Objeto ResponsavelTransacao
 	 *
 	 * @return ResponsavelTransacao  
 	 */
 	public function inicializarResponsavelTransacao($nCodResponsavelTransacao,$nIdTransacaoModulo,$sResponsavel,$sOperacao){
 		$oResponsavelTransacao = new ResponsavelTransacao();
 		
		$oResponsavelTransacao->setCodResponsavelTransacao($nCodResponsavelTransacao);
		$oResponsavelTransacao->setCodTransacaoModulo($nIdTransacaoModulo);
		$oResponsavelTransacao->setResponsavel($sResponsavel);
		$oResponsavelTransacao->setOperacao($sOperacao);
 		return $oResponsavelTransacao;
 	}
 	
 	/**
 	 *
 	 * M�todo para inserir na base de dados um Objeto ResponsavelTransacao
 	 *
 	 * @return boolean
 	 */
 	public function inserirResponsavelTransacao($oResponsavelTransacao){
 		$oPersistencia = new Persistencia($oResponsavelTransacao);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para alterar na base de dados um Objeto ResponsavelTransacao
 	 *
 	 * @return boolean
 	 */
 	public function alterarResponsavelTransacao($oResponsavelTransacao){
 		$oPersistencia = new Persistencia($oResponsavelTransacao);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto ResponsavelTransacao
 	 *
 	 * @return boolean
 	 */
 	public function excluirResponsavelTransacao($nCodResponsavelTransacao){
 		$oResponsavelTransacao = new ResponsavelTransacao();
  		
		$oResponsavelTransacao->setCodResponsavelTransacao($nCodResponsavelTransacao);
  		$oPersistencia = new Persistencia($oResponsavelTransacao);
  		if($oPersistencia->excluirFisicamente())
  				return true;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para verificar se existe um Objeto ResponsavelTransacao na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteResponsavelTransacao($nCodResponsavelTransacao){
 		$oResponsavelTransacao = new ResponsavelTransacao();
  		
		$oResponsavelTransacao->setCodResponsavelTransacao($nCodResponsavelTransacao);
  		$oPersistencia = new Persistencia($oResponsavelTransacao);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para recuperar um objeto ResponsavelTransacao da base de dados
 	 *
 	 * @return ResponsavelTransacao
 	 */
 	public function recuperarUmResponsavelTransacao($nnCodResponsavelTransacao){
 		$oResponsavelTransacao = new ResponsavelTransacao();
  		$oPersistencia = new Persistencia($oResponsavelTransacao);
  		$sTabelas = "acesso_responsavel_transacao";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_responsavel_transacao = $nnCodResponsavelTransacao";
  		$voResponsavelTransacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voResponsavelTransacao)
  			return $voResponsavelTransacao[0];
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos ResponsavelTransacao da base de dados
 	 *
 	 * @return ResponsavelTransacao[]
 	 */
 	public function recuperarTodosResponsavelTransacao(){
 		$oResponsavelTransacao = new ResponsavelTransacao();
  		$oPersistencia = new Persistencia($oResponsavelTransacao);
  		$sTabelas = "acesso_responsavel_transacao";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voResponsavelTransacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voResponsavelTransacao)
  			return $voResponsavelTransacao;
  		return false;
 	}
 	
	
 	
  	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos ResponsavelTransacao da base de dados
 	 *
 	 * @return ResponsavelTransacao[]
 	 */
 	public function recuperarTodosResponsavelTransacaoPorTransacaoModulo($nIdTransacaoModulo){
 		$oResponsavelTransacao = new ResponsavelTransacao();
  		$oPersistencia = new Persistencia($oResponsavelTransacao);
  		$sTabelas = "acesso_responsavel_transacao";
  		$sCampos = "*";
  		$sComplemento = "WHERE cod_transacao_modulo = ".$nIdTransacaoModulo;
                
  		$voResponsavelTransacao = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voResponsavelTransacao)
  			return $voResponsavelTransacao;
  		return false;
 	}
	/**
 	 *
 	 * M�todo para inicializar um Objeto TransacaoModulo
 	 *
 	 * @return TransacaoModulo  
 	 */
 	public function inicializarTransacaoModulo($nCodTransacaoModulo,$nCodModulo,$sDescricao){
 		$oTransacaoModulo = new TransacaoModulo();
 		
		$oTransacaoModulo->setCodTransacaoModulo($nId);
		$oTransacaoModulo->setCodModulo($nCodModulo);
		$oTransacaoModulo->setDescricao($sDescricao);
 		return $oTransacaoModulo;
 	}
 	
 	/**
 	 *
 	 * M�todo para inserir na base de dados um Objeto TransacaoModulo
 	 *
 	 * @return boolean
 	 */
 	public function inserirTransacaoModulo($oTransacaoModulo){
 		$oPersistencia = new Persistencia($oTransacaoModulo);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para alterar na base de dados um Objeto TransacaoModulo
 	 *
 	 * @return boolean
 	 */
 	public function alterarTransacaoModulo($oTransacaoModulo){
 		$oPersistencia = new Persistencia($oTransacaoModulo);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto TransacaoModulo
 	 *
 	 * @return boolean
 	 */
 	public function excluirTransacaoModulo($nCodTransacaoModulo){
 		$oTransacaoModulo = new TransacaoModulo();
  		
		$oTransacaoModulo->setCodTransacaoModulo($nCodTransacaoModulo);
  		$oPersistencia = new Persistencia($oTransacaoModulo);
  		if($oPersistencia->excluirFisicamente())
  				return true;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para verificar se existe um Objeto TransacaoModulo na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteTransacaoModulo($nCodTransacaoModulo){
 		$oTransacaoModulo = new TransacaoModulo();
  		
		$oTransacaoModulo->setCodTransacaoModulo($nCodTransacaoModulo);
  		$oPersistencia = new Persistencia($oTransacaoModulo);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para recuperar um objeto TransacaoModulo da base de dados
 	 *
 	 * @return TransacaoModulo
 	 */
 	public function recuperarUmTransacaoModulo($nCodTransacaoModulo){
 		$oTransacaoModulo = new TransacaoModulo();
  		$oPersistencia = new Persistencia($oTransacaoModulo);
  		$sTabelas = "acesso_transacao_modulo";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_transacao_modulo = $nCodTransacaoModulo";
  		$voTransacaoModulo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voTransacaoModulo)
  			return $voTransacaoModulo[0];
  		return false;
 	}
	
	
 	public function recuperarUmTransacaoModuloPorResponsavelOperacao($sResponsavel,$sOperacao){
 		$oTransacaoModulo = new TransacaoModulo();
  		$oPersistencia = new Persistencia($oTransacaoModulo);
  		$sTabelas = "acesso_transacao_modulo";
  		$sCampos = "acesso_transacao_modulo.*";
  		$sComplemento = " INNER JOIN acesso_responsavel_transacao ON acesso_transacao_modulo.cod_transacao_modulo = acesso_responsavel_transacao.cod_transacao_modulo
						  WHERE responsavel = '$sResponsavel' AND operacao= '$sOperacao'";

//		die("SELECT $sCampos FROM $sTabelas " . $sComplemento);

  		$voTransacaoModulo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voTransacaoModulo)
  			return $voTransacaoModulo[0];
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos TransacaoModulo da base de dados
 	 *
 	 * @return TransacaoModulo[]
 	 */
 	public function recuperarTodosTransacaoModulo(){
 		$oTransacaoModulo = new TransacaoModulo();
  		$oPersistencia = new Persistencia($oTransacaoModulo);
  		$sTabelas = "acesso_transacao_modulo";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voTransacaoModulo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voTransacaoModulo)
  			return $voTransacaoModulo;
  		return false;
 	}
 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos TransacaoModulo da base de dados
 	 *
 	 * @return TransacaoModulo[]
 	 */
 	public function recuperarTodosTransacaoModuloPorModulo($nIdModulo){
 		$oTransacaoModulo = new TransacaoModulo();
  		$oPersistencia = new Persistencia($oTransacaoModulo);
  		$sTabelas = "acesso_transacao_modulo";
  		$sCampos = "*";
  		$sComplemento = "WHERE cod_modulo = $nIdModulo";
		
  		$voTransacaoModulo = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voTransacaoModulo)
  			return $voTransacaoModulo;
  		return false;
 	}
 	
	/**
 	 *
 	 * M�todo para inicializar um Objeto Usuario
 	 *
 	 * @return Usuario  
 	 */
 	public function inicializarUsuario($nCodUsuario,$nCodGrupoUsuario,$sLogin,$sSenha,$nAtivo){
 		$oUsuario = new Usuario();
 		
		$oUsuario->setCodUsuario($nCodUsuario);
		$oUsuario->setCodGrupoUsuario($nCodGrupoUsuario);
		$oUsuario->setLogin($sLogin);
		$oUsuario->setSenha($sSenha);
		$oUsuario->setAtivo($nAtivo);
 		return $oUsuario;
 	}
 	
 	/**
 	 *
 	 * M�todo para inserir na base de dados um Objeto Usuario
 	 *
 	 * @return boolean
 	 */
 	public function inserirUsuario($oUsuario){
 		$oPersistencia = new Persistencia($oUsuario);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para alterar na base de dados um Objeto Usuario
 	 *
 	 * @return boolean
 	 */
 	public function alterarUsuario($oUsuario){
 		$oPersistencia = new Persistencia($oUsuario);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto Usuario
 	 *
 	 * @return boolean
 	 */
 	public function excluirUsuario($nCodUsuario){
 		$oUsuario = new Usuario();
  		
		$oUsuario->setCodUsuario($nCodUsuario);
  		$oPersistencia = new Persistencia($oUsuario);
  		if($oPersistencia->excluir())
  				return true;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para verificar se existe um Objeto Usuario na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteUsuario($nCodUsuario){
 		$oUsuario = new Usuario();
  		
		$oUsuario->setCodUsuario($nCodUsuario);
  		$oPersistencia = new Persistencia($oUsuario);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para recuperar um objeto Usuario da base de dados
 	 *
 	 * @return Usuario
 	 */
 	public function recuperarUmUsuario($nCodUsuario){
 		$oUsuario = new Usuario();
  		$oPersistencia = new Persistencia($oUsuario);
  		$sTabelas = "acesso_usuario";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_usuario = $nCodUsuario";
  		$voUsuario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voUsuario)
  			return $voUsuario[0];
  		return false;
 	}

 	/**
 	 *
 	 * M�todo para recuperar um objeto Usuario da base de dados
 	 *
 	 * @return Usuario
 	 */
 	public function recuperarUmUsuarioPorLogin($sLogin){
 		$oUsuario = new Usuario();
  		$oPersistencia = new Persistencia($oUsuario);
  		$sTabelas = "acesso_usuario";
  		$sCampos = "*";
  		$sComplemento = " WHERE login = '$sLogin'";

  		$voUsuario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voUsuario)
  			return $voUsuario[0];
  		return false;
 	}

 	
 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos Usuario da base de dados
 	 *
 	 * @return Usuario[]
 	 */
 	public function recuperarTodosUsuario(){
 		$oUsuario = new Usuario();
  		$oPersistencia = new Persistencia($oUsuario);
  		$sTabelas = "acesso_usuario";
  		$sCampos = "*";
  		$sComplemento = " INNER JOIN acesso_grupo_usuario ON acesso_grupo_usuario.cod_grupo_usuario = acesso_usuario.cod_grupo_usuario ";
  				$sComplemento .= "WHERE cod_usuario <> 1 AND acesso_usuario.ativo=1";
//		echo "SELECT $sCampos FROM $sTabelas $sComplemento";
  		$voUsuario = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voUsuario)
  			return $voUsuario;
  		return false;
 	}
}
?>