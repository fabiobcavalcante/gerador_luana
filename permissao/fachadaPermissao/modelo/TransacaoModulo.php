<?php
 /**
  * @author Auto-Generated 
  * @package fachadaPermissao 
  * @SGBD mysql
  * @tabela acesso_transacao_modulo 
  */
 class TransacaoModulo{
 	/**
	* @campo cod_transacao_modulo
	* @var number
	* @primario true
	* @auto-increment true
	*/
	private $nCodTransacaoModulo;
	/**
	* @campo cod_modulo
	* @var number
	* @primario false
	* @auto-increment false
	*/
	private $nCodModulo;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sDescricao;
	private $oModulo;
	private $voResponsavel;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodTransacaoModulo($nCodTransacaoModulo){
		$this->nCodTransacaoModulo = $nCodTransacaoModulo;
	}
	public function getCodTransacaoModulo(){
		return $this->nCodTransacaoModulo;
	}
	public function setCodModulo($nCodModulo){
		$this->nCodModulo = $nCodModulo;
	}
	public function getCodModulo(){
		return $this->nCodModulo;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
	public function setModulo($oModulo){
		$this->oModulo = $oModulo;
	}
	public function getModulo(){
		$oFachada = new FachadaPermissaoBD();
		$this->oModulo = $oFachada->recuperarUmModulo($this->getCodModulo());
		return $this->oModulo;
	}
	
	public function getResponsaveis(){
		$oFachada = new FachadaPermissaoBD();
		$this->voResponsavel = $oFachada->recuperarTodosResponsavelTransacaoPorTransacaoModulo($this->nCodTransacaoModulo);
		return $this->voResponsavel;
	}
 }
 ?>
