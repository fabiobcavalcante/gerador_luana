<?php
 /**
  * @author Auto-Generated 
  * @package fachadaPermissao 
  * @SGBD mysql
  * @tabela acesso_responsavel_transacao 
  */
 class ResponsavelTransacao{
 	/**
	* @campo cod_responsavel_transacao
	* @var number
	* @primario true
	* @auto-increment true
	*/
	private $nCodResponsavelTransacao;
	/**
	* @campo cod_transacao_modulo
	* @var number
	* @primario false
	* @auto-increment false
	*/
	private $nCodTransacaoModulo;
	/**
	* @campo responsavel
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sResponsavel;
	/**
	* @campo operacao
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sOperacao;
	private $oTransacaoModulo;
	private $voResponsaveis;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodResponsavelTransacao($nCodResponsavelTransacao){
		$this->nCodResponsavelTransacao = $nCodResponsavelTransacao;
	}
	public function getCodResponsavelTransacao(){
		return $this->nCodResponsavelTransacao;
	}
	public function setCodTransacaoModulo($nCodTransacaoModulo){
		$this->nCodTransacaoModulo = $nCodTransacaoModulo;
	}
	public function getCodTransacaoModulo(){
		return $this->nCodTransacaoModulo;
	}
	public function setResponsavel($sResponsavel){
		$this->sResponsavel = $sResponsavel;
	}
	public function getResponsavel(){
		return $this->sResponsavel;
	}
	public function setOperacao($sOperacao){
		$this->sOperacao = $sOperacao;
	}
	public function getOperacao(){
		return $this->sOperacao;
	}
	public function setTransacaoModulo($oTransacaoModulo){
		$this->oTransacaoModulo = $oTransacaoModulo;
	}
	public function getTransacaoModulo(){
		$oFachada = new FachadaPermissaoBD();
		$this->oTransacaoModulo = $oFachada->recuperarUmTransacaoModulo($this->getCodTransacaoModulo());
		return $this->oTransacaoModulo;
	}
	public function getResponsaveis(){
		$oFachada = new FachadaPermissaoBD();
		$this->voResponsaveis = $oFachada->recuperarTodosResponsavelTransacaoPorTransacaoModulo($this->getCodTransacaoModulo());
		if($this->voResponsaveis)
			return $this->voResponsaveis;
		else
			return false;
	}

	
 }
 ?>
