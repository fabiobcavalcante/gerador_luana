<?php
 /**
  * @author Auto-Generated 
  * @package fachadaPermissao 
  * @SGBD mysql
  * @tabela acesso_permissao 
  */
 class Permissao{
 	/**
	* @campo cod_transacao_modulo
	* @var number
	* @primario true
	* @auto-increment false
	*/
	private $nCodTransacaoModulo;
	/**
	* @campo cod_grupo_usuario
	* @var number
	* @primario true
	* @auto-increment false
	*/
	private $nCodGrupoUsuario;
	private $oGrupoUsuario;
	private $oTransacaoModulo;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodTransacaoModulo($nCodTransacaoModulo){
		$this->nCodTransacaoModulo = $nCodTransacaoModulo;
	}
	public function getCodTransacaoModulo(){
		return $this->nCodTransacaoModulo;
	}
	public function setCodGrupoUsuario($nCodGrupoUsuario){
		$this->nCodGrupoUsuario = $nCodGrupoUsuario;
	}
	public function getCodGrupoUsuario(){
		return $this->nCodGrupoUsuario;
	}
	public function setGrupoUsuario($oGrupoUsuario){
		$this->oGrupoUsuario = $oGrupoUsuario;
	}
	public function getGrupoUsuario(){
		$oFachada = new FachadaPermissaoBD();
		$this->oGrupoUsuario = $oFachada->recuperarUmGrupoUsuario($this->getCodGrupoUsuario());
		return $this->oGrupoUsuario;
	}
	public function setTransacaoModulo($oTransacaoModulo){
		$this->oTransacaoModulo = $oTransacaoModulo;
	}
	public function getTransacaoModulo(){
		$oFachada = new FachadaPermissaoBD();
		$this->oTransacaoModulo = $oFachada->recuperarUmTransacaoModulo($this->getCodTransacaoModulo());
		return $this->oTransacaoModulo;
	}
	
 }
 ?>
