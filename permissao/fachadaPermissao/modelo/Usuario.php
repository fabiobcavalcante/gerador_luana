<?php
 /**
  * @author Auto-Generated 
  * @package fachadaPermissao 
  * @SGBD mysql
  * @tabela acesso_usuario 
  */
 class Usuario{
 	/**
	* @campo cod_usuario
	* @var number
	* @primario true
	* @auto-increment true
	*/
	private $nCodUsuario;
	/**
	* @campo cod_grupo_usuario
	* @var number
	* @primario false
	* @auto-increment false
	*/
	private $nCodGrupoUsuario;
	
	/**
	* @campo login
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sLogin;
	/**
	* @campo senha
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sSenha;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @auto-increment false
	*/
	private $nAtivo;
	private $oGrupoUsuario;
	private $oBasOrgao;
//	private $oUnidadeOrcamentaria;
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodUsuario($nCodUsuario){
		$this->nCodUsuario = $nCodUsuario;
	}
	public function getCodUsuario(){
		return $this->nCodUsuario;
	}
	public function setCodGrupoUsuario($nCodGrupoUsuario){
		$this->nCodGrupoUsuario = $nCodGrupoUsuario;
	}
	public function getCodGrupoUsuario(){
		return $this->nCodGrupoUsuario;
	}

	public function setLogin($sLogin){
		$this->sLogin = $sLogin;
	}
	public function getLogin(){
		return $this->sLogin;
	}
	public function setSenha($sSenha){
		$this->sSenha = $sSenha;
	}
	public function getSenha(){
		return $this->sSenha;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	public function setGrupoUsuario($oGrupoUsuario){
		$this->oGrupoUsuario = $oGrupoUsuario;
	}
	public function getGrupoUsuario(){
		$oFachada = new FachadaPermissaoBD();
		$this->oGrupoUsuario = $oFachada->recuperarUmGrupoUsuario($this->getCodGrupoUsuario());
		return $this->oGrupoUsuario;
	}
 }
 ?>
