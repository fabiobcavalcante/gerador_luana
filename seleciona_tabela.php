﻿<?php session_start();

require_once("classes/class.BancoDeDados.php");

if(!$_SESSION['sServidor']){
	$_SESSION['sProjeto'] = $_POST['fProjeto'];
	$_SESSION['sSGBD'] = $_POST['fSGBD'];
	$_SESSION['sServidor'] = $_POST['fServidor'];
	$_SESSION['sUsuario'] = $_POST['fUsuario'];
	$_SESSION['sSenha'] = $_POST['fSenha'];
	$_SESSION['sBanco'] = $_POST['fBanco'];
	$_SESSION['sEsquema'] = $_POST['fEsquema'];
	$_SESSION['sModulo'] = $_POST['fModulo'];
}

$oBanco = new BancoDeDados();

if($oBanco->iniciaConexaoBanco())
	$oBanco->terminaConexaoBanco();
else {
	$_SESSION['sMsg'] = "Não foi possível conectar ao banco com os parametros informados!";
	header("Location: index.php");
	die();
}
$vTabela = $oBanco->retornaTabela();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>GeneratorWeb - MVC</title>
  <meta charset="utf-8">
<link rel="icon" type="image/x-icon" href="img/favicon_gerador.png" />
<style type="text/css">
<!--
.style1 {
	font-size: 36px;
	font-weight: bold;
	color: rgb(37, 78, 145);
	font-family: Arial, Helvetica, sans-serif;
	margin-top:20px;

}
.style2 {color: #FFFFFF}
.style4 {color: #000000}
.style5 {
	color: #FF2222;
	font-weight: bold;
}

body{

background:linear-gradient(rgba(94,94,94,.1),rgba(94,94,94,.1)),url(img/bg.jpg) no-repeat top;
font-family: monospace;
}

-->

</style>
</head>

<body>
<form name="form1" method="post" action="gera_classes.php">

  <table width="70%" border="0" align="center" cellpadding="2" cellspacing="2" style="background-color: #ffffff; margin-top:08%">
    <tr>
      <td colspan="2"><div align="center" class="style1"><img src="img/logo.png" class="img-responsive" alt="Imagem Responsiva" href="#" width="320"><br>
          <hr>
</div></td>
    </tr>
	<?php if($_SESSION['sMsg']){?>
	<tr>
      <td colspan="2">&nbsp;</td>
    </tr>
	<tr>
      <td colspan="2"><div align="center" class="style5"><?php echo $_SESSION['sMsg']?></div></td>
    </tr>
	<?php unset($_SESSION['sMsg']);
	   }?>
  <tr style="background-color: rgb(37, 78, 145);">
      <td colspan="2"><div align="center" class="style2">Informe para quais tabelas você deseja gerar as classes !</div></td>
    </tr>
    <tr>
      <td width="50%" bgcolor="#FFFFFF"><div align="right" class="style4">Tabelas do Banco de dados:</div></td>
      <td><select name="fTabela[]" size="15" multiple>
	  <?php foreach($vTabela as $oTabela){?>
	  <option value="<?php echo $oTabela['TABLE_NAME'] . "||". $oTabela['comment']?>"><?php echo $oTabela['TABLE_NAME']?></option>
	  <?php }?>
	  </select></td>
    </tr>
	<tr>
      <td bgcolor="#FFFFFF"><div align="right">Nome da Classe Fachada:</div></td>
      <td><input type="text" name="fNomeFachada" size="30"></td>
    </tr>
<!--
    <tr>
      <td bgcolor="#FFFFFF"><div align="right">Tipo de Classe:</div></td>
      <td><input type="radio" name="fGeracao" value="0" checked/>PHP <input type="radio" name="fGeracao" value="2"/>JAVA </td>
    </tr>
-->
    <tr>
      <td bgcolor="#FFFFFF">&nbsp;</td>
      <td><input type="submit" name="Submit" value="Gerar Classes">
      <input type="hidden" name="fPermissao" value="<?php echo $_REQUEST['fPermissao']?>">
      <input type="hidden" name="fLog" value="<?php echo $_REQUEST['fLog']?>">
      </td>
    </tr>
  </table>
</form>
</body>
</html>
