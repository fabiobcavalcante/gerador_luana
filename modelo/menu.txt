<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
	<?php if($_SESSION['oUsuario']){?>
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo ($_SESSION['oUsuario']) ? $_SESSION['oUsuario']->getNome() : "Não conectado"?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
	  <?php } ?>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">PRINCIPAL</li>
		#CELULA_MENU#
		
	    <li class="treeview">
 			<a href="#">
 				<i class="fa fa-search"></i> <span>Consultas / Relatórios</span>
 					<span class="pull-right-container">
 						<i class="fa fa-angle-left pull-right"></i>
 					</span>
 			</a>
 			<ul class="treeview-menu">
 				<li><a href="#"><i class="fa fa-circle-o"></i> Pedidos Por Cliente</a></li>
 				<li><a href="#"><i class="fa fa-circle-o"></i> Clientes Por Ano</a></li>
 				<li><a href="#"><i class="fa fa-circle-o"></i> Empenhos por Período</a></li>
 				<li><a href="#"><i class="fa fa-circle-o"></i> Pedidos por Período</a></li>
 			</ul>
 		</li>
 	    <li class="treeview">
 			<a href="#">
 				<i class="fa fa-info"></i> <span>Ajuda</span>
 					<span class="pull-right-container">
 						<i class="fa fa-angle-left pull-right"></i>
 					</span>
 			</a>
 			<ul class="treeview-menu">
 				<li><a href="?action=Info.preparaFormulario&sOP=sobre"><i class="fa fa-circle-o"></i> Sobre</a></li>
 				<li><a href="#"><i class="fa fa-wikipedia-w"></i> Manual</a></li>
 			</ul>
 		</li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>