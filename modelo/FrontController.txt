package frontController;

import fachadaUsuario.controle.*;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class FrontController {
    
    private static HttpServletRequest request = null;
    private static HttpServletResponse response = null;
    private static HttpSession session = null;
    
    
    public FrontController(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        this.setRequest(request);
        this.setResponse(response);
        this.setSession(session);
              
        
        if(request.getParameter("action") == null){
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("controle/index.jsp");
            try {
                requestDispatcher.forward(request, response);
            } catch (IOException ex) {
                ex.printStackTrace();
            } catch (ServletException ex) {
                ex.printStackTrace();
            }
        } else {
            this.executaRequisicao(request,response,session);
        }
    }
    
    public void executaRequisicao(HttpServletRequest request, HttpServletResponse response, HttpSession session){
        String[] voAction = request.getParameter("action").split("\\.");
        
        IControle oControle = null;
        
        try {
            oControle = (IControle)Class.forName("fachadaUsuario.controle."+voAction[0]+"CTR").newInstance();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        }
        
        if(voAction[1].equals("preparaLista")){
            try {
                oControle.preparaLista(request,response,session);
            } catch (ServletException ex) {
                ex.printStackTrace();
            }
        } else if(voAction[1].equals("preparaFormulario")){
            try {
                oControle.preparaFormulario(request,response,session);
            } catch (ServletException ex) {
                ex.printStackTrace();
            }
        } else if(voAction[1].equals("processaFormulario")){
            try {
                oControle.processaFormulario(request,response,session);
            } catch (ServletException ex) {
                ex.printStackTrace();
            }
        }
        
    }
    
    public HttpServletRequest getRequest() {
        return request;
    }
    
    public void setRequest(HttpServletRequest aRequest) {
        request = aRequest;
    }
    
    public HttpServletResponse getResponse() {
        return response;
    }
    
    public void setResponse(HttpServletResponse aResponse) {
        response = aResponse;
    }
    
    public HttpSession getSession() {
        return session;
    }
    
    public void setSession(HttpSession aSession) {
        session = aSession;
    }
}