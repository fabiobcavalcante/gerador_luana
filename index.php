﻿<?php
session_start();

$sMsg = $_SESSION['sMsg'];

if($_GET['logout'])
	session_destroy();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>GeneratorWeb - MVC</title>
  <meta charset="utf-8">
<link rel="icon" type="image/x-icon" href="img/favicon_gerador.png" />
<style type="text/css">
<!--
.style1 {
	font-size: 36px;
	font-weight: bold;
	color: rgb(37, 78, 145);
	font-family: Arial, Helvetica, sans-serif;
	margin-top:20px;

}
.style2 {color: #FFFFFF}
.style4 {color: #000000}
.style5 {
	color: #FF2222;
	font-weight: bold;
}

body{

background:linear-gradient(rgba(94,94,94,.1),rgba(94,94,94,.1)),url(img/bg.jpg) no-repeat top;
font-family:monospace;
}
-->
</style>
</head>

<body>
<form name="form1" method="post" action="seleciona_tabela.php">
   <table width="70%" border="0" align="center" cellpadding="2" cellspacing="2" style="background-color: #ffffff; margin-top:08%">
    <tr>
      <td colspan="2"><div align="center" class="style1"><img src="img/logo.png" class="img-responsive" alt="Imagem Responsiva" href="#" width="320"><br>
        <hr>
      </div></td>
    </tr>
	<?php if($_SESSION['sMsg']){?>
	<tr>
      <td colspan="2">&nbsp;</td>
    </tr>
	<tr>
      <td colspan="2"><div align="center" class="style5"><?=$sMsg?></div></td>
    </tr>
	<?php unset($_SESSION['sMsg']);
	   }?>
    <tr style="background-color: rgb(37, 78, 145);">
      <td colspan="2"><div align="center" class="style2">Informe o nome do projeto e os parametros de conex&atilde;o com o banco de dados </div></td>
    </tr>
    <tr>
      <td width="50%" bgcolor="#FFFFFF"><div align="right" class="style4">Nome do Projeto:</div></td>
      <td><input name="fProjeto" type="text" id="fProjeto" size="20"></td>
    </tr>
	<tr>
      <td bgcolor="#FFFFFF"><div align="right"><span class="style4">Módulo: </span></div></td>
      <td><input name="fModulo" type="text" id="fModulo" size="30"></td>
    </tr>
	<tr>
      <td width="50%" bgcolor="#FFFFFF"><div align="right" class="style4">SGBD:</div></td>
      <td><input name="fSGBD" type="radio" value="mysql" checked>MySQL&nbsp;&nbsp;&nbsp;<input name="fSGBD" type="radio" value="sqlsrv">MsSQL&nbsp;&nbsp;&nbsp;<input name="fSGBD" type="radio" value="pgsql">PgSQL</td>
    </tr>
    <tr>
      <td width="50%" bgcolor="#FFFFFF"><div align="right" class="style4">Servidor de Banco de Dados:</div></td>
      <td><input name="fServidor" type="text" id="fServidor" size="20"></td>
    </tr>
    <tr>
      <td width="50%" bgcolor="#FFFFFF"><div align="right" class="style4">Usu&aacute;rio do Banco de Dados:</div></td>
      <td><input name="fUsuario" type="text" id="fUsuario" size="40"></td>
    </tr>
    <tr>
      <td width="50%" bgcolor="#FFFFFF"><div align="right" class="style4">Senha do usuário:</div></td>
      <td><input name="fSenha" type="password" id="fServidor3" size="20"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="right"><span class="style4">Nome da Base de Dados: </span></div></td>
      <td><input name="fBanco" type="text" id="fSenha" size="30"></td>
    </tr>
	<tr>
      <td bgcolor="#FFFFFF"><div align="right"><span class="style4">Esquema: </span></div></td>
      <td><input name="fEsquema" type="text" id="fEsquema" size="30"> * Quando o banco não for MySQL</td>
    </tr>

	<tr>
      <td bgcolor="#FFFFFF"><div align="right"><span class="style4">Modelo de Permissão: </span></div></td>
      <td> <input name="fPermissao" type="radio" id="fPermissao" value="S"> Sim &nbsp;&nbsp;&nbsp;
			<input name="fPermissao" type="radio" id="fPermissao" value="N" checked >Não</td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="right"><span class="style4">Log: </span></div></td>
      <td><input name="fLog" type="radio" id="fLog" value="S">Sim &nbsp;&nbsp;&nbsp;
		  <input name="fLog" type="radio" id="fLog" value="N" checked >N&atilde;o</td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF">&nbsp;</td>
      <td><input type="submit" name="Submit" value="Conectar"></td>
    </tr>
  </table>
</form>
</body>
</html>
