<?php

require_once("classes/class.BancoDeDados.php");

 set_time_limit(3600); // 30 minutos

function geraMenu($vTabela,$sNomeFachada,$sPermissao){
	$sProjeto = "/luanapalheta/docs";
	foreach($vTabela as $oTabela){
		$vCampo = explode("||",$oTabela);
		$sNomeClasse = retornaNomeClasse($vCampo[0]);
		if(!($vCampo[1]))
			$sComentario = $vCampo[0];
		else
			$sComentario = $vCampo[1];
		$sCelula .= "	\t\t<li class=\"treeview\">\n
						\t\t\t<a href=\"#\">\n
						\t\t\t\t<i class=\"fa fa-edit\"></i> <span>".$sComentario."</span>\n
						\t\t\t\t<span class=\"pull-right-container\">\n
						\t\t\t\t\t<i class=\"fa fa-angle-left pull-right\"></i>\n
						\t\t\t\t</span>\n
						\t\t\t</a>\n
						\t\t<ul class=\"treeview-menu\">\n
						\t\t\t<li><a href=\"?action=".$sNomeClasse.".preparaLista\"><i class=\"fa fa-circle-o\"></i> Gerenciar ".$sComentario."</a></li>\n
						\t\t\t\t<li><a href=\"?action=".$sNomeClasse.".preparaFormulario&sOP=Cadastrar\"><i class=\"fa fa-circle-o\"></i> Cadastrar ".$sComentario."</a></li>\n
						\t\t\t</ul>\n
						\t\t</li>\n";
	}

	$_SERVER['DOCUMENT_ROOT'].$sProjeto."/gerador/modelo/menu.txt";

	$vMenu = file($_SERVER['DOCUMENT_ROOT'].$sProjeto."/gerador/modelo/menu.txt");
	$sMenu = implode(" ",$vMenu);
	$sMenu = str_replace("#CELULA_MENU#",$sCelula,$sMenu);

	if(is_file($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/paginas_geradas/menu.php'))
		unlink($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/paginas_geradas/menu.php');

	$fonte = fopen($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/paginas_geradas/menu.php','a+');
	if(fwrite($fonte,$sMenu))
		return true;
}

function geraIndex($sTabela,$sComentario,$sNomeFachada){
	$sProjeto = "/luanapalheta/docs";
	$oBancoDeDados = new BancoDeDados();
	$vvCampo = $oBancoDeDados->retornaCampo($sTabela);


	$vvCampoPrimario = $oBancoDeDados->retornaChavePrimaria($sTabela);
	$sNomeClasse = retornaNomeClasse($sTabela);
	$sSistema = strtoupper($_SESSION['sProjeto']);
	$sModulo = $_SESSION['sModulo'];
	$sNomeClasseMinusculo = strtolower($sNomeClasse);
	$sNomeColunas = retornaNomeColunas($vvCampo,$sNomeClasse);
	$sDadosColunas = retornaDadosColunas($vvCampo,$sNomeClasse);
	$sValueCamposPrimarios = retornaValueCamposPrimarios($vvCampoPrimario,$sNomeClasse);

	$vIndex = file("modelo/index.txt");
	$sIndex = implode(" ",$vIndex);
	$sIndex = str_replace("#NOME_CLASSE#",$sNomeClasse,$sIndex);
	$sIndex = str_replace("#SISTEMA#",$sSistema,$sIndex);
	$sIndex = str_replace("#MODULO#",$sModulo,$sIndex);
	$sIndex = str_replace("#COMENTARIO_TABELA#",$sComentario,$sIndex);
	$sIndex = str_replace("#NOME_CLASSE_MINUSCULO#",$sNomeClasseMinusculo,$sIndex);
	$sIndex = str_replace("#NOME_COLUNAS#",$sNomeColunas,$sIndex);
	$sIndex = str_replace("#DADOS_COLUNAS#",$sDadosColunas,$sIndex);
	$sIndex = str_replace("#NOME_FACHADA#",$sNomeFachada,$sIndex);
	$sIndex = str_replace("#VALUE_CAMPOS_PRIMARIOS#",$sValueCamposPrimarios,$sIndex);

	if(is_file($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/paginas_geradas/'.$sTabela.'/index.php'))
		unlink($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/paginas_geradas/'.$sTabela.'/index.php');
	else {
		if(!is_dir($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/paginas_geradas/'.$sTabela.'/'))
			mkdir($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/paginas_geradas/'.$sTabela.'/');
	}

	$fonte = fopen($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/paginas_geradas/'.$sTabela.'/index.php','a+');
	if(fwrite($fonte,$sIndex))
		return true;
}

function geraDetalhe($sTabela,$Comentario,$sNomeFachada){
$sProjeto = "/luanapalheta/docs";
	$oBancoDeDados = new BancoDeDados();
	$vvCampo = $oBancoDeDados->retornaCampo($sTabela);
	$vvCampoEstrangeiro = $oBancoDeDados->retornaChaveEstrangeira($sTabela);
	$vvCampoPrimario = $oBancoDeDados->retornaChavePrimaria($sTabela);
	$sNomeClasse = retornaNomeClasse($sTabela);
	$sSistema = strtoupper($_SESSION['sProjeto']);
	$sModulo = $_SESSION['sModulo'];
	$sNomeClasseMinusculo = strtolower($sNomeClasse);
	$sDetalhe = retornaFormularioDetalhe($vvCampo,$vvCampoEstrangeiro,$sNomeClasse);

	$vDetalhe = file("modelo/detalhe.txt");
	$sDetalhe2 = implode(" ",$vDetalhe);
	$sDetalhe2 = str_replace("#NOME_CLASSE#",$sNomeClasse,$sDetalhe2);
	$sDetalhe2 = str_replace("#SISTEMA#",$sSistema,$sDetalhe2);
	$sDetalhe2 = str_replace("#MODULO#",$sModulo,$sDetalhe2);
	$sDetalhe2 = str_replace("#COMENTARIO_TABELA#",$Comentario,$sDetalhe2);
	$sDetalhe2 = str_replace("#NOME_CLASSE_MINUSCULO#",$sNomeClasseMinusculo,$sDetalhe2);
	$sDetalhe2 = str_replace("#DETALHE#",$sDetalhe,$sDetalhe2);
	$sDetalhe2 = str_replace("#NOME_FACHADA#",$sNomeFachada,$sDetalhe2);


	if(is_file($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/paginas_geradas/'.$sTabela.'/detalhe.php'))
		unlink($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/paginas_geradas/'.$sTabela.'/detalhe.php');
	else {
		if(!is_dir($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/paginas_geradas/'.$sTabela.'/'))
     // chmod($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/paginas_geradas/,0777);
      mkdir($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/paginas_geradas/'.$sTabela.'/');
	}

	$fonte = fopen($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/paginas_geradas/'.$sTabela.'/detalhe.php','a+');
	if(fwrite($fonte,$sDetalhe2))
		return true;
}

function geraInsereAltera($sTabela,$sComentario,$sNomeFachada){
$sProjeto = "/luanapalheta/docs";
	$oBancoDeDados = new BancoDeDados();
	$vvCampo = $oBancoDeDados->retornaCampo($sTabela);
	$vvCampoEstrangeiro = $oBancoDeDados->retornaChaveEstrangeira($sTabela);
	$vvCampoPrimario = $oBancoDeDados->retornaChavePrimaria($sTabela);
	$sNomeClasse = retornaNomeClasse($sTabela);
	$sSistema = strtoupper($_SESSION['sProjeto']);
	$sModulo = $_SESSION['sModulo'];
	$sNomeClasseMinusculo = strtolower($sNomeClasse);
	$sFormularioInsereAltera = retornaFormularioInsereAltera($vvCampo,$vvCampoEstrangeiro,$sNomeClasse);
	$sGetVetorObjetos = retornaGetVetorObjetos($vvCampoEstrangeiro);
	$sInputHiddenPrimario = retornaInputHiddenPrimario($vvCampoPrimario,$sNomeClasse);
	$sMascaraJavascript = retornaMascaraJavascript($vvCampo,$sNomeClasse,$vvCampoEstrangeiro);

	$vInsereAltera = file("modelo/insere_altera.txt");
	$sInsereAltera = implode(" ",$vInsereAltera);
	$sInsereAltera = str_replace("#NOME_CLASSE#",$sNomeClasse,$sInsereAltera);
	$sInsereAltera = str_replace("#SISTEMA#",$sSistema,$sInsereAltera);
	$sInsereAltera = str_replace("#MODULO#",$sModulo,$sInsereAltera);
	$sInsereAltera = str_replace("#COMENTARIO_TABELA#",$sComentario,$sInsereAltera);
	$sInsereAltera = str_replace("#NOME_CLASSE_MINUSCULO#",$sNomeClasseMinusculo,$sInsereAltera);
	$sInsereAltera = str_replace("#FORMULARIO_INSERE_ALTERA#",$sFormularioInsereAltera,$sInsereAltera);
	$sInsereAltera = str_replace("#NOME_FACHADA#",$sNomeFachada,$sInsereAltera);
	$sInsereAltera = str_replace("#GET_VETOR_OBJETOS#",$sGetVetorObjetos,$sInsereAltera);
	$sInsereAltera = str_replace("#INPUT_HIDDEN_PRIMARIO#",$sInputHiddenPrimario,$sInsereAltera);
	$sInsereAltera = str_replace("#MASCARA_JAVASCRIPT#",$sMascaraJavascript,$sInsereAltera);


	if(is_file($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/paginas_geradas/'.$sTabela.'/insere_altera.php'))
		unlink($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/paginas_geradas/'.$sTabela.'/insere_altera.php');
	else {
		if(!is_dir($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/paginas_geradas/'.$sTabela.'/'))
			mkdir($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/paginas_geradas/'.$sTabela.'/');
	}

	$fonte = fopen($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/paginas_geradas/'.$sTabela.'/insere_altera.php','a+');
	if(fwrite($fonte,$sInsereAltera))
		return true;
}

function geraClasseCTR($sTabela,$sComentario,$sNomeFachada){
$sProjeto = "/luanapalheta/docs";
	$oBancoDeDados = new BancoDeDados();
	$vvCampo = $oBancoDeDados->retornaCampo($sTabela);
	$vvCampoEstrangeiro = $oBancoDeDados->retornaChaveEstrangeira($sTabela);
	$vvCampoPrimario = $oBancoDeDados->retornaChavePrimaria($sTabela);
	$sNomeClasse = retornaNomeClasse($sTabela);
	$sModulo = $_SESSION['sModulo'];
	$sParametroPost = retornaVariavelParametroPost($vvCampo);
	$sVetorObjetos = retornaSetVetorObjetos($vvCampoEstrangeiro,$sNomeFachada);
	$sVariaveisParametroExplode = retornaVariaveisParametrosExplode($vvCampoPrimario,$sNomeClasse);
	$sVariaveisParametroGet = retornaVariaveisParametroGet($vvCampoPrimario,$sNomeClasse);
	$sAddValidate = retornaAddValidate($vvCampo,$sNomeClasse);


	$vProcessa = file("modelo/class.CTR.txt");
	$sProcessa = implode(" ",$vProcessa);
	$sProcessa = str_replace("#NOME_CLASSE#",$sNomeClasse,$sProcessa);
	$sProcessa = str_replace("#MODULO#",$sModulo,$sProcessa);
	$sProcessa = str_replace("#SET_VETOR_OBJETOS#",$sVetorObjetos,$sProcessa);
	$sProcessa = str_replace("#ADD_VALIDATE#",$sAddValidate,$sProcessa);
	$sProcessa = str_replace("#VARIAVEL_PARAMETRO_POST#",$sParametroPost,$sProcessa);
	$sProcessa = str_replace("#NOME_FACHADA#",$sNomeFachada,$sProcessa);
	$sProcessa = str_replace("#NOME_TABELA#",$sTabela,$sProcessa);
	$sProcessa = str_replace("#COMENTARIO_TABELA#",$sComentario,$sProcessa);
	$sProcessa = str_replace("#VARIAVEIS_PARAMETROS_EXPLODE#",$sVariaveisParametroExplode,$sProcessa);
	$sProcessa = str_replace("#VARIAVEIS_PARAMETRO_GET#",$sVariaveisParametroGet,$sProcessa);

	if(is_file($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/controle/'.$sNomeClasse.'CTR.php'))
		unlink($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/controle/'.$sNomeClasse.'CTR.php');
	else {
		if(!is_dir($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/'))
			mkdir($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/');

		if(!is_dir($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/controle/'))
			mkdir($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/controle/');

	}

	$fonte = fopen($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/controle/'.$sNomeClasse.'CTR.php','a+');
	if(fwrite($fonte,$sProcessa))
		return true;
}

function geraFachada($vTabela,$sNomeFachada,$sNomeProjeto){
$sProjeto = "/luanapalheta/docs";
	$bResultado = true;
	//topo da fachada
	$sRequire = "";
	$sTopo = "<?\n".$sRequire."\n\n"."class Fachada".$sNomeFachada."BD {\n";

	if(is_file($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/BD/Fachada'.$sNomeFachada.'BD.php'))
		unlink($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/BD/Fachada'.$sNomeFachada.'BD.php');
	else {
		if(!is_dir($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/'))
			mkdir($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/');
		if(!is_dir($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/BD/'))
			mkdir($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/BD/');

	}

	$fonte = fopen($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/BD/Fachada'.$sNomeFachada.'BD.php','a+');
	fwrite($fonte,$sTopo);fclose($fonte);

	$oBancoDeDados = new BancoDeDados();

	foreach($vTabela as $oTabela){
		$vetTabela = explode("||",$oTabela);

		$sTabela = retornaNomeClasse($vetTabela[0]);

		$vvCampo = $oBancoDeDados->retornaCampo($vetTabela[0]);


		$vvCampoPrimario = $oBancoDeDados->retornaChavePrimaria($vetTabela[0]);
		$sChavePrimariaParametro = retornaVariavelParametro($vvCampoPrimario);
		$sParametro = retornaVariavelParametro($vvCampo);
		$sNomeClasse = retornaNomeClasse($vetTabela[0]);
		$sParametroPost = retornaVariavelParametroPost($vvCampo);
		$sParametroGet = retornaVariavelParametroGet($vvCampo);
		$sSetAtributos = retornaSetAtributos($vvCampo,$sNomeClasse);
		$sSetAtributosPrimario = retornaSetAtributos($vvCampoPrimario,$sNomeClasse);
		$sCamposPrimarios = retornaWhereCamposPrimarios($vvCampoPrimario);

		$vClasseFachada = file($_SERVER['DOCUMENT_ROOT'].$sProjeto."/gerador/modelo/class.FachadaMeio.txt");
		$sClasseFachada = implode(" ",$vClasseFachada);
		$sClasseFachada = str_replace("#SISTEMA#",$sNomeProjeto,$sClasseFachada);
		$sClasseFachada = str_replace("#NOME_CLASSE#",$sNomeClasse,$sClasseFachada);
		$sClasseFachada = str_replace("#ATRIBUTOS_PARAMETRO#",$sParametro,$sClasseFachada);
		$sClasseFachada = str_replace("#ATRIBUTOS_PRIMARIOS_PARAMETRO#",$sChavePrimariaParametro,$sClasseFachada);
		$sClasseFachada = str_replace("#PARAMETRO_POST#",$sParametroPost,$sClasseFachada);
		$sClasseFachada = str_replace("#PARAMETRO_GET#",$sParametroGet,$sClasseFachada);
		$sClasseFachada = str_replace("#SET_ATRIBUTOS#",$sSetAtributos,$sClasseFachada);
		$sClasseFachada = str_replace("#SET_ATRIBUTOS_PRIMARIOS#",$sSetAtributosPrimario,$sClasseFachada);
		$sClasseFachada = str_replace("#NOME_TABELA#",$vetTabela[0],$sClasseFachada);
		$sClasseFachada = str_replace("#WHERE_CAMPOS_PRIMARIO#",$sCamposPrimarios,$sClasseFachada);

		$fonte = fopen($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/BD/Fachada'.$sNomeFachada.'BD.php','a+');
		if(fwrite($fonte,$sClasseFachada))
			$bResultado &= true;
		else
			$bResultado &= false;

	}

	$fonte = fopen($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/BD/Fachada'.$sNomeFachada.'BD.php','a+');
	fwrite($fonte,"}\n?>");fclose($fonte);
	if($bResultado)
		return true;
	else
		return false;
}

function geraClasseBD($sTabela,$sComentario,$sNomeFachada){
$sProjeto = "/luanapalheta/docs";
	$oBancoDeDados = new BancoDeDados();
	$vvCampo = $oBancoDeDados->retornaCampo($sTabela);
	$vvCampoPrimario = $oBancoDeDados->retornaChavePrimaria($sTabela);
	$sChavePrimariaParametro = retornaVariavelParametro($vvCampoPrimario);
	$sNomeClasse = retornaNomeClasse($sTabela);
	$sCamposInserir = retornaCamposInserir($vvCampo);
	$sCamposInserirValores = retornaCamposInserirValores($vvCampo,$sNomeClasse);
	$sSetCampos = retornaSetCampos($vvCampoPrimario,$vvCampo,$sNomeClasse);
	$sWhereNaoVazio = retornaWhereNaoVazio($vvCampo,$sNomeClasse);
	$sCamposOreg = retornaCamposOreg($vvCampo);
	$sCamposPrimarios = retornaWhereCamposPrimarios($vvCampoPrimario);
	$sWhereCamposPrimariosAlterar = retornaWhereCamposPrimariosAlterar($vvCampoPrimario,$sNomeClasse);

	$vClasseBd = file("modelo/class.BD.txt");
	$sClasseBd = implode(" ",$vClasseBd);
	$sClasseBd = str_replace("#NOME_FACHADA#",$sNomeFachada,$sClasseBd);
	$sClasseBd = str_replace("#NOME_CLASSE#",$sNomeClasse,$sClasseBd);
	$sClasseBd = str_replace("#NOME_TABELA#",$sTabela,$sClasseBd);
	$sClasseBd = str_replace("#CAMPOS_INSERIR#",$sCamposInserir,$sClasseBd);
	$sClasseBd = str_replace("#CAMPOS_INSERIR_VALORES#",$sCamposInserirValores,$sClasseBd);
	$sClasseBd = str_replace("#SET_CAMPOS#",$sSetCampos,$sClasseBd);
	$sClasseBd = str_replace("#WHERE_NAO_VAZIO#",$sWhereNaoVazio,$sClasseBd);
	$sClasseBd = str_replace("#CAMPOS_OREG#",$sCamposOreg,$sClasseBd);
	$sClasseBd = str_replace("#WHERE_CAMPOS_PRIMARIO#",$sCamposPrimarios,$sClasseBd);
	$sClasseBd = str_replace("#ATRIBUTOS_PRIMARIOS_PARAMETRO#",$sChavePrimariaParametro,$sClasseBd);
	$sClasseBd = str_replace("#WHERE_CAMPOS_PRIMARIO_ALTERAR#",$sWhereCamposPrimariosAlterar,$sClasseBd);


	if(is_file($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/BD/'.$sNomeClasse.'BD.php'))
		unlink($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/BD/'.$sNomeClasse.'BD.php');
	else {
		if(!is_dir($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/'))
			mkdir($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/',0777,true);
		if(!is_dir($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/BD/'))
			mkdir($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/BD/',0777,true);

	}

	$fonte = fopen($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/BD/'.$sNomeClasse.'BD.php','a+');
	if(fwrite($fonte,$sClasseBd))
		return true;

}

function geraClasseSimples($sTabela,$sComentario,$sNomeFachada){
$sProjeto = "/luanapalheta/docs";
	$oBancoDeDados = new BancoDeDados();
	$vvCampo = $oBancoDeDados->retornaCampo($sTabela);
	//die();
	//print_r($vvCampo);
	//	die();
	$vvCampoForeign = $oBancoDeDados->retornaChaveEstrangeira($sTabela);
	$vvCampoPrimario = $oBancoDeDados->retornaChavePrimaria($sTabela);
	$sNomeClasse = retornaNomeClasse($sTabela);
	$sDeclaracaoVariaveis = retornaDeclaracaoVariavel($vvCampoPrimario,$vvCampo,$vvCampoForeign);
	$sParametro = retornaVariavelParametro($vvCampo);
	$sAtribuicaoVariavel = retornaAtribuicaoVariavel($vvCampo);
	$sGetSet = retornaGetSet($vvCampo,$vvCampoForeign,$sNomeFachada);

	$vClasseSimples = file("modelo/class.Simples.txt");
	$sClasseSimples = implode(" ",$vClasseSimples);
	$sClasseSimples = str_replace("#NOME_CLASSE#",$sNomeClasse,$sClasseSimples);
	$sClasseSimples = str_replace("#NOME_TABELA#",$sTabela,$sClasseSimples);
	$sClasseSimples = str_replace("#SGBD#",$_SESSION['sSGBD'],$sClasseSimples);
	$sClasseSimples = str_replace("#NOME_FACHADA#","fachada".$sNomeFachada,$sClasseSimples);
	$sClasseSimples = str_replace("#DECLARACAO_VARIAVEIS#",$sDeclaracaoVariaveis,$sClasseSimples);
	$sClasseSimples = str_replace("#ATRIBUTOS_PARAMETRO#",$sParametro,$sClasseSimples);
	$sClasseSimples = str_replace("#ATRIBUICAO_VARIAVEIS#",$sAtribuicaoVariavel,$sClasseSimples);
	$sClasseSimples = str_replace("#GET_SET#",$sGetSet,$sClasseSimples);




	if(is_file($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/modelo/'.$sNomeClasse.'.php')){
		unlink($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/modelo/'.$sNomeClasse.'.php');
	}else {
		if(!is_dir($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/')){
			mkdir($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/',0777,true);
		}
		if(!is_dir($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/modelo/')){
			//chmod($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada',0777);
			mkdir($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/modelo/',0777,true);
		}
	}

	$fonte = fopen($_SERVER['DOCUMENT_ROOT'].$sProjeto.'/gerador/classes_geradas/fachada'.$sNomeFachada.'/modelo/'.$sNomeClasse.'.php','a+');

	if(fwrite($fonte,$sClasseSimples))
		return true;

}

function retornaRequire($vTabela,$sNomeFachada){
	foreach($vTabela as $sTabela){
		$sRequire .= "require_once('classes/fachada".$sNomeFachada."/modelo/".retornaNomeClasse($sTabela).".php');\nrequire_once('classes/fachada".$sNomeFachada."/BD/".retornaNomeClasse($sTabela)."BD.php');\n";
	}
	return $sRequire;
}

function retornaNomeClasse($sTabela){
	$vNomeClasse = explode("_",$sTabela);
	for($i = 0; $i < count($vNomeClasse); $i++){
		$sNomeClasse .= ucfirst(strtolower($vNomeClasse[$i]));
	}

	return $sNomeClasse;
}

function retornaDeclaracaoVariavel($vvCampoPrimario,$vvCampo,$vvCampoForeign){
	$vNomeCampoPrimario = array();
/*
	print_r($vvCampoPrimario);
	echo "<br>";
	print_r($vvCampo,$vvCampoForeign);
	echo "<br>";
	print_r($vvCampoForeign);
die();*/
	foreach($vvCampoPrimario as $vCampoPrimario)
		$vNomeCampoPrimario[] = $vCampoPrimario['sNome'];



	foreach($vvCampo as $vCampo){

		$sNomeCampo = "";
		$sTipo = verificaTipo($vCampo['sTipo']);
		$bNulo = ($vCampo['bNulo'] == "YES") ? "true" : "false";
		$sTipoPHP = verificaTipoPHP($vCampo['sTipo']);
		$sPrimario = (!in_array($vCampo['sNome'],$vNomeCampoPrimario)) ? "false" : "true";
		$sAuto = ($vCampo['sAuto'] == "auto_increment") ? "true" : "false";
		$vNomeCampo = explode("_",$vCampo['sNome']);
		for($i = 0; $i < count($vNomeCampo); $i++){
			$sNomeCampo .= ucfirst(strtolower($vNomeCampo[$i]));
		}
		$sDeclaracao .= "/**\r\n";
		$sDeclaracao .= "\t* @campo ".$vCampo['sNome']."\r\n";
 	 	$sDeclaracao .= "\t* @var ".$sTipoPHP."\r\n";
 	 	$sDeclaracao .= "\t* @primario ".$sPrimario."\r\n";
 	 	$sDeclaracao .= "\t* @nulo ".$bNulo."\r\n";
		$sDeclaracao .= "\t* @auto-increment ".$sAuto."\r\n";
 	 	$sDeclaracao .= "\t*/\r\n";
		$sDeclaracao .= "\tprivate \$".$sTipo.$sNomeCampo.";\r\n\t";

	}
	if($vvCampoForeign){
		foreach($vvCampoForeign as $vCampoForeign){
			$sNomeCampo = retornaNomeClasse($vCampoForeign['sTabelaReferenciada']);
			$sDeclaracao .= "private \$o".$sNomeCampo.";\r\n\t";
		}
	}

	return $sDeclaracao;
}

function retornaVariavelParametro($vvCampo){
	foreach($vvCampo as $vCampo){
		$sNomeCampo = "";
		$sTipo = verificaTipo($vCampo['sTipo']);
		$vNomeCampo = explode("_",$vCampo['sNome']);
		for($i = 0; $i < count($vNomeCampo); $i++){
			$sNomeCampo .= ucfirst(strtolower($vNomeCampo[$i]));
		}
		$sParametro .= $sVirgula."\$".$sTipo.$sNomeCampo;
		$sVirgula = ",";
	}

	return $sParametro;
}

function retornaVariavelParametroGet($vvCampo){
	foreach($vvCampo as $vCampo){
		$sNomeCampo = "";
		$sTipo = verificaTipo($vCampo['sTipo']);
		$vNomeCampo = explode("_",$vCampo['sNome']);
		for($i = 0; $i < count($vNomeCampo); $i++){
			$sNomeCampo .= ucfirst(strtolower($vNomeCampo[$i]));
		}
		$sParametro .= $sVirgula."\$_GET['$sTipo".$sNomeCampo."']";
		$sVirgula = ",";
	}
	return $sParametro;
}

function retornaVariavelParametroPost($vvCampo){
	foreach($vvCampo as $vCampo){
		$sNomeCampo = "";
		$vNomeCampo = explode("_",$vCampo['sNome']);
		for($i = 0; $i < count($vNomeCampo); $i++){
			$sNomeCampo .= ucfirst(strtolower($vNomeCampo[$i]));
		}
		$sParametro .= $sVirgula."\$_POST['f".$sNomeCampo."']";
		$sVirgula = ",";
	}
	return $sParametro;
}

function retornaAtribuicaoVariavel($vvCampo){
	foreach($vvCampo as $vCampo){
		$sNomeCampo = "";
		$sTipo = verificaTipo($vCampo['sTipo']);
		$vNomeCampo = explode("_",$vCampo['sNome']);
		for($i = 0; $i < count($vNomeCampo); $i++){
			$sNomeCampo .= ucfirst(strtolower($vNomeCampo[$i]));
		}
		$sAtribuicao .= "\$this->set".$sNomeCampo."(\$".$sTipo.$sNomeCampo.");\r\n\t\t";
	}
	return $sAtribuicao;
}
function retornaGetSet($vvCampo,$vvCampoForeign,$sNomeFachada){

	foreach($vvCampo as $vCampo){
		$sNomeCampo = "";
		$sTipo = verificaTipo($vCampo['sTipo']);
		$vNomeCampo = explode("_",$vCampo['sNome']);
		$sTipoCampoFormatado = explode ("(",$vCampo['sTipo']);
		//print_r($sTipoCampoFormatado[0]);
		//die();

		for($i = 0; $i < count($vNomeCampo); $i++){
			$sNomeCampo .= ucfirst(strtolower($vNomeCampo[$i]));
		}
		$sGetSet .= "public function set".$sNomeCampo."(\$".$sTipo.$sNomeCampo."){\r\n\t\t";
		$sGetSet .= "\$this->".$sTipo.$sNomeCampo." = \$".$sTipo.$sNomeCampo.";\r\n\t}\r\n\t";
		$sGetSet .= "public function get".$sNomeCampo."(){\r\n\t\t";
		$sGetSet .= "return \$this->".$sTipo.$sNomeCampo.";\r\n\t}\r\n\t";

		if(($vCampo['sTipo'] == 'date') || ($vCampo['sTipo'] == 'datetime')){
			$sGetSet .= "public function get".$sNomeCampo."Formatado(){\r\n\t\t";
			$sGetSet .= "\$oData = new DateTime(\$this->".$sTipo.$sNomeCampo.");";
			$sGetSet .= "\r\t\t return \$oData->format(\"d/m/Y\");\r\n\t}\r\n\t";

			$sGetSet .= "public function set".$sNomeCampo."Banco(\$".$sTipo.$sNomeCampo."){\n\t";
			$sGetSet .= "\t if(\$".$sTipo.$sNomeCampo."){\n\t";
			$sGetSet .= "\t\t \$oData = DateTime::createFromFormat('d/m/Y', \$".$sTipo.$sNomeCampo.");\n\t";
			$sGetSet .= "\t\t \$this->".$sTipo.$sNomeCampo." = \$oData->format('Y-m-d') ;\r\n\t}\r\n\t";
			$sGetSet .= "\t }\n\t";

		}

		$vPermitido = array('real','decimal','float','numeric','double');

		if(in_array($sTipoCampoFormatado[0], $vPermitido)) {
			$sGetSet .= "public function get".$sNomeCampo."Formatado(){\r\n\t\t";
			$sGetSet .= " \$vRetorno = number_format(\$this->".$sTipo.$sNomeCampo." , 2, ',', '.');";
			$sGetSet .= "\r\t\t return \$vRetorno;\r\n\t}\r\n\t";

			$sGetSet .= "public function set".$sNomeCampo."Banco(\$".$sTipo.$sNomeCampo."){\r\n\t\t";
			$sGetSet .= "if(\$".$sTipo.$sNomeCampo."){\n\t\t\t";
			$sGetSet .= "\$sOrigem = array('.',',');\r\n\t\t\t";
			$sGetSet .= "\$sDestino = array('','.');\r\n\t\t\t";
			$sGetSet .= "\$this->".$sTipo.$sNomeCampo." = str_replace(\$sOrigem, \$sDestino, \$".$sTipo.$sNomeCampo.");\r\n\t\r\n\t\t";
			$sGetSet .= "}else{\n\t\t";
			$sGetSet .= "\$this->".$sTipo.$sNomeCampo." = 'null';\r\n\t\t\t}\r\n\t\t}\r\n";

		}

	}

	if($vvCampoForeign){
		foreach($vvCampoForeign as $vCampoForeign){
			$sNomeCampo = "";
			$sNomeCampo = retornaNomeClasse($vCampoForeign['sTabelaReferenciada']);
			$sNomeClasseReferenciada = retornaNomeClasse($vCampoForeign['sTabelaReferenciada']);
			$sNomeIdCampo = retornaNomeClasse($vCampoForeign['sNome']);
			//SET
			$sGetSet .= "public function set".$sNomeCampo."(\$o".$sNomeCampo."){\n\t\t";
			$sGetSet .= "\$this->o".$sNomeCampo." = \$o".$sNomeCampo.";\n\t}\n\t";
			//GET
			$sGetSet .= "public function get".$sNomeCampo."(){\n\t\t";
			$sGetSet .= "\$oFachada = new Fachada".$sNomeFachada."BD();\r\n\t\t";
			$sGetSet .= "\$this->o".$sNomeCampo." = \$oFachada->recuperarUm".$sNomeClasseReferenciada."(\$this->get".$sNomeIdCampo."());\r\n\t\t";
			$sGetSet .= "return \$this->o".$sNomeCampo.";\r\n\t}\n\t";
		}
	}

	return $sGetSet;
}

function verificaTipoPHP($sTipo){
	//---TIPOS MYSQL
	if(strpos($sTipo,"varchar") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"tinyint") !== false)
			$sTipo = "number";
	if(strpos($sTipo,"text") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"date") !== false)
			$sTipo = "date";
	if(strpos($sTipo,"smallint") !== false)
			$sTipo = "number";
	if(strpos($sTipo,"mediumint") !== false)
			$sTipo = "number";
	if(strpos($sTipo,"int") !== false)
			$sTipo = "number";
	if(strpos($sTipo,"bigint") !== false)
			$sTipo = "number";
	if(strpos($sTipo,"float") !== false)
			$sTipo = "number";
	if(strpos($sTipo,"double") !== false)
			$sTipo = "number";
	if(strpos($sTipo,"decimal") !== false)
			$sTipo = "number";
	if(strpos($sTipo,"datetime") !== false)
			$sTipo = "date";
	if(strpos($sTipo,"timestamp") !== false)
			$sTipo = "date";
	if(strpos($sTipo,"time") !== false)
			$sTipo = "date";
	if(strpos($sTipo,"year") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"date") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"tinyblob") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"tinytext") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"blob") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"mediumblob") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"mediumtext") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"longblob") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"longtext") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"enum") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"set") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"bool") !== false)
			$sTipo = "boolean";
	if(strpos($sTipo,"binary") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"varbinary") !== false)
			$sTipo = "String";
	//---FIM TIPOS MYSQL

	//TIPOS MSSQL
	if(strpos($sTipo,"image") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"text") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"uniqueidentifier") !== false)
			$sTipo = "number";
	if(strpos($sTipo,"tinyint") !== false)
			$sTipo = "number";
	if(strpos($sTipo,"smallint") !== false)
			$sTipo = "number";
	if(strpos($sTipo,"int") !== false)
			$sTipo = "number";
	if(strpos($sTipo,"smalldatetime") !== false)
			$sTipo = "date";
	if(strpos($sTipo,"real") !== false)
			$sTipo = "number";
	if(strpos($sTipo,"money") !== false)
			$sTipo = "number";
	if(strpos($sTipo,"datetime") !== false)
			$sTipo = "date";
	if(strpos($sTipo,"float") !== false)
			$sTipo = "number";
	if(strpos($sTipo,"sql_variant") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"ntext") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"bit") !== false)
			$sTipo = "boolean";
	if(strpos($sTipo,"decimal") !== false)
			$sTipo = "number";
	if(strpos($sTipo,"numeric") !== false)
			$sTipo = "number";
	if(strpos($sTipo,"smallmoney") !== false)
			$sTipo = "number";
	if(strpos($sTipo,"bigint") !== false)
			$sTipo = "number";
	if(strpos($sTipo,"varbinary") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"varchar") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"binary") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"char") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"timestamp") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"nvarchar") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"nchar") !== false)
			$sTipo = "String";
	if(strpos($sTipo,"xml") !== false)
			$sTipo = "String";
	//---FIM TIPOS MSSQL

	////TIPOS PGSQL
	if(strpos($sTipo,"bool") !== false)
		$sTipo = "boolean";
	if(strpos($sTipo,"bytea") !== false)
		$sTipo = "boolean";
	if(strpos($sTipo,"char") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"name") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"int8") !== false)
		$sTipo = "number";
	if(strpos($sTipo,"int2") !== false)
		$sTipo = "number";
	if(strpos($sTipo,"int2vector") !== false)
		$sTipo = "number";
	if(strpos($sTipo,"int4") !== false)
		$sTipo = "number";
	if(strpos($sTipo,"regproc") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"text") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"oid") !== false)
		$sTipo = "number";
	if(strpos($sTipo,"tid") !== false)
		$sTipo = "number";
	if(strpos($sTipo,"xid") !== false)
		$sTipo = "number";
	if(strpos($sTipo,"cid") !== false)
		$sTipo = "number";
	if(strpos($sTipo,"oidvector") !== false)
		$sTipo = "number";
	if(strpos($sTipo,"smgr") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"point") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"lseg") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"path") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"box") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"polygon") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"line") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"float4") !== false)
		$sTipo = "number";
	if(strpos($sTipo,"float8") !== false)
		$sTipo = "number";
	if(strpos($sTipo,"abstime") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"reltime") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"tinterval") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"circle") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"money") !== false)
		$sTipo = "number";
	if(strpos($sTipo,"macaddr") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"inet") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"inet") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"cidr") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"aclitem") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"bpchar") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"varchar") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"date") !== false)
		$sTipo = "date";
	if(strpos($sTipo,"time") !== false)
		$sTipo = "date";
	if(strpos($sTipo,"timestamp") !== false)
		$sTipo = "date";
	if(strpos($sTipo,"timestamptz") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"interval") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"timetz") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"bit") !== false)
		$sTipo = "number";
	if(strpos($sTipo,"varbit") !== false)
		$sTipo = "number";
	if(strpos($sTipo,"numeric") !== false)
		$sTipo = "number";
	if(strpos($sTipo,"refcursor") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"record") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"cstring") !== false)
		$sTipo = "String";
	if(strpos($sTipo,"cardinal_number") !== false)
		$sTipo = "number";
	if(strpos($sTipo,"time_stamp") !== false)
		$sTipo = "date";
	//---FIM TIPOS PGSQL

	return $sTipo;
}
function verificaTipo($sTipo){
	//---TIPOS MYSQL
	if(strpos($sTipo,"varchar") !== false)
			$sTipo = "s";
	if(strpos($sTipo,"tinyint") !== false)
			$sTipo = "n";
	if(strpos($sTipo,"text") !== false)
			$sTipo = "s";
	if(strpos($sTipo,"date") !== false)
			$sTipo = "d";
	if(strpos($sTipo,"smallint") !== false)
			$sTipo = "n";
	if(strpos($sTipo,"mediumint") !== false)
			$sTipo = "n";
	if(strpos($sTipo,"int") !== false)
			$sTipo = "n";
	if(strpos($sTipo,"bigint") !== false)
			$sTipo = "n";
	if(strpos($sTipo,"float") !== false)
			$sTipo = "n";
	if(strpos($sTipo,"double") !== false)
			$sTipo = "n";
	if(strpos($sTipo,"decimal") !== false)
			$sTipo = "n";
	if(strpos($sTipo,"datetime") !== false)
			$sTipo = "d";
	if(strpos($sTipo,"timestamp") !== false)
			$sTipo = "d";
	if(strpos($sTipo,"time") !== false)
			$sTipo = "d";
	if(strpos($sTipo,"year") !== false)
			$sTipo = "d";
	if(strpos($sTipo,"char") !== false)
			$sTipo = "s";
	if(strpos($sTipo,"tinyblob") !== false)
			$sTipo = "s";
	if(strpos($sTipo,"tinytext") !== false)
			$sTipo = "s";
	if(strpos($sTipo,"blob") !== false)
			$sTipo = "s";
	if(strpos($sTipo,"mediumblob") !== false)
			$sTipo = "s";
	if(strpos($sTipo,"mediumtext") !== false)
			$sTipo = "s";
	if(strpos($sTipo,"longblob") !== false)
			$sTipo = "s";
	if(strpos($sTipo,"longtext") !== false)
			$sTipo = "s";
	if(strpos($sTipo,"enum") !== false)
			$sTipo = "s";
	if(strpos($sTipo,"set") !== false)
			$sTipo = "s";
	if(strpos($sTipo,"bool") !== false)
			$sTipo = "b";
	if(strpos($sTipo,"binary") !== false)
			$sTipo = "s";
	if(strpos($sTipo,"varbinary") !== false)
			$sTipo = "s";
	//---FIM TIPOS MYSQL

	//TIPOS MSSQL
	if(strpos($sTipo,"image") !== false)
			$sTipo = "s";
	if(strpos($sTipo,"text") !== false)
			$sTipo = "s";
	if(strpos($sTipo,"uniqueidentifier") !== false)
			$sTipo = "n";
	if(strpos($sTipo,"tinyint") !== false)
			$sTipo = "n";
	if(strpos($sTipo,"smallint") !== false)
			$sTipo = "n";
	if(strpos($sTipo,"int") !== false)
			$sTipo = "n";
	if(strpos($sTipo,"smalldatetime") !== false)
			$sTipo = "d";
	if(strpos($sTipo,"real") !== false)
			$sTipo = "n";
	if(strpos($sTipo,"money") !== false)
			$sTipo = "n";
	if(strpos($sTipo,"datetime") !== false)
			$sTipo = "d";
	if(strpos($sTipo,"float") !== false)
			$sTipo = "n";
	if(strpos($sTipo,"sql_variant") !== false)
			$sTipo = "s";
	if(strpos($sTipo,"ntext") !== false)
			$sTipo = "s";
	if(strpos($sTipo,"bit") !== false)
			$sTipo = "b";
	if(strpos($sTipo,"decimal") !== false)
			$sTipo = "n";
	if(strpos($sTipo,"numeric") !== false)
			$sTipo = "n";
	if(strpos($sTipo,"smallmoney") !== false)
			$sTipo = "n";
	if(strpos($sTipo,"bigint") !== false)
			$sTipo = "n";
	if(strpos($sTipo,"varbinary") !== false)
			$sTipo = "s";
	if(strpos($sTipo,"varchar") !== false)
			$sTipo = "s";
	if(strpos($sTipo,"binary") !== false)
			$sTipo = "s";
	if(strpos($sTipo,"binary") !== false)
			$sTipo = "s";
	if(strpos($sTipo,"char") !== false)
			$sTipo = "s";
	if(strpos($sTipo,"timestamp") !== false)
			$sTipo = "d";
	if(strpos($sTipo,"nvarchar") !== false)
			$sTipo = "s";
	if(strpos($sTipo,"nchar") !== false)
			$sTipo = "s";
	if(strpos($sTipo,"xml") !== false)
			$sTipo = "s";
	//---FIM TIPOS MSSQL

	////TIPOS PGSQL
	if(strpos($sTipo,"bool") !== false)
		$sTipo = "b";
	if(strpos($sTipo,"bytea") !== false)
		$sTipo = "b";
	if(strpos($sTipo,"char") !== false)
		$sTipo = "s";
	if(strpos($sTipo,"name") !== false)
		$sTipo = "s";
	if(strpos($sTipo,"int8") !== false)
		$sTipo = "n";
	if(strpos($sTipo,"int2") !== false)
		$sTipo = "n";
	if(strpos($sTipo,"int2vector") !== false)
		$sTipo = "n";
	if(strpos($sTipo,"int4") !== false)
		$sTipo = "n";
	if(strpos($sTipo,"regproc") !== false)
		$sTipo = "s";
	if(strpos($sTipo,"text") !== false)
		$sTipo = "s";
	if(strpos($sTipo,"oid") !== false)
		$sTipo = "n";
	if(strpos($sTipo,"tid") !== false)
		$sTipo = "n";
	if(strpos($sTipo,"xid") !== false)
		$sTipo = "n";
	if(strpos($sTipo,"cid") !== false)
		$sTipo = "n";
	if(strpos($sTipo,"oidvector") !== false)
		$sTipo = "n";
	if(strpos($sTipo,"smgr") !== false)
		$sTipo = "s";
	if(strpos($sTipo,"point") !== false)
		$sTipo = "s";
	if(strpos($sTipo,"lseg") !== false)
		$sTipo = "s";
	if(strpos($sTipo,"path") !== false)
		$sTipo = "s";
	if(strpos($sTipo,"box") !== false)
		$sTipo = "s";
	if(strpos($sTipo,"polygon") !== false)
		$sTipo = "s";
	if(strpos($sTipo,"line") !== false)
		$sTipo = "s";
	if(strpos($sTipo,"float4") !== false)
		$sTipo = "n";
	if(strpos($sTipo,"float8") !== false)
		$sTipo = "n";
	if(strpos($sTipo,"abstime") !== false)
		$sTipo = "d";
	if(strpos($sTipo,"reltime") !== false)
		$sTipo = "d";
	if(strpos($sTipo,"tinterval") !== false)
		$sTipo = "d";
	if(strpos($sTipo,"circle") !== false)
		$sTipo = "s";
	if(strpos($sTipo,"money") !== false)
		$sTipo = "n";
	if(strpos($sTipo,"macaddr") !== false)
		$sTipo = "s";
	if(strpos($sTipo,"inet") !== false)
		$sTipo = "s";
	if(strpos($sTipo,"inet") !== false)
		$sTipo = "s";
	if(strpos($sTipo,"cidr") !== false)
		$sTipo = "s";
	if(strpos($sTipo,"aclitem") !== false)
		$sTipo = "s";
	if(strpos($sTipo,"bpchar") !== false)
		$sTipo = "s";
	if(strpos($sTipo,"varchar") !== false)
		$sTipo = "s";
	if(strpos($sTipo,"date") !== false)
		$sTipo = "d";
	if(strpos($sTipo,"time") !== false)
		$sTipo = "d";
	if(strpos($sTipo,"timestamp") !== false)
		$sTipo = "d";
	if(strpos($sTipo,"timestamptz") !== false)
		$sTipo = "d";
	if(strpos($sTipo,"interval") !== false)
		$sTipo = "d";
	if(strpos($sTipo,"timetz") !== false)
		$sTipo = "d";
	if(strpos($sTipo,"bit") !== false)
		$sTipo = "n";
	if(strpos($sTipo,"varbit") !== false)
		$sTipo = "n";
	if(strpos($sTipo,"numeric") !== false)
		$sTipo = "n";
	if(strpos($sTipo,"refcursor") !== false)
		$sTipo = "s";
	if(strpos($sTipo,"record") !== false)
		$sTipo = "s";
	if(strpos($sTipo,"cstring") !== false)
		$sTipo = "s";
	if(strpos($sTipo,"cardinal_number") !== false)
		$sTipo = "n";
	if(strpos($sTipo,"time_stamp") !== false)
		$sTipo = "d";
	//---FIM TIPOS PGSQL

	return $sTipo;
}

function retornaCamposInserir($vvCampo){
	foreach($vvCampo as $vCampo){
		if($vCampo['sNome'] != "id"){
			$sCamposInserir .= $sVirgula.$vCampo['sNome'];
			$sVirgula = ",";
		}
	}
	return $sCamposInserir;
}

function retornaCamposInserirValores($vvCampo,$sNomeClasse){


	foreach($vvCampo as $vCampo){
		$sTipo = verificaTipo($vCampo['sTipo']);
		$sAspas = "";
		if($sTipo == 's' || $sTipo == 'd')
			$sAspas = "'";
		$sNomeCampo = "";
		$vNomeCampo = explode("_",$vCampo['sNome']);
		for($i = 0; $i < count($vNomeCampo); $i++){
			$sNomeCampo .= ucfirst(strtolower($vNomeCampo[$i]));
		}
		if($sNomeCampo != "Id"){
			$sCamposInserir .= "$sVirgula".$sAspas."\".\$o".$sNomeClasse."->"."get".$sNomeCampo."().\"".$sAspas;
			$sVirgula = ",";
		}
	}
	$sCamposInserir = '"'.$sCamposInserir.'"';
	return $sCamposInserir;
}

function retornaSetCampos($vvCampoPrimario,$vvCampo,$sNomeClasse){
	$vNomeCampoPrimario = array();
	foreach($vvCampoPrimario as $vCampoPrimario)
		$vNomeCampoPrimario[] = $vCampoPrimario['sNome'];

	foreach($vvCampo as $vCampo){
		if(!in_array($vCampo['sNome'],$vNomeCampoPrimario)){
			$sTipo = verificaTipo($vCampo['sTipo']);
			$sAspas = "";
			if($sTipo == 's' || $sTipo == 'd')
				$sAspas = "'";
			$sNomeCampo = "";
			$vNomeCampo = explode("_",$vCampo['sNome']);
			for($i = 0; $i < count($vNomeCampo); $i++){
				$sNomeCampo .= ucfirst(strtolower($vNomeCampo[$i]));
			}
			$sSetCampos .= "\n\t\t if(\$o".$sNomeClasse."->get".$sNomeCampo."() !== NULL)
			\$sCampos .=  \"".$sVirgula.$vCampo['sNome']." = ".$sAspas."\".\$o".$sNomeClasse."->get".$sNomeCampo."().\"".$sAspas."\";";
			$sVirgula = ",";
		}
	}
	return $sSetCampos;
}
function retornaSetAtributos($vvCampo,$sNomeClasse){
//	print_r($vvCampo);
//		die();
	foreach($vvCampo as $vCampo){
		$sNomeCampo = "";
		$sTipo = verificaTipo($vCampo['sTipo']);
		$vNomeCampo = explode("_",$vCampo['sNome']);
		for($i = 0; $i < count($vNomeCampo); $i++){
			$sNomeCampo .= ucfirst(strtolower($vNomeCampo[$i]));
		}

		$vTipoCampo = explode ("(",$vCampo['sTipo']);
		$vPermitido = array('decimal','float','numeric','double','date','datetime','timestamp');

		if(in_array($vTipoCampo[0], $vPermitido)) {
			$sSetAtributos .= "\n\t\t\$o".$sNomeClasse."->set".$sNomeCampo."Banco(\$".$sTipo.$sNomeCampo.");";
		}else{
			$sSetAtributos .= "\n\t\t\$o".$sNomeClasse."->set".$sNomeCampo."(\$".$sTipo.$sNomeCampo.");";
		}

	}
	return $sSetAtributos;
}

function retornaWhereNaoVazio($vvCampo,$sNomeClasse){
	foreach($vvCampo as $vCampo){
		$sTipo = verificaTipo($vCampo['sTipo']);
		$sAspas = "";
		if($sTipo == 's' || $sTipo == 'd')
			$sAspas = "'";
		$sNomeCampo = "";
		$vNomeCampo = explode("_",$vCampo['sNome']);
		for($i = 0; $i < count($vNomeCampo); $i++){
			$sNomeCampo .= ucfirst(strtolower($vNomeCampo[$i]));
		}
		$sSetCampos .= "\n\t\t if(\$o".$sNomeClasse."->get".$sNomeCampo."())
		\t\$sComplemento .=  \""." AND ".$vCampo['sNome']." = ".$sAspas."\".\$o".$sNomeClasse."->get".$sNomeCampo."().\"".$sAspas."\";";
		$sAnd = " AND ";
	}
	return $sSetCampos;
}

function retornaCamposOreg($vvCampo){
	foreach($vvCampo as $vCampo){
		$sCamposOreg .= $sVirgula."\$oReg->".$vCampo['sNome'];
		$sVirgula = ",";
	}
	return $sCamposOreg;
}

function retornaWhereCamposPrimarios($vvCampo){
	$sAnd = " AND ";
	$sWhereCamposPrimarios = " WHERE ";
	foreach($vvCampo as $nIndice => $vCampo){
		$sTipo = verificaTipo($vCampo['sTipo']);
			$sAspas = "";
			if($sTipo == 's' || $sTipo == 'd')
				$sAspas = "'";
		if($nIndice == (sizeof($vvCampo)-1))
			$sAnd = '';
		//NOME DA VARI�VEL
		$sNomeCampo = "";
		$sTipo = verificaTipo($vCampo['sTipo']);
		$vNomeCampo = explode("_",$vCampo['sNome']);
		for($i = 0; $i < count($vNomeCampo); $i++){
			$sNomeCampo .= ucfirst(strtolower($vNomeCampo[$i]));
		}
		$sNomeCampo = "\$".$sTipo.$sNomeCampo;
		$sWhereCamposPrimarios .= $vCampo['sNome']." = ".$sAspas.$sNomeCampo.$sAspas.$sAnd;
	}
	return $sWhereCamposPrimarios;
}

function retornaWhereCamposPrimariosAlterar($vvCampo,$sNomeClasse){
	$sAnd = " AND ";
	$sWhereCamposPrimarios = " WHERE ";
	foreach($vvCampo as $nIndice => $vCampo){
		$sTipo = verificaTipo($vCampo['sTipo']);
			$sAspas = "";
			if($sTipo == 's' || $sTipo == 'd')
				$sAspas = "'";
		if($nIndice == (sizeof($vvCampo)-1))
			$sAnd = '';
		//NOME DA VARI�VEL
		$sNomeCampo = "";
		$sTipo = verificaTipo($vCampo['sTipo']);
		$vNomeCampo = explode("_",$vCampo['sNome']);
		for($i = 0; $i < count($vNomeCampo); $i++){
			$sNomeCampo .= ucfirst(strtolower($vNomeCampo[$i]));
		}
		$sWhereCamposPrimarios .= $vCampo['sNome']." = ".$sAspas."\".\$o".$sNomeClasse."->get".$sNomeCampo."().\"".$sAspas.$sAnd;
	}
	return $sWhereCamposPrimarios;
}

function recuperaInformacaoCampoEstrangeiro($sNomeCampo,$vvCampoEstrangeiro){
	$vNomeCampoEstrangeiro = array();
	if($vvCampoEstrangeiro){
		foreach($vvCampoEstrangeiro as $vCampoForeign)
			$vNomeCampoEstrangeiro[] = $vCampoForeign['sNome'];
	}
	if(in_array($sNomeCampo,$vNomeCampoEstrangeiro)){
		foreach($vvCampoEstrangeiro as $vCampoForeign){
			if($sNomeCampo == $vCampoForeign['sNome']){
				return $vCampoForeign;
			}
		}
	}
	return false;
}


function retornaMascaraJavascript($vvCampo,$sNomeClasse,$vvCampoEstrangeiro){
	$nChaveEstrangeira=0;

	foreach($vvCampo as $nIndice => $vCampo){
		$sNomeCampoOriginal = $vCampo['sNome'];
		$sNomeCampo = '';
		$vNomeCampo = explode("_",$vCampo['sNome']);
		for($i = 0; $i < count($vNomeCampo); $i++){
			$sNomeCampo .= ucfirst(strtolower($vNomeCampo[$i]));
		}

		// VERIFICAR SE � NUMERICO OU DATA SE FOR COLOCAR MASCARA

		$vTipoCampo = explode ("(",$vCampo['sTipo']);
		$vMoney = array('decimal','float','numeric','double');
		$vData = array('date','datetime');

		if(in_array($vTipoCampo[0], $vMoney)) {
			$sMascara .= "\n\t\t\t\$(\"#".$sNomeCampo."\").maskMoney(	{symbol:'R$ ', showSymbol:false, thousands:'.', decimal:',', symbolStay: true});";
		}
		if(in_array($vTipoCampo[0], $vData)) {
			$sMascara .= "\n\t\t\t\$(\"#".$sNomeCampo."\").inputmask(\"99/99/9999\"), { 'placeholder': \"99/99/9999\" };";
		}
		if($vTipoCampo[0] == 'time'){
			$sMascara .= "\n\t\t\t\$(\"#".$sNomeCampo."\").inputmask(\"99:99\"), { 'placeholder': \"00:00\" };";
		}


		if($vCampoForeign = recuperaInformacaoCampoEstrangeiro($sNomeCampoOriginal,$vvCampoEstrangeiro) ){
			$nChaveEstrangeira++;
		}

	}//foreach($vvCampo as $nIndice => $vCampo){

	if ($nChaveEstrangeira > 0){
		$sChosen = "\n\t\t\t jQuery(document).ready(function(){jQuery(\".chosen\").data(\"placeholder\",\"Selecione\").chosen(); })";
	}

	$sMascaraFinal = $sChosen;
	$sMascaraFinal .= "\n\t\t\t jQuery(function(\$){ ";
	$sMascaraFinal .= $sMascara;
	$sMascaraFinal .= "\n\t\t\t\t });";

	return $sMascaraFinal;
}



function retornaFormularioInsereAltera($vvCampo,$vvCampoEstrangeiro,$sNomeClasse){

	foreach($vvCampo as $nIndice => $vCampo){
		$sObrigatorio = ($vCampo['bNulo'] == 'YES') ? "" : " required ";
		$sComentario = $vCampo['sComentario'];
		$sNomeCampoOriginal = $vCampo['sNome'];
		$sNomeCampo = '';
		$vNomeCampo = explode("_",$vCampo['sNome']);
		for($i = 0; $i < count($vNomeCampo); $i++){
			$sNomeCampo .= ucfirst(strtolower($vNomeCampo[$i]));
		}

		if($vCampo['sAuto'] == "auto_increment"){
			$sFormularioInsereAltera .= "\t\t\t\t<input type='hidden' name='f".$sNomeCampo."' value='<?php echo (\$o".$sNomeClasse.") ? \$o".$sNomeClasse."->get".$sNomeCampo."() : \"\"?>'/>\n";
		}else{
			if(!$vCampoForeign = recuperaInformacaoCampoEstrangeiro($sNomeCampoOriginal,$vvCampoEstrangeiro)){
				if($sNomeCampo == 'Ativo'){

					$sFormularioInsereAltera .= "\t\t\t\t\t<input type='hidden' name='f".$sNomeCampo."' value='<?php echo (\$o".$sNomeClasse.") ? \$o".$sNomeClasse."->get".$sNomeCampo."() : \"1\"?>'/>\n";
				}else{
					$sFormularioInsereAltera .= "\t\t\t\t\n <div class=\"form-group col-md-4\">\n";
					$sFormularioInsereAltera .= "\t\t\t\t\t<label for=\"".$sNomeCampo."\">".$sComentario.":</label>\n";
					$sFormularioInsereAltera .= "\t\t\t\t\t<div class=\"input-group col-md-11\">\n";
					// verifica se � inteiro
					$vInteiro = explode('(',$vCampo['sTipo']);
					$vPermitido = array('int','integer','smallint','tinyint','mediumint','bigint');
					//print_r($vInteiro);

					if(in_array($vInteiro[0], $vPermitido)) {
						$TodosNumero = "onKeyPress=\"TodosNumero(event);\"";
					}else{
						$TodosNumero = "";
					}

					$vTipoCampo = explode ("(",$vCampo['sTipo']);
					$vPermitido = array('real','decimal','float','numeric','double','date','datetime');
					$sFormatado = (in_array($vTipoCampo[0], $vPermitido)) ? "Formatado" : "";

					$sFormularioInsereAltera .= "\t\t\t\t\t<input class=\"form-control\" type='text' id='".$sNomeCampo."' placeholder='".$sComentario."' name='f".$sNomeCampo."' $sObrigatorio $TodosNumero value='<?php echo (\$o".$sNomeClasse.") ? \$o".$sNomeClasse."->get".$sNomeCampo . $sFormatado."() : \"\"?>'/>\n";
					$sFormularioInsereAltera .= "\t\t\t\t</div>\n";
					$sFormularioInsereAltera .= "\t\t\t</div>\n";
				}
			} else {
				$sNomeCampo = retornaNomeClasse($vCampoForeign['sTabelaReferenciada']);
				$sNomeCampoReferenciado = retornaNomeClasse($vCampoForeign['sCampoReferenciado']);
				$sNomeClasseReferenciada = retornaNomeClasse($vCampoForeign['sTabelaReferenciada']);
				$sNomeIdCampo = retornaNomeClasse($vCampoForeign['sNome']);

				$sFormularioInsereAltera .= "\t\t\t\t\n <div class=\"form-group col-md-4\">\n";
				$sFormularioInsereAltera .= "\t\t\t\t\t<label for=\"".$sNomeCampo."\">".$sComentario.":</label>\n";
				$sFormularioInsereAltera .= "\t\t\t\t\t<div class=\"input-group col-md-11\">\n";
				$sFormularioInsereAltera .= "\t\t\t\t\t<select name='f".$sNomeIdCampo."'  class=\"form-control select2\" $sObrigatorio >\n";
					  $sFormularioInsereAltera .= "\t\t\t\t\t\t<option value=''>Selecione</option>\n";
				$sFormularioInsereAltera .= "\t\t\t\t\t\t<?php \$sSelected = \"\";\n";
				$sFormularioInsereAltera .= "\t\t\t\t\t\t   if(\$vo".$sNomeCampo."){\n";
					  $sFormularioInsereAltera .= "\t\t\t\t\t\t\t   foreach(\$vo".$sNomeCampo." as \$o".$sNomeCampo."){\n";
					  $sFormularioInsereAltera .= "\t\t\t\t\t\t\t\t   if(\$o".$sNomeClasse."){\n";
					  $sFormularioInsereAltera .= "\t\t\t\t\t\t\t\t\t   \$sSelected = (\$o".$sNomeClasse."->get".$sNomeIdCampo."() == \$o".$sNomeCampo."->get".$sNomeCampoReferenciado."()) ? \"selected\" : \"\";\n";
				$sFormularioInsereAltera .= "\t\t\t\t\t\t\t\t   }\n";
					  $sFormularioInsereAltera .= "\t\t\t\t\t\t?>\n";
					  $sFormularioInsereAltera .= "\t\t\t\t\t\t\t\t   <option  <?php echo \$sSelected?> value='<?php echo \$o".$sNomeCampo."->get".$sNomeCampoReferenciado."()?>'><?php echo \$o".$sNomeCampo."->get".$sNomeCampoReferenciado."()?></option>\n";
				$sFormularioInsereAltera .= "\t\t\t\t\t\t<?php\n";
				$sFormularioInsereAltera .= "\t\t\t\t\t\t\t   }\n";
					$sFormularioInsereAltera .= "\t\t\t\t\t\t   }\n";
				$sFormularioInsereAltera .= "\t\t\t\t\t\t?>\n";
				$sFormularioInsereAltera .= "\t\t\t\t\t</select>\n";
				$sFormularioInsereAltera .= "\t\t\t</div>\n";
				$sFormularioInsereAltera .= "\t\t\t\t</div>\n";

			}
		}
	}//foreach($vvCampo as $nIndice => $vCampo){


	return $sFormularioInsereAltera;
}

function retornaFormularioDetalhe($vvCampo,$vvCampoEstrangeiro,$sNomeClasse){
	foreach($vvCampo as $nIndice => $vCampo){
		$sComentario = $vCampo['sComentario'];
		if($vCampo['sAuto'] != "auto_increment"){
			$sNomeCampoOriginal = $vCampo['sNome'];
			$sNomeCampo = '';
			$vNomeCampo = explode("_",$vCampo['sNome']);
			for($i = 0; $i < count($vNomeCampo); $i++){
				$sNomeCampo .= ucfirst(strtolower($vNomeCampo[$i]));
			}

			if($sNomeCampo != 'Ativo'){
				$sDetalhe .= "\t\n <div class=\"col-md-4\">\n";
				$sDetalhe .= "\t\t\n <label for=\"".$sNomeCampo."\" class=\"control-label\">".$sComentario.":</label>\n";
				$sDetalhe .= "\t\t<p><?php echo (\$o".$sNomeClasse.") ? \$o".$sNomeClasse."->get".$sNomeCampo ."() : \"\"?></p>\n";
				$sDetalhe .= "\t</div>\n";
			}
		}
	}//foreach($vvCampo as $nIndice => $vCampo){

	return $sDetalhe;

}

function retornaNomeColunas($vvCampo,$sNomeClasse){
		foreach($vvCampo as $nIndice => $vCampo){
		$sNomeCampo = '';
		$vNomeCampo = explode("_",$vCampo['sComentario']);

		for($i = 0; $i < count($vNomeCampo); $i++){
			$sNomeCampo .= ucfirst(strtolower($vNomeCampo[$i]));
		}

		if($sNomeCampo !='Ativo' && $sNomeCampo !='IncluidoPor' && $sNomeCampo !='AlteradoPor'){
			$sNomeColunas .= $sBarraT."<th>".$sNomeCampo."</th>\n";
			$sBarraT = "\t\t\t\t\t";
		}
	}//foreach($vvCampo as $nIndice => $vCampo){

	return $sNomeColunas;
}

function retornaDadosColunas($vvCampo,$sNomeClasse){
	foreach($vvCampo as $nIndice => $vCampo){
		$sNomeCampo = '';
		$vNomeCampo = explode("_",$vCampo['sNome']);
		for($i = 0; $i < count($vNomeCampo); $i++){
			$sNomeCampo .= ucfirst(strtolower($vNomeCampo[$i]));
		}

		if(strtolower($sNomeCampo) !='ativo'){
			$vTipoCampo = explode ("(",$vCampo['sTipo']);
			$vPermitido = array('real','decimal','float','numeric','double','date','datetime');
			if(in_array($vTipoCampo[0], $vPermitido)) {
				$sDadosColunas .= $sBarraT."<td><?php echo \$o".$sNomeClasse."->get".$sNomeCampo."Formatado()?></td>\n";
			}else{
				$sDadosColunas .= $sBarraT."<td><?php echo \$o".$sNomeClasse."->get".$sNomeCampo."()?></td>\n";
			}
			$sBarraT = "\t\t\t\t\t";
		}
	}//foreach($vvCampo as $nIndice => $vCampo){
	return $sDadosColunas;
}


function retornaSetVetorObjetos($vvCampoEstrangeiro,$sNomeFachada){
	$sVetorObjetos = "";
	if($vvCampoEstrangeiro){
		foreach($vvCampoEstrangeiro as $vCampoForeign){
			$sTipoJava = retornaNomeClasse($vCampoForeign['sTabelaReferenciada']);
			$sNomeCampo = retornaNomeClasse($vCampoForeign['sTabelaReferenciada']);
			$sNomeClasseReferenciada = retornaNomeClasse($vCampoForeign['sTabelaReferenciada']);
			$sVetorObjetos .= "\$_REQUEST['vo".$sNomeCampo."'] = \$oFachada->recuperarTodos".$sNomeClasseReferenciada."();\n\r\t\t";
		}
	}
	return $sVetorObjetos;
}

function retornaGetVetorObjetos($vvCampoEstrangeiro){
	$sVetorObjetos = "";
	if($vvCampoEstrangeiro){
		foreach($vvCampoEstrangeiro as $vCampoForeign){
			$sTipoJava = retornaNomeClasse($vCampoForeign['sTabelaReferenciada']);
			$sNomeCampo = retornaNomeClasse($vCampoForeign['sTabelaReferenciada']);
			$sNomeClasseReferenciada = retornaNomeClasse($vCampoForeign['sTabelaReferenciada']);

			$sVetorObjetos .= "\$vo".$sNomeCampo." = \$_REQUEST['vo".$sNomeCampo."'];\n\r";
		}

	}
	return $sVetorObjetos;
}

function retornaValueCamposPrimarios($vvCampo, $sNomeClasse){
	$sValueCamposPrimarios = "";
	foreach($vvCampo as $vCampo){
		$sNomeCampo = retornaNomeClasse($vCampo["sNome"]);
		$sValueCamposPrimarios .= $sPipe."<?=\$o".$sNomeClasse."->get".$sNomeCampo."()?>";
		$sPipe = "||";

	}
	return $sValueCamposPrimarios;
}
function retornaInputHiddenPrimario($vvCampo, $sNomeClasse){
	$sInputHiddenPrimario = "";
	foreach($vvCampo as $vCampo){
		$sNomeCampo = retornaNomeClasse($vCampo["sNome"]);
		$sInputHiddenPrimario .= $sPipe."<input type=\"hidden\" name=\"f".$sNomeCampo."\" value=\"<?=(is_object(\$o".$sNomeClasse.")) ? \$o".$sNomeClasse."->get".$sNomeCampo."() : \"\"?>\" />";
		$sPipe = "\r\n\t\t\t";

	}

	return $sInputHiddenPrimario;

}

function retornaVariaveisParametrosExplode($vvCampo, $sNomeClasse){
	$sVariaveisParametroExplode = "";
	foreach($vvCampo as $nIdice => $vCampo){
		$sNomeCampo = retornaNomeClasse($vCampo["sNome"]);
		$sVariaveisParametroExplode .= $sPipe."\$vId".$sNomeClasse."[".$nIdice."]";
		$sPipe = ",";

	}
	return $sVariaveisParametroExplode;

}

function retornaVariaveisParametroGet($vvCampo, $sNomeClasse){
	$sVariaveisParametroGet = "";
	foreach($vvCampo as $vCampo){
		$sNomeCampo = retornaNomeClasse($vCampo["sNome"]);
		$sVariaveisParametroGet .= $sPipe."\".\$_POST['f".$sNomeCampo."'].";
		$sPipe = "\"||";

	}
	$sVariaveisParametroGet .= "\"\"";
	return $sVariaveisParametroGet;
}

function retornaAddValidate($vvCampo,$sNomeClasse){

	foreach($vvCampo as $nIndice => $vCampo){
		if($vCampo['sAuto'] != "auto_increment"){
			$sTipo = verificaTipo($vCampo['sTipo']);
			switch($sTipo){
				case "n":
					$sTipo = "number";
				break;
				case "d":
					$sTipo = "date";
				break;
				case "s":
					$sTipo = "text";
				break;
			}
			if($sTipo != "b"){
				$sNomeCampo = '';
				$vNomeCampo = explode("_",$vCampo['sNome']);
				for($i = 0; $i < count($vNomeCampo); $i++){
					$sNomeCampo .= ucfirst(strtolower($vNomeCampo[$i]));
				}

				$sComentada = ($vCampo['bNulo'] != 'NO') ? "//" : "";

				$sAddValidate .= $sTab.$sComentada . "\$oValidate->add_".$sTipo."_field(\"".$sNomeCampo."\", \$o".$sNomeClasse."->get".$sNomeCampo."(), \"".$sTipo."\", \"y\");\n";
				$sTab = "\t\t\t";
			}
		}
	}//foreach($vvCampo as $nIndice => $vCampo){
	return $sAddValidate;
}


?>
