<?php
 /**
  * @author Auto-Generated 
  * @package fachadaLog 
  * @SGBD mysql 
  * @tabela acesso_log 
  */
 class AcessoLog{
 	/**
	* @campo cod_acesso
	* @var number
	* @primario true
	* @auto-increment true
	*/
	private $nCodAcesso;
	/**
	* @campo usuario
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sUsuario;
	/**
	* @campo tabela
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sTabela;
	/**
	* @campo operacao
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sOperacao;
	/**
	* @campo data_log
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $dDataLog;
	/**
	* @campo descricao
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sDescricao;
	/**
	* @campo sql_descricao
	* @var String
	* @primario false
	* @auto-increment false
	*/
	private $sSqlDescricao;
	/**
	* @campo ativo
	* @var number
	* @primario false
	* @auto-increment false
	*/
	private $nAtivo;
	
 	
 	public function __construct(){
 		
 	}
 	
 	public function setCodAcesso($nCodAcesso){
		$this->nCodAcesso = $nCodAcesso;
	}
	public function getCodAcesso(){
		return $this->nCodAcesso;
	}
	public function setUsuario($sUsuario){
		$this->sUsuario = $sUsuario;
	}
	public function getUsuario(){
		return $this->sUsuario;
	}
	public function setTabela($sTabela){
		$this->sTabela = $sTabela;
	}
	public function getTabela(){
		return $this->sTabela;
	}
	public function setOperacao($sOperacao){
		$this->sOperacao = $sOperacao;
	}
	public function getOperacao(){
		return $this->sOperacao;
	}
	public function setDataLog($dDataLog){
		$this->dDataLog = $dDataLog;
	}
	public function getDataLog(){
		return $this->dDataLog;
	}
	public function setDescricao($sDescricao){
		$this->sDescricao = $sDescricao;
	}
	public function getDescricao(){
		return $this->sDescricao;
	}
	public function setSqlDescricao($sSqlDescricao){
		$this->sSqlDescricao = $sSqlDescricao;
	}
	public function getSqlDescricao(){
		return $this->sSqlDescricao;
	}
	public function setAtivo($nAtivo){
		$this->nAtivo = $nAtivo;
	}
	public function getAtivo(){
		return $this->nAtivo;
	}
	
 }
 ?>
