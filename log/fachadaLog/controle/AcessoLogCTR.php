<?php
 class AcessoLogCTR implements IControle{
 
 	public function AcessoLogCTR(){
 	
 	}
 
 	public function preparaLista(){
 		$oFachada = new FachadaLogBD();
 
 		$voAcessoLog = $oFachada->recuperarTodosAcessoLog();
 
 		$_REQUEST['voAcessoLog'] = $voAcessoLog;
 		
 		
 		include_once("controle/acesso_log/index.php");
 
 		exit();
 	
 	}
 
 	public function preparaFormulario(){
 		$oFachada = new FachadaLogBD();
 
 		$oAcessoLog = false;
 		
 		if($_REQUEST['sOP'] == "Alterar" || $_REQUEST['sOP'] == "Detalhar"){
 			$nIdAcessoLog = ($_POST['fIdAcessoLog'][0]) ? $_POST['fIdAcessoLog'][0] : $_GET['nIdAcessoLog'];
 	
 			if($nIdAcessoLog){
 				$vIdAcessoLog = explode("||",$nIdAcessoLog);
 				$oAcessoLog = $oFachada->recuperarUmAcessoLog($vIdAcessoLog[0]);
 			}
 		}
 		
 		$_REQUEST['oAcessoLog'] = ($_SESSION['oAcessoLog']) ? $_SESSION['oAcessoLog'] : $oAcessoLog;
 		unset($_SESSION['oAcessoLog']);
 
 		
 		
 		if($_REQUEST['sOP'] == "Detalhar")
 			include_once("controle/acesso_log/detalhe.php");
 		else
 			include_once("controle/acesso_log/insere_altera.php");
 
 		exit();
 	
 	}
 
 	public function processaFormulario(){
 		$oFachada = new FachadaLogBD();
 
 		$sOP = (array_key_exists('sOP',$_POST)) ? $_POST['sOP'] : $_GET['sOP'];
 
 		if($sOP != "Excluir"){
 			$oAcessoLog = $oFachada->inicializarAcessoLog($_POST['fCodAcesso'],$_POST['fUsuario'],$_POST['fTabela'],$_POST['fOperacao'],$_POST['fDataLog'],$_POST['fDescricao'],$_POST['fSqlDescricao'],$_POST['fAtivo']);
 			$_SESSION['oAcessoLog'] = $oAcessoLog;
 			
 			$oValidate = FabricaUtilitario::getUtilitario("Validate");
 			$oValidate->check_4html = true;
 		
 			$oValidate->add_text_field("Usuario", $oAcessoLog->getUsuario(), "text", "y");
			$oValidate->add_text_field("Tabela", $oAcessoLog->getTabela(), "text", "y");
			$oValidate->add_text_field("Operacao", $oAcessoLog->getOperacao(), "text", "y");
			$oValidate->add_date_field("DataLog", $oAcessoLog->getDataLog(), "date", "y");
			$oValidate->add_text_field("Descricao", $oAcessoLog->getDescricao(), "text", "y");
			$oValidate->add_text_field("SqlDescricao", $oAcessoLog->getSqlDescricao(), "text", "y");
			$oValidate->add_number_field("Ativo", $oAcessoLog->getAtivo(), "number", "y");

 			
 			if (!$oValidate->validation()) {
 				$_SESSION['sMsg'] = $oValidate->create_msg();
 				$sHeader = "?bErro=1&action=AcessoLog.preparaFormulario&sOP=".$sOP."&nIdAcessoLog=".$_POST['fCodAcesso']."";
 				header("Location: ".$sHeader);	
 				die();
 			}
 		}
 
 		switch($sOP){
 			case "Cadastrar":
 				if($oFachada->inserirAcessoLog($oAcessoLog)){
 					unset($_SESSION['oAcessoLog']);
 					$_SESSION['sMsg'] = "AcessoLog inserido com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoLog.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N�o foi poss�vel inserir o AcessoLog!";
 					$sHeader = "?bErro=1&action=AcessoLog.preparaFormulario&sOP=".$sOP;
 				}
 			break;
 			case "Alterar":
 				if($oFachada->alterarAcessoLog($oAcessoLog)){
 					unset($_SESSION['oAcessoLog']);
 					$_SESSION['sMsg'] = "AcessoLog alterado com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoLog.preparaLista";
 					
 				} else {
 					$_SESSION['sMsg'] = "N�o foi poss�vel alterar o AcessoLog!";
 					$sHeader = "?bErro=1&action=AcessoLog.preparaFormulario&sOP=".$sOP."&nIdAcessoLog=".$_POST['fCodAcesso']."";
 				}
 			break;
 			case "Excluir":
 				$bResultado = true;
 				foreach($_POST['fIdAcessoLog'] as $nIdAcessoLog){
 					$vIdAcessoLog = explode("||",$nIdAcessoLog);
 					$bResultado &= $oFachada->excluirAcessoLog($vIdAcessoLog[0]);
 				}
 		
 				if($bResultado){
 					$_SESSION['sMsg'] = "AcessoLog(s) exclu�do(s) com sucesso!";
 					$sHeader = "?bErro=0&action=AcessoLog.preparaLista";
 				} else {
 					$_SESSION['sMsg'] = "N�o foi poss�vel excluir o(s) AcessoLog!";
 					$sHeader = "?bErro=1&action=AcessoLog.preparaLista";
 				}
 			break;
 		}
 
 		header("Location: ".$sHeader);		
 	
 	}
 
 }
 
 
 ?>