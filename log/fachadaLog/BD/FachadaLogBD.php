<?


class FachadaLogBD {
	/**
 	 *
 	 * M�todo para inicializar um Objeto AcessoLog
 	 *
 	 * @return AcessoLog  
 	 */
 	public function inicializarAcessoLog($nCodAcesso,$sUsuario,$sTabela,$sOperacao,$dDataLog,$sDescricao,$sSqlDescricao,$nAtivo){
 		$oAcessoLog = new AcessoLog();
 		
		$oAcessoLog->setCodAcesso($nCodAcesso);
		$oAcessoLog->setUsuario($sUsuario);
		$oAcessoLog->setTabela($sTabela);
		$oAcessoLog->setOperacao($sOperacao);
		$oAcessoLog->setDataLog($dDataLog);
		$oAcessoLog->setDescricao($sDescricao);
		$oAcessoLog->setSqlDescricao($sSqlDescricao);
		$oAcessoLog->setAtivo($nAtivo);
 		return $oAcessoLog;
 	}
 	
 	/**
 	 *
 	 * M�todo para inserir na base de dados um Objeto AcessoLog
 	 *
 	 * @return boolean
 	 */
 	public function inserirAcessoLog($oAcessoLog){
 		$oPersistencia = new Persistencia($oAcessoLog);
  		if($nId = $oPersistencia->inserir())
  				return $nId;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para alterar na base de dados um Objeto AcessoLog
 	 *
 	 * @return boolean
 	 */
 	public function alterarAcessoLog($oAcessoLog){
 		$oPersistencia = new Persistencia($oAcessoLog);
  		if($oPersistencia->alterar())
  				return true;
  		return false;
 	}
 	
 	
 	/**
 	 *
 	 * M�todo para excluir da base de dados um Objeto AcessoLog
 	 *
 	 * @return boolean
 	 */
 	public function excluirAcessoLog($nCodAcesso){
 		$oAcessoLog = new AcessoLog();
  		
		$oAcessoLog->setCodAcesso($nCodAcesso);
  		$oPersistencia = new Persistencia($oAcessoLog);
   		$bExcluir = ($_SESSION['oUsuarioLogado'] && $_SESSION['oUsuarioLogado']->getCodGrupoUsuario() != '1') ? $oPersistencia->excluir() : $oPersistencia->excluirFisicamente();
 
 		if($bExcluir)
  				return true;
  		return false;
 		
 	}	
 	
 	/**
 	 *
 	 * M�todo para verificar se existe um Objeto AcessoLog na base de dados
 	 *
 	 * @return boolean
 	 */
 	public function presenteAcessoLog($nCodAcesso){
 		$oAcessoLog = new AcessoLog();
  		
		$oAcessoLog->setCodAcesso($nCodAcesso);
  		$oPersistencia = new Persistencia($oAcessoLog);
  		if($oPersistencia->presente())
  				return true;
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para recuperar um objeto AcessoLog da base de dados
 	 *
 	 * @return AcessoLog
 	 */
 	public function recuperarUmAcessoLog($nCodAcesso){
 		$oAcessoLog = new AcessoLog();
  		$oPersistencia = new Persistencia($oAcessoLog);
  		$sTabelas = "acesso_log";
  		$sCampos = "*";
  		$sComplemento = " WHERE cod_acesso = $nCodAcesso";
  		$voAcessoLog = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoLog)
  			return $voAcessoLog[0];
  		return false;
 	}
 	
 	/**
 	 *
 	 * M�todo para recuperar um vetor de objetos AcessoLog da base de dados
 	 *
 	 * @return AcessoLog[]
 	 */
 	public function recuperarTodosAcessoLog(){
 		$oAcessoLog = new AcessoLog();
  		$oPersistencia = new Persistencia($oAcessoLog);
  		$sTabelas = "acesso_log";
  		$sCampos = "*";
  		$sComplemento = "";
  		$voAcessoLog = $oPersistencia->consultar($sTabelas,$sCampos,$sComplemento);
  		if($voAcessoLog)
  			return $voAcessoLog;
  		return false;
 	}
 	
 	
 
}
?>