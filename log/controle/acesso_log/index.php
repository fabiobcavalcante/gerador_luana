<!DOCTYPE HTML>
 <html lang="pt-br"><!-- InstanceBegin template="/Templates/controle.dwt.php" codeOutsideHTMLIsLocked="false" -->
 <head>
 <meta charset="UTF-8">
 <link href="css/style.css" rel="stylesheet" type="text/css">
 <link href="css/menu.css" rel="stylesheet" type="text/css">
   <script language="javascript" src="js/jquery/jquery.js"></script>
 <script language="javascript" src="js/menu.js"></script>
 <script language="javascript" src="js/producao.js"></script>
 <!-- InstanceBeginEditable name="Head" -->
 <title>Sistema</title>
     <style type="text/css" title="currentStyle">
   	@import "css/jquery-datatables.css";
     </style>
  <link href="css/jquery.ui.core.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery.ui.theme.min.css" rel="stylesheet" type="text/css">
 <link href="css/jquery.ui.dialog.min.css" rel="stylesheet" type="text/css">
 <script src="js/jquery/jquery.js" type="text/javascript"></script>
 <script src="js/jquery/jquery-ui-1.9.2.dialog.custom.min.js" type="text/javascript"></script>
 
    <script language="javascript" src="js/jquery/plugins/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf-8">
     	var asInitVals = new Array();
    	$(document).ready(function() {
    		oTable = $('#lista').dataTable({
    			"bJQueryUI": true,
    			"aoColumnDefs": [
                { 
    			"bSortable": false, "aTargets": [ 0 ],
    			"bSearchable": false, "aTargets": [ 0 ]
    			}
            ]
    		});
    	} );
      </script>
 <!-- InstanceEndEditable -->
 </head>
 <body>
  <?php include_once("controle/includes/mensagem.php")?>
  <div class="TabelaPai" border="0">
  <?php include_once("controle/includes/topo.php")?>
  <?php include_once("controle/includes/menu.php")?>
   <div id="corpo">
     <div id="migalha" class="CelulaMigalhaLink"><strong>Voc&ecirc; est&aacute; aqui: </strong> <a href="index.php">Home</a> &gt; 
     <!-- InstanceBeginEditable name="Migalha" --><strong> Gerenciar AcessoLogs</strong><!-- InstanceEndEditable --></div>
     <div id="largura">
     <div id="titulo"><!-- InstanceBeginEditable name="Titulo Pagina" --><h2 class="TituloPagina"> Gerenciar AcessoLog</h2><!-- InstanceEndEditable --></div>
     <div id="acoes">
     <!-- InstanceBeginEditable name="Conteudo" -->
       <form method="post" action="" name="formAcessoLog" id="formAcessoLog">
  			<table width="100%" cellpadding="2" cellspacing="2" border="0">
  				<tr>
  					<td align="left"><select class="Acoes" name="acoes" id="acoesAcessoLog" onChange="JavaScript: submeteForm('AcessoLog')">
  						<option value="" selected>A&ccedil;&otilde;es...</option>
  						<option value="?action=AcessoLog.preparaFormulario&sOP=Cadastrar" lang="0">Cadastrar novo AcessoLog</option>
  						<option value="?action=AcessoLog.preparaFormulario&sOP=Alterar" lang="1">Alterar AcessoLog selecionado</option>
  						<option value="?action=AcessoLog.preparaFormulario&sOP=Detalhar" lang="1">Detalhar AcessoLog selecionado</option>
  						<option value="?action=AcessoLog.processaFormulario&sOP=Excluir" lang="2">Excluir AcessoLog(s) selecionado(s)</option>
  					</select></td>
                     <td align="right"><a href="javascript:window.print();"><img id="btn_acao" border="0" src="imagens/btn_impressao.gif"></a></td>
  				</tr>
  			</table><br/>
  			<table id="lista" align="left" width="100%" class="TabelaAdministracao" cellpadding="0" cellspacing="0">
  			<?php if(is_array($voAcessoLog)){?>
  				<thead>
  				<tr>
  					<th class="Titulo" width="1%"><img onclick="javascript: marcarTodosCheckBoxFormulario('RecBeneficiario')" src="controle/imagens/checkbox.gif" width="16" height="16"></th>
  					<th class='Titulo'>CodAcesso</th>
					<th class='Titulo'>Usuario</th>
					<th class='Titulo'>Tabela</th>
					<th class='Titulo'>Operacao</th>
					<th class='Titulo'>DataLog</th>
					<th class='Titulo'>Descricao</th>
					<th class='Titulo'>SqlDescricao</th>

  				</tr>
  				</thead>
  				<tbody>
                  <?php foreach($voAcessoLog as $oAcessoLog){ ?>
  				<tr>
 					<td class="Conteudo"><input onClick="JavaScript: atualizaAcoes('AcessoLog')" type="checkbox" value="<?=$oAcessoLog->getCodAcesso()?>" name="fIdAcessoLog[]"/></td>
 					<td class='Conteudo'><?= $oAcessoLog->getCodAcesso()?></td>
					<td class='Conteudo'><?= $oAcessoLog->getUsuario()?></td>
					<td class='Conteudo'><?= $oAcessoLog->getTabela()?></td>
					<td class='Conteudo'><?= $oAcessoLog->getOperacao()?></td>
					<td class='Conteudo'><?= $oAcessoLog->getDataLog()?></td>
					<td class='Conteudo'><?= $oAcessoLog->getDescricao()?></td>
					<td class='Conteudo'><?= $oAcessoLog->getSqlDescricao()?></td>

 				</tr>
 				<?php }?>
 				</tbody>
 				<tfoot>
 				<tr>
  					<td></td>
  				</tr>
  				</tfoot>
 			<?php }//if(count($voAcessoLog)){?>
 			</table>
                   </form>
                   </div>
                 <script language="javascript">
  					atualizaAcoes('AcessoLog');
 				</script>
             </div>
 	<!-- InstanceEndEditable -->
     </div>
     </div>
 </body>
 <!-- InstanceEnd --></html>